var host = "http://www.bitcufund.com/fund/";

$(document).ready(function() {
	Fund_product_init();
	buyModal();
	redirect();
})

function Fund_product_init() {
	var str = window.location.search;
	if (str != undefined) {
		var arr = str.split("=");
		$("#id").val(arr[1]);
		$.ajax({
			"url" : host + "foundInfo/findbyid?id=" + arr[1],
			"type" : "get",
			"dataType" : "json",
			"success" : function(result) {
				$("#fund_name").text(result.fund_name);
				$("#unit_value").text(result.unit_value);
				$("#code").text(result.code);
				$("#survival_time").text(result.survival_time);
				$("#fund_type").text(result.fund_type);
				$("#fund_businessday").text(result.fund_businessday);
				$("#mode_operation").text(result.mode_operation);
				$("#fund_manager").text(result.fund_manager);
				$("#founding_time").text(result.founding_time);
				$("#custodian").text(result.custodian);
				$("#net_fund").text(result.net_fund);
				$("#custodian").text(result.custodian);
			},
			"error" : function() {
			}
		});
	} else {
		return;
	}

}

function Fund_manager_init() {
	var str = window.location.search;
	if (str != undefined) {
		var arr = str.split("=");
		$("#id").val(arr[1]);
		$.ajax({
			"url" : host + "foundInfo/findbyid?id=" + arr[1],
			"type" : "get",
			"dataType" : "json",
			"success" : function(result) {
				$("#manager_img").attr("src", result.photo_url);
				$("#manager_name").text(result.manager_name);
				$("#manager_ntro").text(result.ntro);
			},
			"error" : function() {
			}
		});
	} else {
		return;
	}

}

function Fund_TZZH_init() {
	var str = window.location.search;
	if (str != undefined) {
		var arr = str.split("=");
		$("#id").val(arr[1]);
		$.ajax({
			"url" : host + "foundInfo/findbyid?id=" + arr[1],
			"type" : "get",
			"dataType" : "json",
			"success" : function(result) {
				$("tbody").empty();
				var tab = $("tbody");
				$.each(result, function(index, element) {
					var itemHtml = "<tr>" + "<td>2018-03-31</td>"
							+ "<td>北新建材</td>" + "<td>1,250,000</td>"
							+ "<td>31,475,000.00</td>" + "<td>1.65</td>"
							+ "</tr>";
					tab.append(tab);
				});
			},
			"error" : function() {
			}
		});
	} else {
		return;
	}
}

function Fund_dividend_init() {
	var str = window.location.search;
	if (str != undefined) {
		var arr = str.split("=");
		$("#id").val(arr[1]);
		$.ajax({
			"url" : host + "foundInfo/findbyid?id=" + arr[1],
			"type" : "get",
			"dataType" : "json",
			"success" : function(result) {
				$("tbody").empty();
				var tab = $("tbody");
				$.each(result, function(index, element) {
					var itemHtml = "<tr>" + "<td>2018-01-16</td>"
							+ "<td>0.02</td>" + "<td>2018-01-16</td>"
							+ "<td>2018-01-16</td>" + "</tr>";
					tab.append(tab);
				});
			},
			"error" : function() {
			}
		});
	} else {
		return;
	}
}

function Fund_rating_init() {
	var str = window.location.search;
	if (str != undefined) {
		var arr = str.split("=");
		$("#id").val(arr[1]);
		$.ajax({
			"url" : host + "foundInfo/findbyid?id=" + arr[1],
			"type" : "get",
			"dataType" : "json",
			"success" : function(result) {
				$("tbody").empty();
				var tab = $("tbody");
				$.each(result, function(index, element) {
					var itemHtml = "<tr>" + "<td>2018-01-16</td>"
							+ "<td>0.02</td>" + "<td>2018-01-16</td>"
							+ "<td>2018-01-16</td>" + "</tr>";
					tab.append(tab);
				});
			},
			"error" : function() {
			}
		});
	} else {
		return;
	}
}

function buyModal() {
	$('.buy_fund')
			.click(
					function() {
						layer
								.open({
									title : '申购',
									type : 1,
									skin : 'layui-layer-demo', // 样式类名
									closeBtn : 0, // 不显示关闭按钮
									anim : 2, // 弹出层样式
									shadeClose : true, // 开启遮罩关闭
									area : [ '40%', '500px' ],
									content : "<div class='' id='a_bodyDiv'>"
											+ "<div class='tit'><div><span>基金名称：</span></div><span id='fund_name'>链想基金一号</span></div>"
											+ "<div class='tit'><div><span>单位净值：</span></div><span id='fund_asset'>21111</span></div>"
											+ "<div class='tit'><div><span>申购份额：</span></div><input type='text' id='fund_share' /></div>"
											+ "<div class='tit'><div><span></span></div><span id='fund_total'>总额:<span class='btc_large' id='fund_total_usdt'>2000.12</span>&nbsp;USDT&nbsp;&nbsp;(手续费:<span class='btc_large' id='fund_charge'>200</span>USDT)</span></div>"
											+ "<div class='tit'><div><span>支付方式:</span></div><select><option value='1'>BTC</option><option value='2'>ETC</option><option value='3'>LTC</option><select></div>"
											+ "<div class='tit'><div><span></span></div><span id='name'>所需<span>BTC</span>数量:&nbsp;&nbsp;<span id='end_total'>1.25555</span></span></div>"
											+ "<div class='tit'>申购后请联系在线客服进行最后确认以及支付!</div>"
											+ "<div class='tit'><input type='button' value='确认申购' id='buyFund'/></div>"
											+ "</div>"
								});
						var tr = $(this).parent().parent();
						var fund_name = tr.find(".fundName").text();
						var net_fund = tr.find(".net_fund").text();
						$("#fund_name").text(fund_name);
						$("#fund_asset").text(fund_name);
					});
}

function redirect() {
	$("#jjgkHref").on("click", function() {
		var id = $("#id").val();
		location.href = "fundProduct.html?id=" + id;
	});
	$("#jjjlHref").on("click", function() {
		var id = $("#id").val();
		location.href = "fundManager.html?id=" + id;
	});
	$("#tzzhHref").on("click", function() {
		var id = $("#id").val();
		location.href = "fundTZZH.html?id=" + id;
	});
	$("#jjfhHref").on("click", function() {
		var id = $("#id").val();
		location.href = "fundDividend.html?id=" + id;
	});
	$("#fljgHref").on("click", function() {
		var id = $("#id").val();
		location.href = "fundRating.html?id=" + id;
	});
}