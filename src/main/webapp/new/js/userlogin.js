var host = "http://www.bitcufund.com/fund/";

$(document).ready(function() {
	tip_loginForm();
	login();
});
/***
 *点击登陆
 * */
function login() {
	$("#loginForm_submit").click(function() {

		var userName = $("#loginForm_lognumber").val();
		if(userName.length == 0) {
			$("#tip_loginForm").show();
			return;
		} else {
			$("#tip_loginForm").hide();
		}
		var Password = $("#loginForm_logpassword").val();
		$.ajax({
			type: "get",
			url: host + "userInfo/longin?userMobile=" + userName + "&userPwd=" + Password,
			data: "",
			dataType: "json",
			error: function() {
				alert("error");
			},
			success: function(data) {
				 var storage=window.localStorage;
		         
		            storage.setItem("token",data.token);
				$("#tip_loginForm2").text(data.message);
				if(data.succeed) {
					document.cookie = "userId=" + escape(userName);
					location.href = "index.html";
				}
			}
		})
	})
}

function tip_loginForm() {
	$("#tip_loginForm").hide();
}