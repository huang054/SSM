var host = "http://www.bitcufund.com/fund";
var count = 1;

$(document).ready(function() {
	Fund_init();
	New_init();
	buyModal();
	Fund_init1();
	Fund_init2();
	toFundDetail();
	toNewDetail();
	setInterval("luobo()", 2000);

})

function Fund_init() {
	$("#fund_list").empty();
	$.ajax({
		"url": host + "/foundInfo/findslist",
		"type": "get",
		"async": false,
		"dataType": "json",
		"success": function(result) {
			$.each(result, function(index, element) {
				tableHtml_fund(element);
			});
		},
		"error": function() {
			//alert("出现错误！");
		}
	});
}

function tableHtml_fund(element) {
	var unixTimestamp = new Date(element.net_time);
	var commonTime = unixTimestamp.toLocaleString();
	var itemHTML = "<tr>" +
		"<td align='left' class='fund_name'>" +
		"<div class='fund_bg'>" +
		"<input type='hidden' class='fund_id' value=" + element.id + ">" +
		"<a target='_blank' class='fundName'>" + element.fund_name + "</a>" +
		"</div>" +
		"</td>" +
		"<td>" + element.code + "</td>" +
		"<td>" + commonTime + "</td>" +
		"<td class='net_fund'>" + element.net_fund + "</td>" +
		"<td>" + element.seven_day_annualized + "</td>" +
		"<td><span class='green'>" + element.last_half_year + "</span></td>" +
		"<td><span class='green'>" + element.last_year + "</span></td>" +
		"<td><span class='red'>" + element.since_this_year + "</span></td>" +
		"<td><span class='green'>" + element.last_half_year + "</span></td>" +
		"<td><span class='red'>" + element.since_founding + "</span></td>" +
		"<td><input type='hidden'class='transfer_charge' value=" + element.transferCharge + "><button class='layui-btn layui-btn-warm buy_fund'>申购</button></td>" +
		"</tr>";
	$("#fund_list").append(itemHTML);
}

function prorityInit() {
	$.ajax({
		"url": host + "/foundInfo/???",
		"type": "get",
		"async": false,
		"dataType": "json",
		"success": function(result) {
			$.each(result, function(index, element) {
				tableHtml_fund(element);
			});
		},
		"error": function() {
			//alert("出现错误！");
		}
	});
}

function New_init() {
	
	var box1 = $(".index_news_box").eq(0);
	$.ajax({
		"url": host + "/indystryInfo/industrylist1",
		"type": "get",
		"async": false,
		"dataType": "json",
		"success": function(result) {
			$.each(result, function(index, element) {
				var unixTimestamp = new Date(element.create_date);
				var commonTime = unixTimestamp.toLocaleString();
				var itemHTML = "<li><span class='right'>" + commonTime + "</span><input type='hidden' value=" + element.id + "><a class='new_detail'>" + element.title + "</a>&nbsp;</li>";
				box1.append(itemHTML);

			});
		},
		"error": function() {
			//alert("出现错误！");
		}
	});

	var box2 = $(".index_news_box").eq(1);
	$.ajax({
		"url": host + "/indystryInfo/industrylist2",
		"type": "get",
		"async": false,
		"dataType": "json",
		"success": function(result) {
			$.each(result, function(index, element) {
				var unixTimestamp = new Date(element.create_date);
				var commonTime = unixTimestamp.toLocaleString();
				var itemHTML = "<li><span class='right'>" + commonTime + "</span><input type='hidden' value=" + element.id + "/><a class='new_detail'>" + element.title + "</a>&nbsp;</li>";
				box2.append(itemHTML);

			});
		},
		"error": function() {
			//alert("出现错误！");
		}
	});

	var box3 = $(".index_news_box").eq(2);
	$.ajax({
		"url": host + "/indystryInfo/industrylist3",
		"type": "get",
		"async": false,
		"dataType": "json",
		"success": function(result) {
			$.each(result, function(index, element) {
				var unixTimestamp = new Date(element.create_date);
				var commonTime = unixTimestamp.toLocaleString();
				var itemHTML = "<li><span class='right'>" + commonTime + "</span><input type='hidden' value=" + element.id + "/><a class='new_detail'>" + element.title + "</a>&nbsp;</li>";
				box3.append(itemHTML);

			});
		},
		"error": function() {
			//alert("出现错误！");
		}
	});

	var box4 = $(".index_news_box").eq(3);
	$.ajax({
		"url": host + "/indystryInfo/industrylist4",
		"type": "get",
		"async": false,
		"dataType": "json",
		"success": function(result) {
			$.each(result, function(index, element) {
				var unixTimestamp = new Date(element.create_date);
				var commonTime = unixTimestamp.toLocaleString();
				var itemHTML = "<li><span class='right'>" + commonTime + "</span><input type='hidden' value=" + element.id + "/><a class='new_detail'>" + element.title + "</a>&nbsp;</li>";
				box4.append(itemHTML);

			});
		},
		"error": function() {
			//alert("出现错误！");
		}
	});

}

function Fund_init1() {
	$.ajax({
		"url": host + "/foundInfo/findslist",
		"type": "get",
		"async": false,
		"dataType": "json",
		"success": function(result) {
			$.each(result, function(index, element) {
				$("#p1_name").text(element.fund_name);
				$("#p1_daily_increase").text(element.daily_increase);
				$("#p1_ten_thousand_income").text(element.ten_thousand_income);
				return;
			})
		},
		"error": function() {
			//alert("出现错误！");
		}
	});
}

function Fund_init2() {
	$.ajax({
		"url": host + "/foundInfo/findslist",
		"type": "get",
		"async": false,
		"dataType": "json",
		"success": function(result) {
			$.each(result, function(index, element) {

				switch(index) {
					case 0:
						var itemHtml = element.fund_name + "<span class='f13'>(" + element.code + ")</span>";
						$("#p2_1_name").html(itemHtml);
						break;
					case 1:
						var itemHtml = element.fund_name + "<span class='f13'>(" + element.code + ")</span>";
						$("#p2_2_name").html(itemHtml);
						$("#p2_2_seven_day_annualized").text(element.seven_day_annualized);
						$("#p2_2_ten_thousand_income").text(element.ten_thousand_income);
						break;
					case 2:
						var itemHtml = element.fund_name + "<span class='f13'>(" + element.code + ")</span>";
						$("#p2_3_name").html(itemHtml);
						$("#p2_3_last_year").text(element.last_year);
						$("#p2_3_daily_increase").text(element.daily_increase);
						break;
					case 3:
						var itemHtml = element.fund_name + "<span class='f13'>(" + element.code + ")</span>";
						$("#p2_4_name").html(itemHtml);
						$("#p2_4_last_half_year").text(element.last_half_year);
						$("#p2_4_since_this_year").text(element.since_this_year);
						break;
					default:
						break;
				}
			});
		},
		"error": function() {
			//alert("出现错误！");
		}
	});
	$.ajax({
		"url": host + "/foundInfo/findslist",
		"type": "get",
		"async": false,
		"dataType": "json",
		"success": function(result) {
			$.each(result, function(index, element) {
				var itemHtml = element.fund_name + "<span class='f13'>(" + element.code + ")</span>";
				$("#p2_1_name").html(itemHtml);
			});
		},
		"error": function() {
			//alert("出现错误！");
		}
	});

}

function buyModal() {
	$('.buy_fund').click(function() {
		var storage=window.localStorage;
	     
	     var tokenStr= storage.getItem("token");
	     
		var arrStr = document.cookie.split("=");
		var userId = unescape(arrStr[1]);
		if(userId != "undefined") {
			layer.open({
				title: '申购',
				type: 1,
				skin: 'layui-layer-demo', //样式类名
				closeBtn: 0, //不显示关闭按钮
				anim: 2, //弹出层样式
				shadeClose: true, //开启遮罩关闭
				area: ['40%', '500px'],
				content: "<div class='' id='a_bodyDiv'><input type='hidden' id='fund_id' />" +
					"<div class='tit'><div><span>基金名称：</span></div><span id='fund_name'>链想基金一号</span></div>" +
					"<div class='tit'><div><span>单位净值：</span></div><span id='fund_asset'>21111</span></div>" +
					"<div class='tit'><div><span>申购份额：</span></div><input type='text' id='fund_share' /></div>" +
					"<div class='tit'><div><span></span></div><span id='fund_total'>总额:<span class='btc_large' id='fund_total_usdt'></span>&nbsp;USDT&nbsp;&nbsp;(手续费:<span class='btc_large' id='fund_charge'>0</span>USDT)</span></div>" +
					"<div class='tit'><div><span>支付方式:</span></div><select id='buy_way'><option value='1'>BTC</option><option value='2'>ETC</option><option value='3'>LTC</option><option value='4'>其他</option><select></div>" +
					"<div class='tit'><div><span></span></div><span id='name'>所需<span>BTC</span>数量:&nbsp;&nbsp;<span id='end_total'>1.25555</span></span></div>" +
					"<div class='tit'>申购后请联系在线客服进行最后确认以及支付!</div>" +
					"<div class='tit'><input type='hidden' id='fund_transfer_charge' value=''><input type='button' value='确认申购' id='buyFund'/></div>" +
					"</div>"
			});

			var tr = $(this).parent().parent();
			var fund_name = tr.find(".fundName").text();
			var net_fund = tr.find(".net_fund").text();
			var fund_id = tr.find(".fund_id").val();
			var transfer_charge = tr.find(".transfer_charge").val();
			$("#fund_name").text(fund_name);
			$("#fund_asset").text(net_fund);
			$("#fund_transfer_charge").val(transfer_charge);
			inputCount();
			buyClick(userId, fund_id, $("#fund_share").val(), $("#buy_way option:selected").text());

		} else {
			location.href = "personalLogin.html";
		}
	});
}

function toFundDetail() {
	$(".fundName").on("click", function() {
		var id = $(this).prev().val();
		location.href = "fundProduct.html?id=" + id;
	})
}

function inputCount() {
	$("#fund_share").on("input", function() {
		var count = $(this).val();
		var fund_asset = $("#fund_asset").text();
		var fare_value = $("#fund_transfer_charge").val();
		if(!isNaN(count)) {
			var fare = count * fund_asset * fare_value;
			$("#fund_charge").text(fare.toFixed(4));
			$("#fund_total_usdt").text(count * fund_asset + fare);
		} else {
			return;
		}
	});
}

function buyClick(userId, fundId, fundCount, way) {
	$("#buyFund").on("click", function() {
		var data = {
			"userId": userId,
			"sFundId": fundId,
			"BigDecimal": fundCount,
			"sShare": way
		};
		$.ajax({
			"url": host + "/userInfo/subscribe",
			"type": "get",
			"data": data,
			"dataType": "json",
			"success": function(result) {
				location.href = host+"fund/account/userCenter.html";
			},
			"error": function() {
				//alert("出现错误！");
			}
		});
	})
}

function toNewDetail() {
	$(".new_detail").on("click", function() {
		var id = $(this).prev().val();
		alert(id);
		location.href = "newDetail.html?id=" + id;
	})
}

function luobo() {
	switch(count) {
		case 1:
			$(".d1").eq(0).css("display", "block");
			$(".d1").eq(1).css("display", "none");
			$(".d1").eq(2).css("display", "none");
			count++;
			break;
		case 2:
			$(".d1").eq(0).css("display", "none");
			$(".d1").eq(1).css("display", "block");
			$(".d1").eq(2).css("display", "none");
			count++;
			break;
		case 3:
			$(".d1").eq(0).css("display", "none");
			$(".d1").eq(1).css("display", "none");
			$(".d1").eq(2).css("display", "block");
			count = 1;
			break;

		default:
			break;
	}
}