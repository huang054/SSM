var host = "http://www.bitcufund.com/fund";

$(document).ready(function() {
	Fund_init();
	buyModal();
})

function Fund_init() {
	$("#fund_list").empty();
	$.ajax({
		"url": host + "/foundInfo/findslist",
		"type": "get",
		"async": false,
		"dataType": "json",
		"success": function(result) {
			$.each(result, function(index, element) {
				tableHtml_fund(element);
			});
		},
		"error": function() {
			//alert("出现错误！");
		}
	});
}

function tableHtml_fund(element) {
	var itemHTML = "<tr>" +
		"<td align='left' class='fund_name'>" +
		"<div class='fund_bg'>" +
		"<a target='_blank' href='fundProduct.html' class='fund_name'>" + element.fund_name + "</a>" +
		"</div>" +
		"</td>" +
		"<td>" + element.code + "</td>" +
		"<td>" + element.net_time + "</td>" +
		"<td class='net_fund'>" + element.net_fund + "</td>" +
		"<td>" + element.seven_day_annualized + "</td>" +
		"<td><span class='green'>" + element.last_half_year + "</span></td>" +
		"<td><span class='green'>" + element.last_year + "</span></td>" +
		"<td><span class='red'>" + element.since_this_year + "</span></td>" +
		"<td><span class='green'>" + element.last_half_year + "</span></td>" +
		"<td><span class='red'>" + element.since_founding + "</span></td>" +
		"<td><button class='layui-btn layui-btn-warm buy_fund'>申购</button></td>" +
		"</tr>";
	$("#fund_list").append(itemHTML);
}

function buyModal() {
	$('.buy_fund').click(function() {
		document.cookie = "userId=" + escape("000");
		var arrStr = document.cookie.split("=");
		var userId = unescape(arrStr[1]);
		if(userId != '') {
			location.href = "login.html";
		} else {
			layer.open({
				title: '申购',
				type: 1,
				skin: 'layui-layer-demo', //样式类名
				closeBtn: 0, //不显示关闭按钮
				anim: 2, //弹出层样式
				shadeClose: true, //开启遮罩关闭
				area: ['40%', '500px'],
				content: "<div class='' id='a_bodyDiv'><input type='hidden' id='fund_id' />" +
					"<div class='tit'><div><span>基金名称：</span></div><span id='fund_name'>链想基金一号</span></div>" +
					"<div class='tit'><div><span>单位净值：</span></div><span id='fund_asset'>21111</span></div>" +
					"<div class='tit'><div><span>申购份额：</span></div><input type='text' id='fund_share' /></div>" +
					"<div class='tit'><div><span></span></div><span id='fund_total'>总额:<span class='btc_large' id='fund_total_usdt'></span>&nbsp;USDT&nbsp;&nbsp;(手续费:<span class='btc_large' id='fund_charge'>0</span>USDT)</span></div>" +
					"<div class='tit'><div><span>支付方式:</span></div><select id='buy_way'><option value='1'>BTC</option><option value='2'>ETC</option><option value='3'>LTC</option><option value='4'>其他</option><select></div>" +
					"<div class='tit'><div><span></span></div><span id='name'>所需<span>BTC</span>数量:&nbsp;&nbsp;<span id='end_total'>1.25555</span></span></div>" +
					"<div class='tit'>申购后请联系在线客服进行最后确认以及支付!</div>" +
					"<div class='tit'><input type='hidden' id='fund_transfer_charge' value=''><input type='button' value='确认申购' id='buyFund'/></div>" +
					"</div>"
			});

			var tr = $(this).parent().parent();
			var fund_name = tr.find(".fundName").text();
			var net_fund = tr.find(".net_fund").text();
			var fund_id = tr.find(".fund_id").val();
			var transfer_charge = tr.find(".transfer_charge").val();
			$("#fund_name").text(fund_name);
			$("#fund_asset").text(net_fund);
			$("#fund_transfer_charge").val(transfer_charge);
			inputCount();
			buyClick(userId, fund_id, $("#fund_share").val(), $("#buy_way option:selected"));
		}
	});
}

function inputCount() {
	$("#fund_share").on("input", function() {
		var count = $(this).val();
		var fund_asset = $("#fund_asset").text();
		var fare_value = $("#fund_transfer_charge").val();
		if(!isNaN(count)) {
			var fare = count * fund_asset * fare_value;
			$("#fund_charge").text(fare.toFixed(4));
			$("#fund_total_usdt").text(count * fund_asset + fare);
		} else {
			return;
		}
	});
}