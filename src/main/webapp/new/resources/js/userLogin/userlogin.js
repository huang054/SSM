$(document).ready(function () {
    login();
});
/***
 *点击登陆
 * */
function login() {
    $("#loginForm_submit").click(function () {

        var phone =  $("#loginForm_lognumber").val();

        var Password = $("#loginForm_logpassword").val();

        if(phone.length==0||Password.length==0){
            show();
            return;
        }

        if(!isPhoneNo(phone)){
            hide();
            return;
        }else{
           clean();
        }
        $.ajax({
            type: "post",
            url: "http://127.0.0.1:8080/maven02/userInfo/longin?userMobile="+phone+"&userPwd="+Password, 
            data:"",
            dataType: "json",
            error:function (){
                alert("error");
            },
            success: function(data){
            	//alert(data.succeed);
                if(data.code=="9999"){
                    window.location.href="http://127.0.0.1:8080/maven02/index.html";
                }else{
                	hide();
                }
            }
        })
    })
}

/**
 * 前段验证手机号
 * @param phone
 * @returns {boolean}
 */
function isPhoneNo(phone) {
    var pattern = /^1[34578]\d{9}$/;
    return pattern.test(phone);
}
/**
 * 密码错误
 */
function hide() {
    $("#tip_loginForm").text("手机号或者密码错误！");
}
/**
 * 输入密码
 */
function show() {
    $("#tip_loginForm").text("请输入手机号或密码！");
}
/**
 * 清空
 */
function clean() {
    $("#tip_loginForm").text("");
}