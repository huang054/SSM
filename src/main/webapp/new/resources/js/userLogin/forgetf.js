var interval;
$(document).ready(function () {
        hide();
        get();
        validate();
    regist_submit();
    setInterval("registoktz()",1000);
});

/**
 * 手机验证隐藏
 */
function hide() {
        $("#tip_mobile").hide();
}
/**
 * 手机显示验证
 */
function show() {
    $("#tip_mobile").show();
}
/**
 * 验证码验证隐藏
 */
function yhide() {
    $("#tip_returnCode").hide();
}
/**
 * 显示验证码验证1
 */
function yshow() {
    $("#tip_returnCode").show();
}

/**
 * 获得验证码
 */
function get() {
    $("#resendBt").click(function () {
        var phone = $("#mobile").val();
        if(isPhoneNo(phone)){
            $("#resendBt1").show();
            $("#resendBt").hide();
            interval = setInterval("phoneok()",1000);
                $.ajax({
                    type:"get",
                    url:"http://127.0.0.1:8080/maven02/userInfo/wujipwd?userMobile="+phone,
                    data:"",
                    dataType:"json",
                    success:function (data) {
                    	//alert(data.code);
                    	if(data.code=="6666"){
                    		$("#tip_returnCode1").text(data.message);
                    	}
                    },
                    error:function () {
                        
                    }
                });
                hide();
        }else{
                show();
        }
    })
}
/**
 * 前段验证手机号
 * @param phone
 * @returns {boolean}
 */
function isPhoneNo(phone) {
    var pattern = /^1[34578]\d{9}$/;
    return pattern.test(phone);
}

/**
 * 前端验证密码
 * @param pass
 * @returns {boolean}
 */
function isPassword(pass) {
    var pattern = /^([A-Za-z0-9]){6,20}$/;
    return pattern.test(pass);
}
function isSame(pass,rpass) {
        if(pass==rpass){
                return true;
        }
        return false;
}
/**
 * 后台验证验证码
 */
function validate() {
        $("#registOneForm_submit").click(function () {
                var phone = $("#mobile").val();
                var valdata = $("#returnCode").val();
                if(valdata.length<6){
                        yshow();
                        return;
                }else{
                        yhide();
                        $.ajax({
                            type:"get",
                            url:"http://127.0.0.1:8080/maven02/userInfo/register?userMobile="+phone+"&noteCode="+valdata,
                            data:"",
                            dataType:"json",
                            success:function (data) {
                               
                                if(!data.succeed){
                                       $("#tip_returnCode1").text(data.message);
                                }else if(data.code=="9999"){
                                	 window.location.href="http://127.0.0.1:8080/maven02/ForgotPasswords.html?phone="+phone;
                                }
                            },
                            error:function () {
                                
                            }
                        })
                }
        })
}

/**
 * 密码验证显示
 */
function passShow() {
        $("#tip_pwd").show();
}

/**
 * 密码验证隐藏
 */
function passHide() {
    $("#tip_pwd").hide();
}
/**
 * 密码二次验证显示
 */
function rpassShow() {
    $("#tip_pwd_repeat").show();
}
/**
 * 密码二次验证隐藏
 */
function rpassHide() {
    $("#tip_pwd_repeat").hide();
}

/**
 * 密码不同显示
 */
function sameshow() {
    $("#issame").show();
}

/**
 * 密码相同隐藏
 */
function samehide() {
    $("#issame").hide();
}

/**
 * 忘记密码注册
 */
function regist_submit() {
    $("#regist_submit").click(function () {
    	var phone = window.location.search;
    	console.log(phone.split("=")[1])
    	if(phone != undefined) {
    		phone = phone.split("=");
    		phone = phone[1];
    	}
        var pwd = $("#pwd").val();
        var rpwd = $("#pwd_repeat").val();
        if(isPassword(pwd)){
                passHide();
        }else{
                passShow();
        }
        if(isPassword(rpwd)){
            rpassHide();
        }else{
            rpassShow();
        }
        if(isSame(pwd,rpwd)){
            samehide();
        }else{
            sameshow();
        }
        if(isPassword(pwd)&isPassword(rpwd)&isSame(pwd,rpwd)){

            $.ajax({
            	type:"post",
                url:"http://127.0.0.1:8080/maven02/userInfo/forgetupdatepwd?onepwd="+pwd+"&twopwd="+rpwd+"&phone="+phone+"&userMoblie="+phone,
                data:"",
                dataType:"json",
                success:function (data) {
                    if(data.code=="9999"){
                            window.location.href="http://127.0.0.1:8080/maven02/findok.html";
                    }
                },
                error:function () {

                }
            })
        }else{
                alert("error")
        }
    })
}

/**
 * 成功跳转
 */
function registoktz() {
        var number = $("#seconds").text();
        number = number-1;
        if(number==0){
                number=5;
                window.location.href="http://127.0.0.1:8080/maven02/personalLogin.html";
                return;
        }
    $("#seconds").text(number);

}

/**
 * 验证码读数
 */
function phoneok() {
    var number = $("#resendBt1").text();
    number = number-1;
    if(number==0){
        number="60";
        $("#resendBt1").text(number);
        clearInterval(interval);
        $("#resendBt1").hide();
        $("#resendBt").show();
        return;
    }
    $("#resendBt1").text(number);

}
