$(document).ready(function () {
    message1();
    message2();
    login();
});
/***
 *点击登陆
 * */
function login() {
    $("#loginForm_submit").click(function () {

        var userName =  $("#loginForm_logname").val();
        if(userName.length==0){
            $("#message1").show();
            return;
        }else{
            $("#message1").hide();
        }
        var phone = $("#loginForm_logphone").val();
        if(phone.length==0){
            $("#message2").show();
            return;
        }else{
            $("#message2").hide();
        }
        var password = $("#loginForm_logpassword").val();
        var json = {
            "name":userName,
            "phone":phone,
            "password":password
        };
        $.ajax({
            type: "post",
            url: "",
            data: json,
            dataType: "json",
            error:function (){
                alert("error");
            },
            success: function(data){
                if(data.success){
                    window.location.reload("");
                }
            }
        })
    })
}

function message1() {
    $("#message1").hide();
}
function message2() {
    $("#message2").hide();
}