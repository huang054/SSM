$(document).ready(function() {
	getACountData();
	pageInit();
})

// 持有基金接口
function pageInit() {
	 var storage=window.localStorage;
     
     var tokenStr= storage.getItem("token");
	layui.use('table', function() {
		var table = layui.table;
		
		table.render({
			elem : '#fundlist',
			url : server + '/userInfo/findList',
			method : "post",
			where: {token: tokenStr},
			cols : [ [ {
				field : 'id',
				width : 50,
				title : '序号'
			}, {
				field : 'fName',
				width : 100,
				edit : 'text',
				title : '基金名称'
			}, {
				field : 'purchaseTime',
				width : 120,
				title : '申购时间',
				sort : true
			}, {
				field : 'share',
				width : 80,
				title : '份额'
			}, {
				field : 'netValue',
				title : '单位净值',
				width : 120
			}, {
				field : 'dailyIncreases',
				width : 80,
				title : '日涨幅',
				sort : true
			}, {
				field : 'yesterDayProfitLoss',
				width : 120,
				title : '昨日盈亏',
			}, {
				field : 'addProfitLoss',
				width : 120,
				title : '累计盈亏',
			}, {
				field : 'totalMoney',
				width : 80,
				title : '总金额'
			}

			] ],
			page : true
		});
	});
}

// 申购记录接口
function applylistinnt() {
	 var storage=window.localStorage;
     
     var tokenStr= storage.getItem("token");
	layui.use('table', function() {
		var table = layui.table;

		table.render({
			elem : '#applylist',
			url : server + '/userInfo/selcetfund',
			method : "post",
			where: {token: tokenStr},
			cols : [ [ {
				field : 'creatTime',
				width : 200,
				title : '申购时间',
				sort : true
			}, {
				field : 'sFundName',
				title : '基金名称'
			}, {
				field : 'sNetFund',
				width : 120,
				title : '单位净值',
				sort : true
			}, {
				field : 'payment',
				width : 120,
				title : '申购份额'
			}, {
				field : 'serviceCharge',
				title : '手续费',
				width : 120
			}, {
				field : 'totalMoney',
				width : 120,
				title : '申购总额',
				sort : true
			}, {
				fixed : 'right',
				width : 200,
				title : '状态',
				toolbar : '#paytoolbar'
			}

			] ],
			page : true
		});

	});

}

// 资产管理接口
function capitallistinnt(type) {
	 var storage=window.localStorage;
     
     var tokenStr= storage.getItem("token");
	var url = '';
	if (type == 'out' || type == 'in') {

		if (type == 'out') {
			url = '/assetsInOut/assetsOut';
		} else if (type == 'in') {
			url = '/assetsInOut/assetsIn';

		}
		layui.use('table', function() {
			var table = layui.table;

			table.render({
				elem : '#capitallist',
				url : server + url,
				where: {token: tokenStr},
				cols : [ [ {
					field : 'date',
					width : 200,
					title : '时间',
					sort : true,
				}, {
					field : 'fundName',
					title : '基金名称'
				}, {
					field : 'fundNum',
					width : 140,
					title : '份额',
					sort : true
				}, {
					field : 'fundPirce',
					width : 160,
					title : '单位净值'
				}, {
					field : 'totalPirce',
					title : '总金额',
					width : 140
				}, {
					field : 'submit',
					width : 140,
					title : '备注',
					sort : true
				} ] ],
				page : true
			});
		});

	} else {
		url = "/profit/findProfits";
		layui.use('table', function() {
			var table = layui.table;

			table.render({
				elem : '#capitallist',
				url : server + url,
				where: {token: tokenStr},
				cols : [ [ {
					field : 'time',
					width : 200,
					title : '时间',
					sort : true
				}, {
					field : 'yinFundName',
					title : '基金名称'
				}, {
					field : 'yinFundsFnfo',
					width : 160,
					title : '单位净值'
				}, {
					field : 'yinDailyIncrease',
					width : 160,
					title : '日涨幅'
				}, {
					field : 'profitLoss',
					title : '当日盈亏',
					width : 140
				}

				] ],
				page : true
			});
		});

	}

}

// 查询资产数据接口

function getACountData() {
	 var storage=window.localStorage;
     
     var tokenStr= storage.getItem("token");
	$.ajax({
		type : 'GET',
		url : server + "/userInfo/ssets?token="+tokenStr,
	
		success : function(data) {

			$("#dollar").html(data.dollar);
			$("#totalAssets").html(data.totalAssets);
			$("#yesterdayProfitLoss").html(data.yesterdayProfitLoss);
			$("#profitLoss").html(data.profitLoss);
		},
		dataType : 'json'
	});
}

// 个人信息接口
function getAccountInfo() {
	 var storage=window.localStorage;
     
     var tokenStr= storage.getItem("token");
	$.ajax({
		type : 'GET',
		url : server + "/userInfo/ssets?token="+tokenStr,
		
		success : function(data) {

			$("#account_id").html(data.accountNumber);
			$("#account_phone").html(data.userMobile);
		},
		dataType : 'json'
	});
}
// 换手机号弹窗
function changePhoneBtn() {

	layer
			.open({
				type : 1,
				title : '更換手机号',
				skin : 'demo-class',
				offset : 'auto' // 具体配置参考：http://www.layui.com/doc/modules/layer.html#offset
				,
				id : 'layerDemo' + 'auto' // 防止重复弹出
				,
				area : [ '30%', '40%' ],
				content : " <form ><table><tr style='height:20px;'></tr><tr ><td>新手机号</td>  <td ><input style='margin-left: 20px' /></td>  </tr> <tr style='height:20px;'></tr><tr><td>验证码</td><td><input style='margin-left: 20px' /></td><td ><a style='margin-left: 20px;width:60px;' class='com_btn' href='javascript:void(0)'>发送验证码</a></td>  </tr> <tr style='height:20px;'></tr><tr ><td>登录密码</td>  <td ><input style='margin-left: 20px' /></td>  </tr> "
						+ "<tr style='height:50px;'></tr><tr ><td><input hidden='hidden' style='width:10px;'/></td><td ><a style='margin-left: 100px' class='com_btn' href='javascript:void(0)' >保存</a></td></tr> </table>  </form>",
				btnAlign : 'c' // 按钮居中
				,
				shade : 0 // 不显示遮罩
				,
				yes : function() {
					layer.closeAll();
				}
			});
};
// 确认已经支付弹窗
function confirmPayFun(id, status) {
	var contentStr = '';
	if (status == 1) {
		contentStr = '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;">如果您已经支付请点击确定按钮</div>'
	} else if (status == 4) {
		contentStr = '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;">确定取消该订单吗！</div>'
	}
	layer.open({
		type : 1,
		title : false // 不显示标题栏
		,
		closeBtn : false,
		area : '300px;',
		shade : 0.8,
		id : 'confirmPay' // 设定一个id，防止重复弹出
		,
		btn : [ '确认已支付', '取消' ],
		btnAlign : 'c',
		moveType : 1 // 拖拽模式，0或者1
		,
		content : contentStr,
		yes : function() {
			$.ajax({
				type : 'GET',

				url : server + "/userInfo/affirm?status=" + status + "&id="
						+ id,

				success : function(data) {
					if (data.code == '0') {
						layer.msg(data.message);
					}
					// 刷新数据
					applylistinnt();
				},
				dataType : 'json'
			});

			layer.closeAll();

		}

		,
		success : function(layero) {
			var btn = layero.find('.layui-layer-btn');

		}
	});
};

