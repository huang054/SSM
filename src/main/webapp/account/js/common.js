///////////////////////////////////////////////////////////////////////////////
// 原生js扩展方法
String.prototype.startWith = function (str) {
    var reg = new RegExp("^" + str);
    return reg.test(this);
};

String.prototype.endWith = function (str) {
    var reg = new RegExp(str + "$");
    return reg.test(this);
};

String.prototype.equalsIgnoreCase = function (value) {
    try {
        if (value.toLowerCase() == this.toLowerCase()) {
            return true;
        }
        return false;
    } catch (e) {
        return false;
    }
};

String.prototype.trim = function () {
    return this.replace(/^\s+/g, "").replace(/\s+$/g, "");
};

///////////////////////////////////////////////////////////////////////////////
// 浏览器版本辅助判断
(function (jQuery) {
    if (jQuery.browser) return;

    jQuery.browser = {};
    jQuery.browser.mozilla = false;
    jQuery.browser.webkit = false;
    jQuery.browser.opera = false;
    jQuery.browser.msie = false;

    var nAgt = navigator.userAgent;
    jQuery.browser.name = navigator.appName;
    jQuery.browser.fullVersion = '' + parseFloat(navigator.appVersion);
    jQuery.browser.majorVersion = parseInt(navigator.appVersion, 10);
    var nameOffset, verOffset, ix;

// In Opera, the true version is after "Opera" or after "Version"
    if ((verOffset = nAgt.indexOf("Opera")) != -1) {
        jQuery.browser.opera = true;
        jQuery.browser.name = "Opera";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 6);
        if ((verOffset = nAgt.indexOf("Version")) != -1)
            jQuery.browser.fullVersion = nAgt.substring(verOffset + 8);
    }
// In MSIE, the true version is after "MSIE" in userAgent
    else if ((verOffset = nAgt.indexOf("MSIE")) != -1 || (verOffset = nAgt.indexOf("Trident")) != -1) {
        jQuery.browser.msie = true;
        jQuery.browser.name = "Microsoft Internet Explorer";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 5);
    }
// In Chrome, the true version is after "Chrome"
    else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
        jQuery.browser.webkit = true;
        jQuery.browser.name = "Chrome";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 7);
    }
// In Safari, the true version is after "Safari" or after "Version"
    else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
        jQuery.browser.webkit = true;
        jQuery.browser.name = "Safari";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 7);
        if ((verOffset = nAgt.indexOf("Version")) != -1)
            jQuery.browser.fullVersion = nAgt.substring(verOffset + 8);
    }
// In Firefox, the true version is after "Firefox"
    else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
        jQuery.browser.mozilla = true;
        jQuery.browser.name = "Firefox";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 8);
    }
// In most other browsers, "name/version" is at the end of userAgent
    else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) <
        (verOffset = nAgt.lastIndexOf('/'))) {
        jQuery.browser.name = nAgt.substring(nameOffset, verOffset);
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 1);
        if (jQuery.browser.name.toLowerCase() == jQuery.browser.name.toUpperCase()) {
            jQuery.browser.name = navigator.appName;
        }
    }
// trim the fullVersion string at semicolon/space if present
    if ((ix = jQuery.browser.fullVersion.indexOf(";")) != -1)
        jQuery.browser.fullVersion = jQuery.browser.fullVersion.substring(0, ix);
    if ((ix = jQuery.browser.fullVersion.indexOf(" ")) != -1)
        jQuery.browser.fullVersion = jQuery.browser.fullVersion.substring(0, ix);

    jQuery.browser.majorVersion = parseInt('' + jQuery.browser.fullVersion, 10);
    if (isNaN(jQuery.browser.majorVersion)) {
        jQuery.browser.fullVersion = '' + parseFloat(navigator.appVersion);
        jQuery.browser.majorVersion = parseInt(navigator.appVersion, 10);
    }
    jQuery.browser.version = jQuery.browser.majorVersion;
})(jQuery);

///////////////////////////////////////////////////////////////////////////////
// jquery扩展方法
(function ($) {
    $.extend({
        alert: function (msg) {
            seajs.use("common-utils", function (Utils) {
                Utils.showDialog('温馨提示', '<p style="margin:20px;word-break: break-all;">' + msg + '</p>');
            });
        },

        confirm: function (msg, callback) {
            seajs.use("common-utils", function (Utils) {
                Utils.showDialog('温馨提示', '<p style="margin:20px;word-break: break-all">' + msg + '</p>', 0, 0, {
                    "确定": function () {
                        $(this).dialog("close");
                        if (typeof(callback) == "function") {
                            callback();
                        }
                    },
                    "取消": function () {
                        $(this).dialog("close");
                    }
                });
            });
        },

        // 判断是否来自手机的访问
        isFromMobile: function () {
            if (/AppleWebKit.*Mobile/i.test(navigator.userAgent) ||
                (/Android|webOS|iPhone|iPod|BlackBerry|MIDP|SymbianOS|NOKIA|SAMSUNG|LG|NEC|TCL|Alcatel|BIRD|DBTEL|Dopod|PHILIPS|HAIER|LENOVO|MOT-|Nokia|SonyEricsson|SIE-|Amoi|ZTE|Huawei/i.test(navigator.userAgent))) {
                return true;
            }
            return false;
        },

        getUrlParam: function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return decodeURIComponent(r[2]);
            return null;
        },

        getUrlParams: function () {
            var urlParams = {};
            var match,
                pl = /\+/g,  // Regex for replacing addition symbol with a space
                search = /([^&=]+)=?([^&]*)/g,
                decode = function (s) {
                    return decodeURIComponent(s.replace(pl, " "));
                },
                query = window.location.search.substring(1);

            while (match = search.exec(query)) {
                urlParams[decode(match[1])] = decode(match[2]);
            }
            return urlParams;
        },

        getHashParam: function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.hash.substr(1).match(reg);
            if (r != null) return decodeURIComponent(r[2]);
            return null;
        },

        closeWindow: function () {
            if ($.browser.msie) {
                window.opener = null;
                window.open("", "_self").close();
            }
            else {
                window.location.href = "about:blank";
                window.close();
            }
        },

        fmtDate: function (strTime) {
            var date = new Date(strTime);
            var month = date.getMonth() + 1;
            var day = date.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            return date.getFullYear() + "-" + month + "-" + day;
        }
    });
})(jQuery);

///////////////////////////////////////////////////////////////////////////////
var g_ajaxRequests = {};
$.ajaxPrefilter("json", function (options, originalOptions, jqXHR) {
    if (options.abortOnRetry) {
        if (g_ajaxRequests[options.url]) {
            g_ajaxRequests[options.url].abort();
        }
        g_ajaxRequests[options.url] = jqXHR;
    }
});

$(document).bind("ajaxStart.global", function () {
    seajs.use("common-utils", function (Utils) {
        Utils.showLoadingTip("正在提交请求，请稍候……", -1);
    });
});
$(document).bind("ajaxError.global", function (event, jqXHR, options, errorMsg) {
    seajs.use("common-utils", function (Utils) {
        Utils.showErrorTip("服务繁忙，请稍候重试");
    });
});
$(document).bind("ajaxStop.global", function () {
    seajs.use("common-utils", function (Utils) {
        Utils.hideLoadingTip();
    });
});

///////////////////////////////////////////////////////////////////////////////
// 自定义select框事件
function initSelectEvent() {
    // todo 统一select框!!
    $("body").bind("click", function (event) {
        if($(event.target).hasClass("chosew") || $(event.target).hasClass("chosebox") || $(event.target).hasClass("choseb")) {
            return;
        }
        $('.chosew ul').stop().slideUp();
        $('.chose_ratio ul').stop().slideUp();
        $('.chosebox').find('ul').stop().slideUp();
        $('.choseb ul').stop().slideUp();
        $(".chosew").removeClass("chosewp");
        $(".chosebox").removeClass("chosewp");
        $(".choseb").removeClass("chosewp");
    });

    $(".chosew input").click(function () {
        $(".chosew ul").stop().slideUp();
        $(this).parent().find('ul').stop(false, true).slideUp();
        $(this).parent().addClass("chosewp");
        $(this).parent().find('ul').stop(false, true).slideToggle();
        return false;
    });

    $(".chosew ul li").click(function () {
        if($(this).find("strong").length > 0 && $(this).parent().siblings('.relation_account').length > 0) { // 现金宝，特殊
            $(this).parent().siblings('.relation_account').html($(this).html()).show();
            $(this).parent().parent().find("input[type='text']").val(" ");
        }
        else {
            $(this).parent().parent().find("input[type='text']").val($(this).text());
        }
        $(this).parent().parent().find("input[type='text']").css({"color": "#040404"});
        $(this).parent().parent().find("input[type='hidden']").val($(this).attr("data-value"));
        $(this).addClass("check").siblings().removeClass("check");
    });

    $(".chose_ratio ul li").click(function () {
        $(this).parent().parent().find("input[type='text']").val($(this).text());
        $(this).parent().parent().find("input[type='hidden']").val($(this).attr("data-value"));
        $(this).parent().parent().find("input[type='text']").css({"color": "#040404"});
        $(this).addClass("check").siblings().removeClass("check");
    });

    //////////////////////////////////////////////////////////////
    $('.chosebox input').click(function () {
        $('.chosebox').find('ul').stop().slideUp();
        $(this).parent().find('ul').stop(false, true).slideUp();
        $(this).parent().find('ul').stop(false, true).slideToggle();
        $(this).parent().addClass("chosewp");
        return false;
    });

    $(".chosebox ul li").click(function () {
        $(this).parent().parent().find('input[type="text"]').val($(this).text());
        $(this).parent().parent().find('input[type="text"]').css({"color": "#040404"});
        $(this).addClass('check').siblings().removeClass('check');
        $(this).parent().parent().find('input[type="hidden"]').val($(this).attr("data-value"));
    });

    $(".chosebox input").blur(function () {
        $(this).parent().removeClass("chosewp");
    });

    ///////////////////////////////////////////////////////////////
    //主要超级现金宝页面choseb
    $('.choseb input').click(function () {
        $('.choseb ul').stop().slideUp();
        $('.choseb>input').next('ul').stop(false, true).slideUp();
        $(this).next('ul').stop(false, true).slideToggle();
        $(this).parent().addClass("chosewp");
        return false;
    });

    $(".choseb ul li").click(function () {
        //$(this).parent().prev('input').val($(this).text());
        $(this).parent().parent().find('input[type="hidden"]').val($(this).attr("data-value"));
        $(this).parent().parent().find('input[type="text"]').val($(this).text());
        
        $(this).parent().parent().find('input[type="text"]').css({"color": "#040404"});
        $(this).addClass('check').siblings().removeClass('check');
    });

    $(".choseb input").blur(function () {
        $(this).parent().removeClass("chosewp");
    });
}
$(function () {
    initSelectEvent();
});

///////////////////////////////////////////////////////////////////////////////
$(function () {
    //input 框输入，文字颜色变深,字体变大
    //绑定聚焦和移出处焦点事件
    $('.minput input[type="text"], .maxput input[type="text"]').bind('focus', function () {
        if ($(this).prop("readonly") || $(this).hasClass("datepicker")) {
            if ($(this).prop("readonly")) {
                $(this).blur();
            }
            return;
        }

        $(this).css({"color": "#040404", "border":"1px solid #3c9ad5"});
    }).bind('blur', function () {
        if ($(this).prop("readonly") || $(this).hasClass("datepicker")) {
            return;
        }

        var txt_value = $(this).attr('placeholder');
        var thisval = this.defaultValue || $(this).get(0).value;
        if (thisval == '') {
            $(this).css({"color": "#A3A2A2", "border":"1px solid #afafaf"});
        } else {
            $(this).css({"color": "#040404", "border":"1px solid #afafaf"});
        }
    });

    $('input[type="password"][class*="keyboard"]').bind("focus click", function (e) {
        var input = this;
        seajs.use("soft_keyboard", function(keyboard) {
            if(!$(input).is(":hidden")) { // 兼容低版本IE浏览器，placeholder组件带来的副作用
                keyboard.show(input);
            }
        });
    });

    // 清掉chrome下自动记住密码
    $('input[type="password"]').each(function() {
        if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
            $(this).append($(this).prop("outerHTML"));
            $(this).find("input").removeAttr("id");
            $(this).find("input").removeAttr("name");
            $(this).before('<input style="display: none;" type="password"/>');
        }
    });

    //文本框只能输入数字
    $(".onlyNum").keyup(function (event) {
        var keyCode = event.keyCode;
        if (keyCode < 37 || keyCode > 40){
            $(this).val($(this).val().replace(/[^\d]/g,''));
        }
    });

    // /^(([0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{0,2})))$/

    $('.onlyMoney').keydown(function () {
        if($(this).val().match(/^(0|0\.|0\.0|[1-9]\d*|[1-9]\d*\.\d{1,2}|0\.[0-9]{1}[1-9]{1}|0\.[1-9]{1})$/)) {
            $(this).attr("t_value", $(this).val());
        }
        else {
            $(this).val($(this).attr("t_value"));
        }
    }).keyup(function () {
        if($(this).val().endWith("\\.")) {
            $(this).val($(this).val().replace(/\.+$/g, "."));
        }

        if ($(this).val().length == 0 || ($(this).val().endWith("\\.") && $(this).val().length > 1) || $(this).val().match(/^(0|0\.|0\.0|[1-9]\d*|[1-9]\d*\.\d{1,2}|0\.[0-9]{1}[1-9]{1}|0\.[1-9]{1})$/)) {
            $(this).attr("t_value", $(this).val());
        } else {
            $(this).val($(this).attr("t_value"));
        }
    }).blur(function() {
        if($(this).val().endWith("\\.")) {
            $(this).val($(this).val().substr(0, $(this).val().length-1));
        }
    });
    
    //协议
    $('.agreement').click(function(){
        $(this).find('.ckb').toggleClass('ckbed');
    })
});

///////////////////////////////////////////////////////////////////////////////
// 公共的页面片段
var NODATA_COUPON_TEMPLATE = '<div class="no_data"><strong></strong><p>目前暂无相关活动，敬请期待。</p></div>';
var NODATA_TEMPLATE = '<div class="no_data"><strong></strong><p>暂无相关记录</p></div>';
var ERROR_TEMPLATE = '<div class="error_area"><strong></strong><a href="javascript:;" onclick="location.reload();return false;">刷新页面</a></div>';
var LOADING_TEMPLATE = '<div class="loading_area">正在响应您的请求，请稍候……<br/><div class="load_img"></div></div>';

///////////////////////////////////////////////////////////////////////////////
// 左右对齐
setInterval(function () {
    if ($(".left_nav").length == 0 || $(".height_main").length == 0) {
        return;
    }

    var minLeftHeight = 50;
    $(".left_nav ul").each(function () {
        minLeftHeight += $(this).height();
    });

    var rightHeight = $(".height_main").height() + parseInt($(".height_main").css("border-top-width")) + parseInt($(".height_main").css("padding-bottom"));
    if (rightHeight - parseInt($(".left_nav").css("padding-bottom")) > minLeftHeight) {
        $(".left_nav").height(rightHeight - parseInt($(".left_nav").css("padding-bottom")));
    }
    else {
        $(".left_nav").height(minLeftHeight + parseInt($(".left_nav").css("padding-bottom")));
    }
}, 10);

///////////////////////////////////////////////////////////////////////////////
// 组合情况
//1、数据中心有数据：old、ds_agency、counter、agency
//2、直销有数据：new、old、counter、ds_agency
//3、直代销均有数据：old、ds_agency、counter
//4、直代销均无数据：phone
var CUST_TYPE = {
    PHONE: "phone", NEW: "new", OLD: "old", COUNTER: "counter", AGENCY: "agency", DS_AGENCY: "ds_agency"
};

// 基金类型（根据网上交易表tfundinfo_web表中的定义）
var FUND_TYPES = {
	"00":"货币型",
	"01":"债劵型",
	"02":"股票型",
	"03":"保本型",
	"04":"混合型",
	"05":"指数型",
	"06":"QDII",
	"07":"偏股型",
	"08":"专户",
	"09":"短期理财",
	"100":"热门",
	"UNKNOWN":"未知类型"
};
// 份额类别
var SHARETYPES = {
	"A":"前收费",
	"B":"后收费"
};


var CustInfoHelper = {
    isNeedVerify: function (type) {
        if (type == CUST_TYPE.PHONE || type == CUST_TYPE.COUNTER || type == CUST_TYPE.AGENCY) {
            return true;
        }
        return false;
    },

    isExistDc: function (type) {
        if (type == CUST_TYPE.OLD || type == CUST_TYPE.COUNTER || type == CUST_TYPE.AGENCY || type == CUST_TYPE.DS_AGENCY) {
            return true;
        }
        return false;
    },

    isExistDs: function (type) {
        if (type == CUST_TYPE.NEW || type == CUST_TYPE.COUNTER || type == CUST_TYPE.OLD || type == CUST_TYPE.DS_AGENCY) {
            return true;
        }
        return false;
    }
};
