seajs.use(['common-utils', 'template'], function (Utils, templates) {
    $Util = Utils;
    template = templates;
});
//详情页面展示
function initDetailPage(data, html, paytype) {
    var render = template.compile(html);
    $(".layer").html(render({"result": data})).fadeIn();
    $("#detail_layer").fadeIn();
    $("#detail_layer").css('margin-top', -($("#detail_layer").innerHeight() / 2).toFixed(0) + 'px');

    if (paytype == '汇款支付') {
        $('.divText').show();
        $('.divText').html("温馨提示：您该笔申购交易申请是通过汇款方式支付的，撤单后，您汇入我司的资金将作为可用余额保留在您的交易账户中。您可通过“我的银行卡->汇款支付”菜单进行“取现”操作后，我司将在下一工作日将资金划往您的银行卡。");
        $("#deal_layer").css("cssText", "width:600px!important;height:350px!important;");
    }

    if (paytype == '支付账户' && (data.businno == '020' || data.businno == '022')) { // 认申购
        $('.divText').show();
        $('.divText').html("温馨提示：<span style='font-size:14px;color:red'>撤单成功后资金将于T+1个工作日划出</span>");
        $("#deal_layer").css("cssText", "width:600px!important;height:300px!important;");
    }

    //关闭弹层
    $(".tc_close,.tc_btn a.cancel").click(function () {
        $(".layer-box").fadeOut();
        $(".layer").fadeOut();
        $('.divText').hide();
        $("#deal_layer").css("cssText", "width:600px!important;height:300px!important;");
    });
//    $(".tc_bg").click(function () {
//        $(".layer-box").fadeOut();
//        $(".layer").fadeOut();
//        $('.divText').hide();
//        $("#deal_layer").css("cssText", "width:600px!important;height:300px!important;");
//    });

    //再次购买
    $("a.againPay").click(function () {
        location.href = $Util.generUrl($Util.getContextPath() + "/go?fundcode=" + $(this).attr("fundcode") +
            "&menuId=" + ($(this).attr("fundstatus") == 1 ? 30500 : 30400));
    });

    //组合投资再次购买
    $("a.combdeclare").click(function () {
        location.href = $Util.generUrl($Util.getContextPath() + "/trade/combin/declare/init?menuId=60000&fundcode=" + $(this).attr("combcode"));
    });
    //组合投资赎回
    $("a.combredeem").click(function () {
        location.href = $Util.generUrl($Util.getContextPath() + "/trade/combin/redeem/init?menuId=60000&fundcode=" + $(this).attr("combcode")+"&tradeacco="  + $(this).attr("tradeacco"));
    });
    //组合投资调仓
    $("a.combfundadjust").click(function () {
        location.href = $Util.generUrl($Util.getContextPath() + "/trade/combin/fundadjust/init?menuId=60000&fundcode=" + $(this).attr("combcode")+"&tradeacco="  + $(this).attr("tradeacco"));
    });
    $("#combUrl").click(function () {
    	window.open($Util.generUrl($Util.getContextPath() + "/trade/combin/risk/combinDetail?menuId=60000&combcode=" + $(this).attr("combcode")));
    });
    //超级现金宝再次购买
    $("a.superAgainPay").click(function () {
        location.href = $Util.generUrl($Util.getContextPath() + "/go?menuId=20100");
    });

    //组合投资再次购买
    /*$("a.combAgainPay").click(function () {
        location.href = $Util.generUrl($Util.getContextPath() + "/go?menuId=30200");
    });*/

    //撤单弹层
    $(".withdraw").on('click', function () {
        $("#detail_layer,#failure_layer").hide();
        $("#deal_layer").fadeIn();
    });

    //撤单操作
    $(".dealOK").on("click", function () {
        var pwd = $("#password").val();
        if (pwd.length == 0) {
            $Util.showNoticeTip("请输入交易密码", 2000);
            $("#password").focus();
            return false;
        }
        $("#deal_layer").fadeOut();
        $(".layer-box").fadeOut();
        $(".layer").fadeOut();
        var $tradeacco = $("#tradeacco").val();				//交易账号
        var $allotno = $("#allotno").val();					//申请单编号
        var original_appno = $("#original_appno").val();	//原申请单编号
        var $combRequestNo = $("#combRequestNo").val();		//组合申请编号
        $('.divText').hide();
        $("#deal_layer").css("cssText", "width:500px!important;height:300px!important;");
        dealWidthdraw($tradeacco, $allotno, original_appno, pwd, $combRequestNo);
    });

    $(".tc_btn a.sucOK").click(function () {
        $("#deal_layer").hide();
        $(".layer").fadeOut();
        $("#success_layer").fadeOut();
        $('.divText').hide();
        $("#deal_layer").css("cssText", "width:500px!important;height:300px!important;");
        location.reload();
    });
}

//处理撤单操作
function dealWidthdraw(tradeacco, allotno, originalno, pwd, combno) {
    $Util.simpleAjax(
        $Util.generUrl($Util.getContextPath() + "/trade/widthdraw/result"),
        {
            "tradeacco": tradeacco,
            "original_appno": originalno,
            "allotno": allotno,
            "password": pwd,
            "combRequestNo": combno
        },
        function (result) {
            if (!result.success) {
                $Util.showErrorTip(result.errormessage, 3000);
                return;
            }

            //撤单成功，显示撤单成功弹层
            $("#deal_layer").hide();
            $(".layer").fadeIn();
            $("#success_layer").fadeIn();

        },
        function (error) {
            $Util.showErrorTip("系统异常，请稍候再试", 2000);
        }
    );
}

//获取组合申请信息
function getCombRequestData(comb_request_no, businflag) {
    $Util.simpleAjax(
        $Util.generUrl($Util.getContextPath() + "/assets/traderecord/combrequest"),
        {"comb_request_no": comb_request_no, "businflag": businflag},
        function (result) {
            if (result.success) {
                var combdata = $.parseJSON(result.addmap);
                $Util.loadHtml($Util.getContextPath() + "/views/assets/traderecord/apply_detail.htm", function (html) {
                    var data = {
                        "combflag": "1",
                        "fundname": combdata["fundname"],
                        "applybalance": combdata["applybalance"],
                        "applyshare": "",
                        "busintype": combdata["busintype"],
                        "businno":combdata["businno"],
                        "businasscode":combdata["businasscode"],
                        "comb_request_no": combdata["combno"],
                        "tradedate": combdata["tradedate"],
                        "sharetype": combdata["sharetype"],
                        "paytype": combdata["payway"],
                        "bankaccount": combdata["bankacco"],
                        "redeem_exceed_flag": combdata["redeem_exceed_flag"],
                        "paystatus": combdata["paystate"],
                        "tradestatus": combdata["confirmstate"],
                        "iswithdraw": combdata["isWithdraw"],
                        "oppo_fund_account": "",
                        "oppo_tradeacco": "",
                        "oppo_agency": "",
                        "oppo_netno": "",
                        "remark": "",
                        "tradeacco": combdata["tradeacco"],
                        "list": combdata["list"],
                        "index": combdata["index"],
                        "balancecount": combdata["balancecount"],
                        "fixtradedate": combdata["fixtradedate"]
                    };

                    initDetailPage(data, html, combdata["payway"]);
                });
            } else {
                $Util.showErrorTip(result.errormessage);
            }
        }, true,
        function (error) {
            $Util.showErrorTip("组合申请信息查询失败，请稍候再试");
        }
    );
}

//组合投资申请
function getCombnRequestData(businflag,$this) {
    $Util.simpleAjax(
        $Util.generUrl($Util.getContextPath() + "/assets/traderecord/combnrequest"),
        {"comb_request_no": $this.attr("comb_request_no"), "businflag": businflag,
         "comb_code":$this.attr("combcode"),"applybalance":$this.attr("applybalance")=="-"?"":$this.attr("applybalance"),
         "busincode":$this.attr("businno"),"curratio":$this.attr("curratio"),"newratio":$this.attr("newratio")},
        function (result) {
            if (result.success) {
                var combdata = $.parseJSON(result.addmap);
                var detailmap = $.parseJSON(result.detailmap);
                $Util.loadHtml($Util.getContextPath() + "/views/assets/traderecord/combin_detail.htm", function (html) {
                    var data = {
                        "name": $this.attr("fundname"),
                        "combcode":$this.attr("combcode"),
                        "busincode":$this.attr("businno"),
                        "allotno":$this.attr("allotno"),
                        "redeemScale":$this.attr("redeemScale"),
                        "original_appno":$this.attr("original_appno"),
                        "sourceTradeacco":$this.attr("sourceTradeacco"),
                        "balance": $this.attr("applybalance")=="-"?"":$this.attr("applybalance"),
                        "busintype": $this.attr("busintype"),
                        "comb_request_no": $this.attr("comb_request_no"),
                        "bankaccount": $this.attr("bankaccount"),
                        "autobuy": $this.attr("auto_buy_name"),
                        "paystate": $this.attr("paystatus"),
                        "confirmstate": $this.attr("tradestatus"),
                        "iswithdraw": $this.attr("iswithdraw"),
                        "list": combdata["shareQueryList"],
                    };

                    initDetailPage(data, html, $this.attr("paytype"));
                });
                //调仓明细
                $(document).on('click','.tcmx',function(){
                    $("#detail_layer").hide();
                    $Util.loadHtml($Util.getContextPath() + "/views/assets/traderecord/fundadjust_detail.htm", function (html) {
                        var data = {
                            "list": detailmap["shareQueryDetailList"],
                        };
                        
                        initDetailPage(data, html, $this.attr("paytype"));
                    });
                });
            } else {
                $Util.showErrorTip(result.errormessage);
            }
        }, true,
        function (error) {
            $Util.showErrorTip("组合申请信息查询失败，请稍候再试");
        }
    );
}

function getTradeRequestData(allot_no, fund_busin_code) {
    $Util.simpleAjax(
        $Util.generUrl($Util.getContextPath() + "/assets/traderecord/tradeQueryDetail"),
        {"allot_no": allot_no, "fund_busin_code": fund_busin_code},
        function (result) {
            if (result.success) {
                $Util.loadHtml($Util.getContextPath() + "/views/assets/traderecord/apply_detail.htm", function (html) {
                    var data = {
                        "allotno": result.applyBean.allotno, "fundcode": result.applyBean.fundcode, "fundname": result.applyBean.fundname,
                        "businno": result.applyBean.businno, "targetcode": result.applyBean.targetfundcode, "applyshare": result.applyBean.applyshare == "null" ? "-" : result.applyBean.applyshare,
                        "applybalance": result.applyBean.applybalance == "null" ? "-" : result.applyBean.applybalance, "tradedate": result.applyBean.tradedate,
                        "applytime": result.applyBean.applytime, "paytype": result.applyBean.paytype, "bankaccount": result.applyBean.bankaccount,
                        "paystatus": result.applyBean.paystatus, "tradestatus": result.applyBean.tradestatus, "original_appno": result.applyBean.original_appno,
                        "original_date": result.applyBean.original_date, "tradeacco": result.applyBean.tradeacco, "remark": result.applyBean.remark,
                        "sharetype": result.applyBean.sharetype, "fundstatus": result.applyBean.fundstatus, "applytime": result.applyBean.applytime,
                        "targetfund": result.applyBean.targetfundname, "redeem_exceed_flag": result.applyBean.redeem_exceed_flag, "iswithdraw": result.applyBean.isWithdraw,

                        "after_discount_rate": result.applyBean.after_discount_rate, "busin_board_type": result.applyBean.busin_board_type,
                        "detail_fund_way": result.applyBean.detail_fund_way, "discount_rate": result.applyBean.discount_rate,
                        "busintype": result.applyBean.busintype, "expiry_date": result.applyBean.expiry_date, "first_exchdate": result.applyBean.first_exchdate,
                        "fund_froze_flag": result.applyBean.fund_froze_flag, "fund_risk_flag": result.applyBean.fund_risk_flag, "comb_request_no": result.applyBean.comb_request_no,
                        "receivable_account": result.applyBean.receivable_account, "reserve_discount_rate": result.applyBean.reserve_discount_rate,
                        "range": result.applyBean.range, "trade_period": result.applyBean.trade_period, "oppo_agency": result.applyBean.oppo_agency,
                        "oppo_fund_account": result.applyBean.oppo_fund_account, "oppo_netno": result.applyBean.oppo_netno, "oppo_tradeacco": result.applyBean.oppo_tradeacco,
                        "issupermaket": result.applyBean.issupermaket, "detailflag": result.applyBean.detailFlag, "fixtradedate": result.applyBean.fixtradedate,
                        "auto_buy_name": result.applyBean.auto_buy_name, "auto_buy": result.applyBean.auto_buy,
                        "coupon_no":result.applyBean.coupon_no,"coupon_amount":result.applyBean.coupon_amount,"actual_pay_amount":result.applyBean.actual_pay_amount
                    };

                    initDetailPage(data, html, result.applyBean.paytype);
                });
            } else {
                $Util.showErrorTip(result.errormessage);
            }
        }, true,
        function (error) {
            $Util.showErrorTip("组合申请信息查询失败，请稍候再试");
        }
    );
}