//弹出对话框 start-----------
var EV_MsgBox_ID=""; 
//重要    
//弹出对话窗口(msgID-要显示的div的id) 
function 
  EV_modeAlert(msgID)
 {  
    //创建大大的背景框   
    var bgObj=document.createElement("div");  
    bgObj.setAttribute('id','EV_bgModeAlertDiv');  
    document.body.appendChild(bgObj);  
    //背景框满窗口显示   
    EV_Show_bgDiv();  
    //把要显示的div居中显示   
   EV_MsgBox_ID=msgID;  
   EV_Show_msgDiv();  
}    
//关闭对话窗口   
function EV_closeAlert(){  
    var msgObj=document.getElementById(EV_MsgBox_ID);  
    var bgObj=document.getElementById("EV_bgModeAlertDiv");  
    msgObj.style.display="none";  
    document.body.removeChild(bgObj);  
    EV_MsgBox_ID="";  
}  
  
//窗口大小改变时更正显示大小和位置   
window.onresize=function(){  
    if (EV_MsgBox_ID.length>0){  
        EV_Show_bgDiv();  
        EV_Show_msgDiv();  
    }  
}  


var investmentTypeOpen = $("#investmentTypeOpen").val();
if(investmentTypeOpen=='true'){
	$("#investmentType_layer").fadeIn();
	$("#investmentTypeOpenDiv").fadeIn();
}else{
	$("#investmentType_layer").fadeOut();
	$("#investmentTypeOpenDiv").fadeOut();
}

$("#investmentType_close").click(function () {
	$("#investmentType_layer").fadeOut();
	$("#investmentTypeOpenDiv").fadeOut();
	
});
//窗口滚动条拖动时更正显示大小和位置   
window.onscroll=function(){  
    if (EV_MsgBox_ID.length>0){  
       EV_Show_bgDiv();  
        EV_Show_msgDiv();  
   }  
}  
  
//把要显示的div居中显示   
function EV_Show_msgDiv(){  
    var msgObj   = document.getElementById(EV_MsgBox_ID);  
    msgObj.style.display  = "block";  
    var msgWidth = msgObj.scrollWidth;  
    var msgHeight= msgObj.scrollHeight;  
    var bgTop=EV_myScrollTop();  
    var bgLeft=EV_myScrollLeft();  
    var bgWidth=EV_myClientWidth();  
    var bgHeight=EV_myClientHeight();  
    var msgTop=bgTop+Math.round((bgHeight-msgHeight)/2);  
    var msgLeft=bgLeft+Math.round((bgWidth-msgWidth)/2);  
    msgObj.style.position = "absolute";  
    msgObj.style.top      = msgTop+"px";  
    msgObj.style.left     = msgLeft+"px";  
    msgObj.style.zIndex   = "10001";  
      
}  
//背景框满窗口显示   
function EV_Show_bgDiv(){  
    var bgObj=document.getElementById("EV_bgModeAlertDiv");  
    var bgWidth=EV_myClientWidth();  
    var bgHeight=EV_myClientHeight();  
    var bgTop=EV_myScrollTop();  
    var bgLeft=EV_myScrollLeft();  
    bgObj.style.position   = "absolute";  
    bgObj.style.top        = bgTop+"px";  
    bgObj.style.left       = bgLeft+"px";  
    bgObj.style.width      = bgWidth + "px";  
    bgObj.style.height     = bgHeight + "px";  
    bgObj.style.zIndex     = "10000";  
    bgObj.style.background = "#000";  
    bgObj.style.filter     = "progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=80,finishOpacity=80);";  
    bgObj.style.opacity    = "0.8";  
}  
//网页被卷去的上高度   
function EV_myScrollTop(){  
    var n=window.pageYOffset   
    || document.documentElement.scrollTop   
    || document.body.scrollTop || 0;  
    return n;  
}  
//网页被卷去的左宽度   
function EV_myScrollLeft(){  
    var n=window.pageXOffset   
   || document.documentElement.scrollLeft   
       || document.body.scrollLeft || 0;  
    return n;  
}  
//网页可见区域宽   
function EV_myClientWidth(){  
    var n=document.documentElement.clientWidth   
    || document.body.clientWidth || 0;  
    return n;  
}  
//网页可见区域高   
function EV_myClientHeight(){  
    var n=document.documentElement.clientHeight   
    || document.body.clientHeight || 0;  
   return n;  
}
//弹出对话框 end-----------

//添加cookie
function setCookie(c_name,value,expiredays){
    var exdate = new Date()
    exdate.setDate(exdate.getDate()+expiredays)
    cookieVal = c_name + "=" + escape(value) + ((expiredays == null) ? "" : ";expires="+exdate.toGMTString());
    document.cookie=cookieVal;
}
//获取cookie
function getCookie(c_name){
    if (document.cookie.length > 0){
      c_start=document.cookie.indexOf(c_name + "=")
      if (c_start != -1){ 
        c_start = c_start + c_name.length+1 
        c_end = document.cookie.indexOf(";",c_start)
        if (c_end == -1) {
        	c_end = document.cookie.length
        }
    	return unescape(document.cookie.substring(c_start,c_end))
      } 
    }
    return ""
}

//顶部广告tip滚动
$(function () {
    //如果少于两条公告，则无需加动画效果。
    if ($(".news .word").find("label").length > 1) {
        function AutoScroll(obj) {
            $(obj).animate({
                marginTop: "-24px"
            }, 500, function() {
                $(this).css({ marginTop: "0px" }).find("label:first").appendTo(this);
            });
        }

        var interval = setInterval(function() {
            AutoScroll(".news .word");
        }, 3000);
        $(".news .word").hover(function () {
            clearInterval(interval);
        }, function () {
            interval = setInterval(function() {
                AutoScroll(".news .word");
            }, 3000)
        });
    }

    if(CustInfoHelper.isNeedVerify(NF_CUST_TYPE)) {
        $("#verifyLink").parent().removeClass("none");
    }
    else {
        $("#reserveWords").parent().removeClass("none");
    }
});



//针对老用户没有注册手机用户的，现在一定要填写完善手机的资料
//获取手机验证码
$("#addUserInfo_Code").click(function(){
	 var $this = $(this);
     if($this.hasClass("btn_disable")) {
         return false;
     }

     seajs.use(['validation-utils', 'common-utils'], function (validation, Utils) {
         var data = null;
         var url = Utils.generUrl(Utils.getContextPath() + "/account/ajax/sendMobileCodeAfterLoginNew");
             var mobile = $("#addUserMobile_val").val().trim();

             if (mobile == "") {
                 Utils.showNoticeTip("请输入新手机号码");
                 return;
             }
             if (!validation.isMobile(mobile)) {
                 Utils.showNoticeTip("请输入正确的手机号码");
                 return;
             }
             data = {
             	"mobile": mobile,
             	"isnewmobile": "isnew",
             	"imgcode":$("#accecode").val()
             };

         Utils.simpleAjax(url, data, function (result) {
             if (result.success) {
             	_paq.push(['trackEvent', "验证码","获取短信验证码" ,""]); 
                 Utils.showSuccessTip("手机验证码已发送成功，请输入序号为【"+result.serialNo+"】的短信验证码",5000);
                 $this.removeClass("btn1");
                 $this.addClass("btn_disable");
                 var timeout = 60;
                 var interval = setInterval(function () {
                     timeout--;
                     if (timeout < 1) {
                         clearInterval(interval);
                         $this.addClass("btn1");
                         $this.removeClass("btn_disable");
                         $this.text("获取验证码");
                     }
                     else {
                         $this.text(timeout + "秒后重新获取");
                     }
                 }, 1000);
             } else {
                 Utils.showErrorTip(result.errormessage);
					$("#loginForm_img_authcode").trigger("click");

             }
         });
     });
     return false;
	
})
 seajs.use([ 'validation-utils', 'common-utils' ], function(validation, Utils) {
	$("#understand").click(
			function() {
				var data = null;
				var url = Utils.generUrl(Utils.getContextPath()
						+ "/account/main/eventrecording");
				Utils.simpleAjax(url, data, function(result) {
					if (!result.success) {
						Utils.showErrorTip(result.errormessage);
					}else{
				    	location.href = Utils.generUrl(Utils.getContextPath() + "/account/main/init");
					}
				});
			})
	$("#conversion").click(function() {
		$("#investmentType_layer").fadeIn();
		$("#investmentTypeOpenDiv").fadeOut();
		$("#deal_layer").fadeIn();
	});
	$("#deal_close").click(function() {
		$("#investmentType_layer").fadeOut();
	});
	$("#btnSubmit").click(
			function() {
				Utils.simpleAjax(Utils.generUrl(Utils.getContextPath()
						+ "/usercenter/personinfo/transInvestor"), {
					pwd : $("#password").val(),
					type :'2'
				}, function(data) {
					if (data.success) {
						$("#deal_layer").fadeOut();
						$("#investmentType_layer").fadeIn();
						$("#zh_succ_layer").fadeIn();
						return;
					}
					Utils.showErrorTip(data.errormessage);
				}, function(error) {
					Utils.showErrorTip("申请转换失败，请稍候再试");
				});
			});
	 $("#btnOk").click(function(){
	    	location.href = Utils.generUrl(Utils.getContextPath() + "/account/main/init");
	    });
})
// 确认添加用户资料
$("#addUserMobile").click(function() {
        seajs.use(['validation-utils', 'common-utils'], function (validation, Utils) {
        	location.hash = "cust_type=new&mask_mobile=&pwd_type=TP";
            var mobile = $('#addUserMobile_val').val();
            if (mobile.length == 0) {
                Utils.showNoticeTip("请输入新手机号码");
                return;
            }
            if (!validation.isMobile(mobile)) {
                Utils.showNoticeTip("请输入正确的新手机号码");
                return;
            }

            var authCode = $('#addMobileLoginDlg_CodeInput').val();
            if (authCode.length == 0) {
                Utils.showNoticeTip("请输入短信验证码");
                return;
            }
            if (authCode.length < 6) {
                Utils.showNoticeTip("请输入正确的短信验证码");
                return;
            }
            
            var pwdType = $.getHashParam("pwd_type");
            var custType = $.getHashParam("cust_type");
            changemobile(mobile,authCode);
    
            function changemobile(mobile,authCode,password) {
            	_paq.push(['trackEvent', Piwik_Event_Quota.modify_mobile.category,Piwik_Event_Quota.modify_mobile.commit.action ,""]); // 页面需要已经加载piwik.js
            	Utils.simpleAjax(Utils.generUrl(Utils.getContextPath()+"/usercenter/personinfo/changemobile"), {
                    newMobile: mobile,
                    authCode: authCode,
                    password: password
                }, function (data) {
                    if (data.success) {
                    	_paq.push(['trackEvent', Piwik_Event_Quota.modify_mobile.category,Piwik_Event_Quota.modify_mobile.succ.action ,""]); // 页面需要已经加载piwik.js
                        Utils.showSuccessTip(data.errormessage);
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    } else {
                    	_paq.push(['trackEvent', Piwik_Event_Quota.modify_mobile.category,Piwik_Event_Quota.modify_mobile.fail.action ,data.errormessage]); // 页面需要已经加载piwik.js
                        $('#addMobileLoginDlg_CodeInput').val("");
//                        $("#fillInTradePwd").fadeOut();
//                        $(".layer").fadeOut();
                        Utils.showErrorTip(data.errormessage);
                    }
                    var customData = "{'type':'modifymobile(更换手机)', 'info': " +
    				"{'"+Piwik_CustomField.mobile_old+"':'"+$("#changeMobileOldDlg_MobileInput").val()+"'," +
    				"'"+Piwik_CustomField.mobile_new+"'：'"+$('#changeMobileNewDlg_MobileInput').val()+"'," +
    				"'"+Piwik_CustomField.authcode_mobile+"':'"+$("#changeMobileNewDlg_CodeInput").val()+"'}}";
    	    		_paq.push(['setCustomData',customData]);
                });
            }
        });
        return false;
    });

// 各种tips
seajs.use(['common-utils','template'], function (Utils,template) {
    var waitstr = "<p style='display: inline;border: none'><img style='vertical-align: middle' src='" + Utils.getContextPath() + "/resources/images/loading.gif'/></p>";

    (function advertPopWin() { // 广告弹窗
        var isProhibitAdvertPop = Utils.cookie("isProhibitAdvertPop");
        if (isProhibitAdvertPop != "true") {
            $(".ad_layer").removeClass("none");

            $("#adCloseBtn").click(function () {
                $(".ad_layer").css("display", "none");
                if ($("#prohibitAdPopBtn").is(":checked")) {
                    Utils.cookie("isProhibitAdvertPop", true, {"expires": 30});
                }
            });
        }
    })();
    $(".staycurrentpage").click(function(){
    	$(".couponlayer").hide();
	    $("#success_layer").hide();
	    window.location.reload();
    });
    //满减券的js事件
    $(".asset_kq .Redeem").click(function(){
    	var fund_codenamelist =$(this).attr("fund_codenamelist");
    	var fundcodestr =$(this).attr("fundcodestr");
    	var fund_codeName =$(this).attr("fund_codeName");
    	var balance =$(this).attr("balance");
    	var expriedDate=$(this).attr("expriedDate");
    	var exchange_EndTime=$(this).attr("exchange_EndTime");
    	var score =$(this).attr("score");
    	var min_threshold=$(this).attr("min_threshold");
    	var coupon_operation_id=$(this).attr("coupon_operationId");
    	var couponId=$(this).attr("couponId");
    	$(".amount").text(balance);
    	$(".index_use_condition").text("单笔投资满"+min_threshold+"可用");
    	if(typeof(exchange_EndTime)!=undefined && exchange_EndTime!=null){
    		$(".use_escription .coupon_expried").text("有效期：截至"+exchange_EndTime+"有效");
    	}else{
    		$(".use_escription .coupon_expried").text("领取后"+expriedDate+"天内有效");
    	}
    	$(".mjq_int sapn").text(score);
    	$("#makesureExchangecoupon").attr({"couponoperationId":coupon_operation_id,"couponId":couponId});
    	var flag =0;
    	if(fundcodestr==undefined){
    		flag=1;
    	}else{
			var fundcodesize = $(this).attr("fundcodestr").split(",").length;
		}
		if(flag==1){
			$(".use_escription .coupon_produce").text("适用范围：所有基金产品");
		}else if(fundcodesize==1){
			$(".use_escription .coupon_produce").text("适用范围："+fund_codeName);
		}else{
			$(".use_escription .coupon_produce").html("适用范围：<a class='showappointfund' href='javascript:;'>指定基金产品</a>");
			$(".showappointfund").attr("nameandcode",fund_codenamelist);
		}
		$(".used_coupon_score").text(score);
    	$(".couponlayer").fadeIn();
    	$("#coupon_deal_layer").fadeIn();
    });
    
    $(document).on("click",".showappointfund",function(){
    	var nameandcode=$(this).attr("nameandcode");
    	$(".appoint_coupon_content").text(nameandcode);
    	$("#index_usingcoupon_layer").fadeIn();
    	$(".couponlayer").fadeIn();
    });
    $("#makesureExchangecoupon").click(function(){
    	 var self =this;
    	 var passwordcoupon=$("#passwordcoupon").val();
    	 var couponId= $(this).attr("couponId");
		 var couponoperationId= $(this).attr("couponoperationId");
		 if(passwordcoupon==null){
			 comUtils.showDialog("温馨提示", "交易密码不能为空");
			 return;
		 }
		 if($(this).attr("disabled") != undefined) {
				return false;
			}
		 $(self).attr("disabled", "disabled");
		 var requestUrl = Utils.generUrl(Utils.getContextPath() + "/usercenter/mywelfare/immediatelyExchange");
		 var ajaxParam={
				 "couponId":couponId,
				 "couponoperationId":couponoperationId,
				 "password_coupon":passwordcoupon
		 }
		 Utils.simpleAjax(requestUrl, ajaxParam, function(data){
			 if(!data.success){
				 if(data.errorcode == "DS-TR3Q0102"){
					 $(".passworderror").text(data.errormessage);
				 } else if(data.errorcode == "DS-TR3QP00Y"){
                     $(".passworderror").text(data.errormessage);
                 } else{
					 $(".couponlayer").show();
					 $("#coupon_deal_layer").hide();
				     $("#success_layer").show();
				     $(".exchange_callback_fail").show().text(data.errormessage);
				     $(".exchange_callback").hide();
				 }
			 }else{
				 $(".couponlayer").show();
				 $("#coupon_deal_layer").hide();
			     $("#success_layer").show();
			     $(".exchange_callback_fail").hide();
			     $(".exchange_callback").show().text("兑换成功");
			 }
			 $(self).removeAttr("disabled");
		 });
    });
    
    $(".close_couponinfo").click(function(){
    	 $("#index_usingcoupon_layer").hide();
    });
    $(".tc .closebtn a, .tc .box1 .tc_close,.coupon_close,.coupon_cancel,.coupon_callback").click(function () {
        $(".tc").hide();
        $(".couponlayer").hide();
        $("#success_layer").hide();
        $("#passwordcoupon").val("");
        $(".passworderror").text("");
    });
    
    //关闭证件有效期的时候更改标志
    $(".id_valid_close,.update_id_valid").click(function(){
    	 Utils.simpleAjax(Utils.generUrl(Utils.getContextPath() + "/account/main/updateIdValidFlag"), {}, function (data) {
    		 if(data.success){
    			 
    		 }
    	 })
    })
    
    $('.news .ico_close').click(function () {
        $('.news').hide();
        $('.height_main').css("border-top", "10px solid #ebecec");
    });
    
    $('.newsforidcard').click(function () {
        $('.newsforidcard').hide();
        $('.height_main').css("border-top", "10px solid #ebecec");
    });

    // 头像
    var tipTimeout = null;
    $('.head_img .img_t, .head_img .aside').hover(function () {
        clearTimeout(tipTimeout);
        tipTimeout = setTimeout(function () {
        	if ($('.person_tip').html() != ""){
        		$('.person_tip').show();
        	}
        }, 400);
    }, function () {
        clearTimeout(tipTimeout);
    });

    /*$('.person_tip .close').click(function () {
        $('.person_tip').hide();
    });*/

    // 手机
    $('li.tel_disable').hover(function () {
        clearTimeout(tipTimeout);
        tipTimeout = setTimeout(function () {
            $('.state_tip').addClass("none");
            $('div.tel').removeClass("none");
            $('.state_tip').css("left", "86px");
        }, 400);
    }, function () {
        clearTimeout(tipTimeout);
    });

    //银行卡
    $('li.card_disable').hover(function () {
        clearTimeout(tipTimeout);
        tipTimeout = setTimeout(function () {
            $('.state_tip').addClass("none");
            $('div.card').removeClass("none");
            $('.state_tip').css("left", "114px");
        }, 400);
    }, function () {
        clearTimeout(tipTimeout);
    });

    //实名认证
    $('li.nickname_disable').hover(function () {
        clearTimeout(tipTimeout);
        tipTimeout = setTimeout(function () {
            $('.state_tip').addClass("none");
            $('div.nickname').removeClass("none");
            $('.state_tip').css("left", "142px");
        }, 400);
    }, function () {
        clearTimeout(tipTimeout);
    });

    //安全风险
    $('li.lock_disable').hover(function () {
        clearTimeout(tipTimeout);
        tipTimeout = setTimeout(function () {
            $('.state_tip').addClass("none");
            $('div.lock').removeClass("none");
            $('.state_tip').css("left", "172px");
        }, 400);
    }, function () {
        clearTimeout(tipTimeout);
    });

    //南方APP
    $('li.app_disable').hover(function () {
        clearTimeout(tipTimeout);
        tipTimeout = setTimeout(function () {
            $('.state_tip').addClass("none");
            $('div.app').removeClass("none");
            $('.state_tip').css("left", "202px");
        }, 400);
    }, function () {
        clearTimeout(tipTimeout);
    });

    //用户等级
    $('.user_lv .lv a').hover(function () {
        clearTimeout(tipTimeout);
        tipTimeout = setTimeout(function () {
            $('.state_tip').addClass("none");
            $('div.lever').show();
            $('.state_tip').css("left", "308px");
        }, 400);
    }, function () {
        clearTimeout(tipTimeout);
        $('div.lever').hide();
    });

    //积分等级
    $('.inte_lv').hover(function () {
        clearTimeout(tipTimeout);
        tipTimeout = setTimeout(function () {
            $('div.intelever').show();
        }, 400);
    }, function () {
        clearTimeout(tipTimeout);
        $('div.intelever').hide();
    });

    $('.state_tip').hover(function () {
    }, function () {
        $('this').addClass("none");
    });

    $('.user_name').hover(function () { },
        function () {
            $('.state_tip').addClass("none");
        }
    );

   /* $(".tc_bg").click(function () {
        $(".tc").hide();
    });*/

    $("#signInfo").find("a:first-child").click(function () {
        Utils.simpleAjax(Utils.generUrl(Utils.getContextPath() + "/account/main/sign"), {}, function (data) {
            if (data.success) {
                var nextscore = (data.score < 10 ? (data.score + 1) : data.score);
                $("#todayGainScore").html(data.score);
                $("#nextGainScore").html(nextscore);
                $("#gainDays").html(data.days);

                var str = "";
                for (var i = 0, cur = data.score; i < 6; i++) {
                    if (cur == data.score) {
                        str = str + '<li class="noleft checkin">+<span>' + cur + '</span></li>';
                        cur++;
                    } else if (cur == nextscore) {
                        str = str + '<li class="nextday">+<span>' + cur + '</span></li>';
                        cur++;
                    } else {
                        str = str + '<li>+<span>' + (cur > 10 ? 10 : cur) + '</span></li>';
                        cur++;
                    }
                }
                $("#jifen .cion ul").html(str);
                $("#jifen").show();
                memberScore();
                $("#signInfo").html("今日已签到");
                $("#signInfo").css("color", "#a4a4a4");
                Utils.showSuccessTip("签到成功，新加积分" + data.score);
                _paq.push(['trackEvent', Piwik_Event_Quota.score.category,Piwik_Event_Quota.score.succ.action ,data.days]); // 页面需要已经加载piwik.js
            } else {
                Utils.showErrorTip(data.errormessage);
                _paq.push(['trackEvent', Piwik_Event_Quota.score.category,Piwik_Event_Quota.score.fail.action ,data.errormessage]); // 页面需要已经加载piwik.js
            }
        });
        return false;
    });

    //预留信息弹框
    $("#reserveWords").click(function () {
        var reservwords = $("#reserveWords").text();
        if (reservwords == "您未设置预留信息") {
            $("#reservetitle").html("新增预留信息");
        } else {
            $("#reservetitle").html("修改预留信息");
            $("#reserve_info").val(reservwords);
        }
        $("#yuliu").show();
        $("#reserve_info").focus();
        return false;
    });
 
    //预留信息修改
    $("#reserveSubmit").click(function () {
        if($("#reserve_info").val().trim().length == 0) {
            Utils.showNoticeTip("请输入预留信息");
            $("#reserve_info").focus();
            return false;
        }
        if($("#reserve_info").val().trim() == $("#reserveWords").text()) {
            Utils.showSuccessTip("更新成功");
            $(".tc").hide();
            return false;
        }
        Utils.simpleAjax(Utils.generUrl(Utils.getContextPath() + "/account/main/reserve"), {
            reserveWords: $("#reserve_info").val().trim()
        }, function (data) {
            if (data.success) {
                Utils.showSuccessTip("更新成功");
                $("#reserveWords").text($("#reserve_info").val().trim());
                $("#reserveWords").attr("title", "点击修改");
                $(".tc").hide();
            } else {
                Utils.showErrorTip(data.errormessage);
            }
        });
        return false;
    });

    //持有明细折叠
    $("#assetsdetail").click(function () {
        $(".bal .list").toggle();
        $(this).toggleClass("titleh");

        if(!$("#mainDiv").attr("loaded")) {
            Utils.ajax(Utils.generUrl(Utils.getContextPath() + "/account/main/queryShare"), {}, function (data) {
                if (data.success) {
                    $("#mainDiv").html(data.data);
                }
                else {
                    $("#mainDiv").html("系统正在维护中，请稍后<a href='javascript:;' onclick='location.reload();return false;'>刷新页面</a>重试");
                }

                $("#mainDiv").attr("loaded", true);
            }, true, null, function () {
                $("#mainDiv").html("<p><img src='" + Utils.getContextPath() + "/resources/images/loading.gif' style='vertical-align:middle'/>&nbsp;&nbsp;&nbsp;正在加载数据...</p>");
            }, null);
        }
    });

    //收益率计算
    function calRatio(total, income) {
        income = parseFloat(income);
        total = parseFloat(total);
        if (income == 0 || total == income) {
            return "0.00";
        } else if (total <= 0 || total < income) {
            return '--';
        } else {
            return (income * 100 / (total - income)).toFixed(2);
        }
    }

    //查询首页总资产
    Utils.ajax(Utils.generUrl(Utils.getContextPath() + "/account/main/queryAssets"), {}, function (data) {
        if (data.success) {
    		 var total_value=Utils.fmtMoney(data.total_value, 2);
             $("#total_value").html(total_value);
             var totalUS = Utils.fmtMoney(data.totalUS_value, 2);
             if(totalUS != 0){
             	$("#totalUS_value").html("$"+totalUS);
                 $("#totaUSDIV").show();
             }
             if (data.last_day_income == '--') {
             	$("#last_day_income").html("更新中");
             	$("#last_day_income").addClass("red");
             	$("#curr_ratio").html("更新中");
             	$("#curr_ratio").addClass("red");
             } else {
             	$("#last_day_income").html(data.last_day_income + '&nbsp;元');
                 $("#last_day_income").addClass(parseFloat(data.last_day_income) >= 0 ? "red" : "gre");
                 $("#curr_ratio").html(calRatio(data.total_value, data.last_day_income) + "%");
                 $("#curr_ratio").addClass(parseFloat(data.last_day_income) >= 0 ? "red" : "gre");
             }
             //$("#accum_income").html(data.accum_income);
             $("#super_total_value").html(Utils.fmtMoney(data.super_total_value, 2));
             $("#super_last_day_income").html(data.super_last_day_income);
             $("#super_accum_income").html(data.super_accum_income);           
       
            $("#super_incomeratio").html(data.super_incomeratio + "%");
            $("#super_perincome").html(data.super_perincome);

            if(total_value != 0) {//总资产为零时，不提供资产分布入口
                $("#fund_fenbu").show();
                $("#fenbu_table tbody").html(template("fenbu_template", {"assetsDistribution":data.assetsDistribution}));
            }
            if (getCookie("noLongerPrompt") == "") {
//            	total_value = total_value.replace(/,/g,"");
//            	if (total_value > 1000000) {
//            		EV_modeAlert('envon');
//            	}
            	if(data.custLevel == "4"){
            		EV_modeAlert('envon');
            	}
            }
            //  业务要求不展示
//            if(data.intransit_num > 0) {
//                $(".theway_assets").removeClass("none");
//                $(".theway_assets .num_red").text(data.intransit_num);
//            }
        } 
        else {
        	/*$("#last_day_income").html("--");
        	$("#curr_ratio").html("--");
        	$(".theway_assets").removeClass("none");*/
        	$("#total_value").html("--");
       	 	$("#last_day_income").html("--");
       	 	$("#last_day_income").addClass("red");
       	 	$("#curr_ratio").html("--");
       	 	$("#curr_ratio").addClass("red");
           //$("#accum_income").html("--");
           $("#super_total_value").html("--");
           $("#super_last_day_income").html("--");
           $("#super_accum_income").html("--");
           $("#super_incomeratio").html("--");
           $("#super_perincome").html("--");
           Utils.showErrorTip("系统正在维护中,请稍后再试");
        }
    }, true, function () {
    	$("#total_value").html("--");
   	 	$("#last_day_income").html("--");
   	 	$("#last_day_income").addClass("red");
   	 	$("#curr_ratio").html("--");
   	 	$("#curr_ratio").addClass("red");
       //$("#accum_income").html("--");
       $("#super_total_value").html("--");
       $("#super_last_day_income").html("--");
       $("#super_accum_income").html("--");
       $("#super_incomeratio").html("--");
       $("#super_perincome").html("--");
       Utils.showErrorTip("系统正在维护中,请稍后再试");
   	},function () {
        $("#total_value").html(waitstr);
        $("#totalUS_value").html(waitstr);
        $("#last_day_income").html(waitstr);
        $("#curr_ratio").html(waitstr);
        $("#accum_income").html(waitstr);
        $("#super_total_value").html(waitstr);
        $("#super_last_day_income").html(waitstr);
        $("#super_accum_income").html(waitstr);
        $("#super_perincome").html(waitstr);
        $("#super_incomeratio").html(waitstr);  // 七日	 
    }, null);

    $("#sendFinancialAdvInfo").click(function () {
    	Utils.ajax(Utils.generUrl(Utils.getContextPath() + "/account/main/sendFinancialAdvInfo"), {}, function (data) {
    		if (data.success) {
    			if ($("#envon_check").is(':checked')) {
    				setCookie("noLongerPrompt","1",null);
    			}
    			Utils.showSuccessTip("已经通过短信的形式发送至您的手机，请注意查收");
    		} else {
    			Utils.showErrorTip(data.message);
    		}
    	});
    });
    
    $("#onPage").click(function () {
    	if ($("#envon_check").is(':checked')) {
    		setCookie("noLongerPrompt","1",null);
    	}
    	EV_closeAlert();
    });
    
    $("#toSpecialFinancial").click(function () {
    	if ($("#envon_check").is(':checked')) {
    		setCookie("noLongerPrompt","1",null);
    	}
    	window.location = Utils.generUrl(Utils.getContextPath() + "/special/main/init?menuId=50000");
    });
    
    // 查询认证信息
    Utils.ajax(Utils.generUrl(Utils.getContextPath() + "/account/main/bind"), {}, function (data) {
        if (data.success) {
            var bindMobile = '尚未实名认证，<a href="' + Utils.generUrl(Utils.getContextPath() + "/account/openacco/verify") + '" title="马上验证">马上验证</a>';
            $("#bindMobile").parent().css("width", "160px");
            if (data.bindMobile) {
                bindMobile = '手机号码已通过实名认证';
                $(".tel_disable").addClass("tel").removeClass("tel_disable");
                $("#bindMobile").parent().css("width", "160px");
            }
            $("#bindMobile").html(bindMobile);

            var bindCard = '未绑定银行卡，<a href="' + Utils.generUrl(Utils.getContextPath() + "/go?menuId=82000") + '" title="马上绑定银行卡">去绑定</a>';
            if (data.bindCard) {
                bindCard = '已绑定银行卡，<a href="' + Utils.generUrl(Utils.getContextPath() + "/go?menuId=82000") + '" title="查看我的银行卡">去查看</a>';
                $(".card_disable").addClass("card").removeClass("card_disable");
            } else {
                if (NF_CUST_TYPE == CUST_TYPE.DS_AGENCY || NF_CUST_TYPE == CUST_TYPE.OLD || NF_CUST_TYPE == CUST_TYPE.NEW) {
                    $("#bangka").show();
                }
            }
            $("#bindCard").html(bindCard);
            $("#bindCard").parent().css("width", "140px");

            var realName = '未完成实名认证，<a href="' + Utils.generUrl(Utils.getContextPath() + "/account/openacco/verify") + '" target="_blank" title="去认证">去认证</a>';
            if (data.realName) {
                realName = '已完成实名认证，<a target="_blank" href="' + Utils.getContextPath() + "/static/verify_privil" + '" title="查看我的特权">查看我的特权</a>';
                $(".nickname_disable").addClass("nickname").removeClass("nickname_disable");
                $("#realName").parent().css("width", "200px");
            }
            else {
                $("#realName").parent().css("width", "160px");
            }
            $("#realName").html(realName);

            var securityClass = '账户安全等级：<font style="color:red;">' + data.securityClass + '</font>，<a href="' + Utils.generUrl(Utils.getContextPath() + "/go?menuId=83000") + '" title="查看我的安全等级">去查看</a>';
            if (data.securityClass.indexOf("高") != -1) {
                $(".lock_disable").addClass("lock").removeClass("lock_disable");
            }
            $("#securityClass").html(securityClass);
            $("#securityClass").parent().css("width", "182px");

            var loginAPP = '南方APP未安装，<a href="http://www.nffund.com/main/campaign/nfapp/index.html" target="_blank" title="下载APP">去下载</a>';
            if (data.isLoginAPP) {
                loginAPP = '你已是南方APP客户';
                $(".app_disable").addClass("app").removeClass("app_disable");
                $("#loginAPP").parent().css("width", "130px");
            }
            else {
                $("#loginAPP").parent().css("width", "160px");
            }
            $("#loginAPP").html(loginAPP);

            // 头像进度条
            var hdheight = data.percent;
            if(hdheight < 100) {
                var html = '';
                var percentFlag = data.percentFlag;
                if(!(percentFlag & 1)) {
                    html += '<span>尚未通过实名认证&nbsp;&nbsp;<a href="'+Utils.generUrl(Utils.getContextPath() + "/account/openacco/verify")+'" title="立即实名认证">立即认证</a></span>'
                }
                else {
                    if(!(percentFlag & 1<<1)) {
                        html += '<span>尚未绑定银行卡&nbsp;&nbsp;<a href="'+Utils.generUrl(Utils.getContextPath() + "/go?menuId=82100&busintype=OPENTRADEACCO")+'" title="立即增开银行卡">立即绑定</a></span>';
                    }
                    if(!(percentFlag & 1<<2)) {
                        html += '<span>尚未完成风险测评&nbsp;&nbsp;<a href="' + Utils.generUrl(Utils.getContextPath() + "/go?menuId=81100") + '" title="立即进行风险测评">立即测评</a></span>';
                    }
                    if(!(percentFlag & 1<<3)) {
                        html += '<span>尚未登录手机APP&nbsp;&nbsp;<a href="http://www.nffund.com/main/campaign/nfapp/index.html" target="_blank" title="立即下载手机APP">立即下载</a></span>';
                    }

                    if(!(percentFlag & 1<<4)) {
                        html += '<span>未完善个人信息&nbsp;&nbsp;<a href="' + Utils.generUrl(Utils.getContextPath() + "/go?menuId=81000") + '" title="立即完善个人资料">立即完善</a></span>';
                    }
                }
            }

            $(".head_img .img_t span").css({height: hdheight * 71 / 100});
            $(".head_img span em").html(hdheight);
            $(".head_img .aside em").html(hdheight);
            if (hdheight == 100) {
                $(".head_img .aside").hide();
                $(".person_tip").append("恭喜您的个人资料完善度达100%").click(function () {
                    $('.person_tip').hide();
                }).css("width", "250px");
            }
            else {
                $(".person_tip").append(html).click(function () {
                    $('.person_tip').hide();
                });
            }
        }
    });
    $("body").bind("click", function (event) {
    	$('.person_tip').hide();
    });

    // 积分等级信息
    function grade(n) {
        if (n < 1) {
            return 0;
        }
        return 10 * (n - 1) * (n - 1) + 60 * n - 10;
    }
    
    function memberScore(){
    	Utils.ajax(Utils.generUrl(Utils.getContextPath() + "/account/main/member"), {}, function (data) {
    		if (data.success) {
    			var currentScore = grade(data.level);
    			var nextScore = grade(data.level + 1)
    			var pre = ((data.total - currentScore) / (nextScore - currentScore)) * 100;
    			$("#currentlevel").html("L" + data.level);
    			$("#nextlevel").html("L" + (data.level + 1));
    			
    			$("#levelInfo1").css("width", pre + "%");
    			$("#levelInfo2").html(data.total + "/" + nextScore);
    			$("#totalInfo").html(data.active);
    			if (data.userRank == "黄金客户") {
    				$(".user_lv span.lv a").css("background-position", "0 -25px");
    			} else if (data.userRank == "白金客户") {
    				$(".user_lv span.lv a").css("background-position", "0 -50px");
    			} else if (data.userRank == "铂金客户") {
    				$(".user_lv span.lv a").css("background-position", "0 -75px");
    			} else if (data.userRank == "钻石客户") {
    				$(".user_lv span.lv a").css("background-position", "0 -100px");
    			}
    			$("#userRankInfo").html(data.userRank);
    			
    			if (data.isSign != null && data.isSign != "" && data.isSign != "0") {
    				$("#signInfo").html('今日已签到');
                    $("#signInfo").css("color", "#a4a4a4");
    			}
    		}
    	});
    }
    memberScore();

    Utils.ajax(Utils.generUrl(Utils.getContextPath() + "/account/main/recommendfunds"), {}, function (data) {
        if (data.success) {
            $("#recommendfunds").html(data.data);
            $("#recommendfunds").show();
        }
        else {
            $("#recommendfunds").parent().hide();
        }
    }, true);

    var timer_1;
    //显示或隐藏预留验证提示
    $('#ezhiTips').hover(function () {
        clearTimeout(timer_1);
        timer_1 = setTimeout(function () {
            $('#ezhiTipsWords').show();
        }, 300);
    }, function () {
        clearTimeout(timer_1);
        $('#ezhiTipsWords').hide();
    });

    var timer_2;
    $(".fund_fenbu").mouseenter(function() {
        clearTimeout(timer_2);
        var $this = $(this);
        timer_2 = setTimeout(function () {
            if("1" != $this.attr("inited")) {
                seajs.use("highcharts", function() {
                    var data = [];
                    var headers = $("#fenbu_table").find('thead').find('th');
                    var rows = $("#fenbu_table").find('tbody').find('tr').find('td');
                    rows.each(function (index) {
                        if ($(this).text().length > 0) {
                            if($(headers.get(index)).text() == "货币型") {
                                data.push({
                                    "name": $(headers.get(index)).text(),
                                    "y": parseFloat($(this).text()),
                                    "sliced": true,
                                    "selected": true
                                });
                            }
                            else {
                                data.push([$(headers.get(index)).text(), parseFloat($(this).text())]);
                            }
                        }
                    });

                    new Highcharts.Chart({
                        credits: {
                            enabled:false
                        },
                        chart: {
                            renderTo: "fund_fenbu_img",
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45,
                                beta: 0
                            }
                        },
                        title: {
                            text: '<b>资产分布</b>'
                        },
                        tooltip: {
                            pointFormat : '{series.name}: <b>{point.y}</b>（元）<br/>'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                depth: 35,
                                dataLabels: {
                                    enabled: true,
                                    formatter : function() {
                                        return this.point.name + "(" + this.point.percentage.toFixed(2) + "%)";
                                    }
                                }
                            }
                        },
                        series: [
                            {
                                type: 'pie',
                                name: '市值',
                                data: data
                            }
                        ]
                    }, function (chartObj) {
                        chart = chartObj;
                    });
                    $(".fund_fenbu_img").prepend('<div class="ico"></div>');
                    $(".fund_fenbu_img").show();
                    $this.attr("inited", "1");
                });
            }
            else {
                $(".fund_fenbu_img").show();
            }
        }, 300);
    });
    $(".fund_fenbu").mouseleave(function() {
        clearTimeout(timer_2);
        $(".fund_fenbu_img").hide();
    });


    Utils.ajax(Utils.generUrl(Utils.getContextPath() + "/account/main/queryBankInfo"), {}, function (data) {
        if (data.success) {
            if(data.bShowCBTips) {
                $(".news").html('<span class="ico"></span><span class="word" style="color: rgb(216, 67, 67);"><label><span>温馨提示：因第三方支付渠道原因，您通过通联签约的中行卡可能会存在支付异常、支付额度不足等问题，请尝试先解约后重新签约。</span></label></span>');
            }
        }
    }, true);
    
    
    $("#bntFollow").click(function(){
    	Utils.simpleAjax(Utils.generUrl(Utils.getContextPath() + "/account/main/pwdFollow"), null,
            function (result) {
                if (result.success) {
                	setTimeout(function () {
	                    location.href = Utils.generUrl(Utils.getContextPath() + "/go?menuId=10000");
	                }, 1000);
                } else {
                    Utils.showErrorTip(result.errormessage);
                }
            },
            function (error) {
                Utils.showErrorTip("密码沿用失败，请稍候再试");
            }
        );
     });

    $(".tc_close").click(function(){
        $(".layer-box").fadeOut();
        $(".layer").fadeOut();
        return false;
    });



});


