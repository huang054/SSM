// 注意:上线后修改,不能直接删掉或调整结构;只能新增;明确已废弃的需要加上deprecated注释
// 站点ID
var Piwik_Site = {
    ETRADE : 2,
    APP : 3,
    ORG_TRADE : 5,
    WECHAT : 6,
    WAP : 7,
    WEB : 8
};

///////////////////////////////////////////////////////////////////////////////
// 页面标题命名规范	业务大类/一级页面/二级页面
// key末尾带有下划线的，表示后面还要添加相关的信息，例如adv_后面加上基金代码，表示相关基金的广告宣传页
// 每行后面的注释，如果各个应用公用，可以不加注释；如果是app专用，请加上注释
// 拼接示例：Piwik_PageView_Quota.index.self + "/" + Piwik_PageView_Quota.index.adv
// 原则,多个层级的self属性需要使用到
var Piwik_PageView_Quota = {
    login: {self: "登录页"},
    adv_: {self: "广告/"},
    intro_: {self: "介绍/"},

    index: {
        self: "首页",
        adv: "广告",
        new_financing: "新手理财"  // app
    },

    supermoney: {
        self: "超级现金宝",
        index: "首页",
        charge: "充值",
        encash: "取现",
        query_profit: "收益查询", // app
        fixed_encash: "定期取现",
        fixed_charge: "定期存入",
        fixed_invest: "定投"
    },

    fund: {
        self: "产品",
        list: {
            hot_sale: "热门产品",
            all: "全部基金",
            nav_reckon: "净值估算",
            fixinvest_rank: "定投排行",
            profit_rank: "收益排行",
            search: "基金搜索"
        },
        detail: {
            market: "基金详情",
            intro: "简介",
            manager: "基金经理",
            ten_holding: "十大持仓",
            rate_info: "费率",
            dividend: "历史分红",
            QA: "基金问答"
        }
    },

    trade: {
        self: "普通交易",
        subscribe: {
            self: "认购",
            apply: "申请",
            preview: "预览",
            result: "结果"
        },
        declare: {
            self: "申购",
            apply: "申请",
            preview: "预览",
            result: "结果"
        },
        redeem: {
            self: "赎回",
            apply: "申请",
            result: "结果"
        },
        convert: {
            self: "转换",
            apply: "申请",
            result: "结果"
        },
        redeem2buy: {
            self: "赎回转购",
            apply: "申请",
            result: "结果"
        },
        redeem_convert_list: "可赎回/转换列表",
        withdraw_list: "可撤单列表",
        custody_in: "托管转入"
    },

    combinvest : {
        self : "超智宝",
        list : "方案列表",
        custom : "定制方案",
        custom_result : "定制结果",
        detail : "组合详情",
        adjust_info : "调仓记录",
        purchase : {
            self: "购买",
            apply: "申请",
            preview: "预览",
            result: "结果"
        },
        redeem : {
            self: "赎回",
            apply: "申请",
            result: "结果"
        },
        adjust :  {
            self: "调仓",
            apply: "申请",
            result: "结果"
        }
    },

    fixinvest : {
        self : "定投",
        index : "首页",
        query_profit: "收益查询", // app
        add : {
            self : "新增",
            common : "普通定投",
            target_profit : "目标盈定投",
            index : "指数定投",
            EI : "e智定投",
            redeem : "定期定额赎回",
            balance : "余额理财"
        },
        update : {
            self : "修改",
            common : "普通定投",
            target_profit : "目标盈定投",
            index : "指数定投",
            EI : "e智定投",
            redeem : "定期定额赎回",
            balance : "余额理财"
        }
    },

    assets : {
        self: "个人资产",
        detail: {
            self: "资产明细",
            index: "首页",
            supermoney: "超级现金宝明细",
            cash: "现金通明细",
            breakeven: "避险基金明细",
            short_term: "短期理财明细"
        },
        profit_query: "盈亏查询",
        trade_record: {
            self: "交易记录",
            apply_list: "交易申请列表",
            confirm_list: "交易确认列表",
            detail: "交易记录明细",
            bill: "历史对账单查询"
        },
        dividend: {
            self: "分红",
            view: "我的分红",
            modify: "修改分红方式",
            list: "查看所有分红方式"
        }
    },
    person_center : {
        self : "个人中心",
        info : {
            self : "个人信息",
            index : "首页",
            combinvest_risk_evl : "超智宝风险测评",
            risk_evl : "风险测评",
            msg_custom : "消息定制",
            close_acco : "销户"
        },
        IDupgrade : "身份证升级",
        security : {
            self : "安全中心",
            index : "首页",
            modify_trade_pwd : "修改交易密码",
            reset_login_pwd : "设置登录密码",
            modify_login_pwd : "修改登录密码",
            reset_pwd_question : "设置密保问题",
            reset_gesture_pwd : "设置手势密码",
            reset_fingerprint_pwd : "设置指纹密码",
            reset_msg_notification : "设置短信通知"
        },
        score : {
            self : "我的积分",
            checkin : "签到",
            earn_more : "更多积分途径",
            rules : "积分规则说明",
            record : "积分记录"
        },
        quick_center : "快捷中心",
        help : "帮助中心",
        suggest : "意见与建议",
        invite : "邀请有礼",
        custom_service : "在线客服",
        adviser : "我的理财顾问",
        invite_friend : "邀请好友"
    },

    info_center : {
        self : "消息资讯",
        msg_list : "消息列表",
        annoucement : "基金公告",
        system : "系统消息",
        view_list : "资讯视点列表",
        finance_list : "财经资讯",
        company : "南方视点"
    },

    bank_card : {
        self : "银行卡",
        index : "首页",
        add : {
            self : "新增",
            apply : "申请",
            result : "结果"
        },
        change : "更换",
        modify_name : "修改名称",
        sign : {
            self : "签约",
            apply : "申请",
            result : "结果"
        },
        unsign : {
            self : "解约",
            apply : "申请",
            result : "结果"
        },
        remit : "汇款支付首页",
        phone_setting : "电话交易设置"
    },

    register : {
        self: "注册",
        step_one: "第一步：手机验证",
        step_two: "第二步：设置密码",
        step_three: "第三步：结果"
    },

    openacco : {
        self: "开户",
        step_one: "第一步：手机验证",
        step_two: "第二步：银行验证",
        step_three: "第三步：设置密码",
        step_four : "第四步：结果"
    },

    forgot_pwd : {
        self : "找回密码",
        index : "首页",
        login_by : {
            self : "找回登录密码",
            mobile_email : "通过手机或邮箱",
            fund_info : "通过持仓基金和销售机构",
            fund_acco : "通过身份信息和基金帐号"
        },
        login_by_register : {
            self : "注册客户找回登录密码",
            step_one : "第一步：手机验证",
            step_two : "第二步：设置新密码",
            step_three : "第三步：结果"
        },
        trade_by_mobile : {
            self : "通过手机号找回交易密码",
            step_one : "第一步：手机验证",
            step_two : "第二步：设置新密码",
            step_three : "第三步：结果"
        },
        trade_by_QA : {
            self : "通过密保找回交易密码",
            step_one : "第一步：身份验证",
            step_two : "第二步：密保验证",
            step_three : "第三步：设置新密码",
            step_four : "第四步：结果"
        },
        trade_by_bank : {
            self : "通过银行卡找回交易密码",
            step_one : "第一步：身份验证",
            step_two : "第二步：银行验证",
            step_three : "第三步：设置新密码",
            step_four : "第四步：结果"
        },
        trade_by_material : {
            self : "通过上传资料找回交易密码",
            step_one : "第一步：身份验证",
            step_two : "第二步：手机验证",
            step_three : "第三步：提交资料",
            step_four : "第四步：结果"
        },
        query_material_state : "查询资料审核状态"
    },

    high_financing : {  // 网上交易
        self: "高端理财",
        index : "首页",
        asset : "个人资产",
        trade_recode : "交易记录",
        dividend : "分红列表",
        modify_dividend : "修改分红方式",
        annoucement : "信息披露",
        reservation : "意向登记查询",
        buy : {
            self : "购买",
            contract_verify : "合同编号校验",
            apply: "申请",
            preview: "预览",
            result: "结果"
        },
        redeem : {
            self : "赎回",
            apply: "申请",
            preview: "预览",
            result: "结果"
        }
    },

    official : {
        self : "微信公众号",
        bind : "绑定",
        bind_succ : "绑定成功",
        lottery : "每日抽奖",
        forgot_login_pwd_by : {
            self: "找回查询密码",
            auto : "自动获取",
            fund : "通过持仓基金获取",
            fund_acco : "通过基金帐号获取"
        }
    }
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// 事件上报
// 拼接示例：_paq.push(['trackEvent', Piwik_Event_Quota.index.category, Piwik_Event_Quota.index.new_financing.action, Piwik_Event_Quota.index.new_financing.name]);
var Piwik_Event_Quota = {
    index: {
        category: "首页",
		
        new_financing: {action : "点击新手理财"},
        EI_fixinvest: {action : "点击e智定投"},
        invest_area: {action : "点击定投专区"},
        fund_rank: {action : "点击热销排行"},
		view_info : {action : "点击资讯视点"},
		msg_center : {action : "点击消息中心"},
		balance_fixinvest : {action : "点击工资理财"},
		supermoney_charge : {action : "点击立即充值"},
		supermoney : {action : "点击超级现金宝"},
		comb_invest : {action : "点击超投宝查看详情"},		
		comb_invest_custom : {action : "点击超投宝立即定制"},
		recommend_fund : {action : "点击推荐基金查看详情"},  // name为基金代码
        buy : {action : "点击立即购买"}, // name为基金代码
        
    index : {action : "首页"},  //微信和手机网站首页导航栏点击事件
    product : {action : "产品"},//微信和手机网站首页导航栏点击事件
    wealth : {action : "资产"},//微信和手机网站首页导航栏点击事件
    more : {action : "我的"},//微信和手机网站首页导航栏点击事件
    },

    fund: {
        category: "产品",
				hot_fund_tab: {action : "点击热门产品TAB"},
				all_fund_tab: {action : "点击全部产品TAB"},				
				est_fund_tab: {action : "点击净值估算TAB"},			
        hot_sale: {action : "点击热销排行"},
        fixinvest_sale: {action : "点击定投排行"},
        profit_sale: {action : "点击收益排行"},
        fav : {action : "点击我的关注"},
        discount_buy: {action : "点击热门0折购"}, // name为基金代码
        hot_sale_ad_detail: {action : "点击热门广告详情"}, // name为基金代码
        hot_sale_detail: {action : "点击热门基金详情"}, // name为基金代码
        discount_fixinvest: {action : "点击热门0折定投"}, // name为基金代码
        buy: {action : "点击全部基金购买"}, // name为基金代码
        view_all: {action : "点击全部基金基金详情"}, // name为基金代码
        est_market: {action : "点击净值估算基金详情"} // name为基金代码
    },

    fund_info: {
        category: "基金查看",
        index : {action : "基金信息首页"}, // 填入基金代码
        type : {action : "基金类型"}, // 指数、股票、混合、债券、保本、理财、货币、QDII
        nav_1 : {action : "净值走势近一月"}, // 填入基金代码
        nav_3 : {action : "净值走势近三月"}, // 填入基金代码
        nav_6 : {action : "净值走势近半年"}, // 填入基金代码
        nav_12 : {action : "净值走势近一年"}, // 填入基金代码

        nav_est : {action : "净值估算"}, // 填入基金代码
        nav_his : {action : "历史净值"}, // 填入基金代码

        detail : {action : "基金详情"}, // 填入基金代码
        mgr : {action : "基金经理"}, // 填入基金代码
        holdings : {action : "十大持仓"}, // 填入基金代码
        rate : {action : "费率结构"}, // 填入基金代码
        dividend : {action : "历史分红"}, // 填入基金代码
        QA : {action : "基金问答"}, // 填入基金代码
        search : {action : "基金搜索"} // 填入基金代码
    },

    login: {
        category: "登录",

        comm : {action : "页面登录"},
        gesture : {action : "手势登录"},
        third_party : {action : "第三方登录"}, // name 为 QQ or 微信

        cert_type : {action : "选择登录证件类型"}, // name 填入具体证件类型，例如手机号码/身份证号、基金帐号/股东帐号……
        passwd_input : {action : "切换密码输入方式"}, // 填入：直接输入、安全控件、软键盘

        fail : {action : "登录失败"}, // name 填入错误消息
        succ : {action : "登录成功"},
        logout : {action : "退出登录"}
    },

    register : {
        category: "注册",

        commit : {action : "提交注册"},
        succ : {action : "注册成功"}, // 手机号码开头3位，例如137
        fail : {action : "注册失败"}, // name 填入错误消息
        cancel : {action : "取消注册"},
        msg_verify_code : {action : "获取短信验证码"},
        img_verify_code : {action : "获取图片验证码"}
    },

    openacco : {
        category: "开户",

        commit : {action : "提交开户"},
        succ : {action : "开户成功"}, // 银行名称＋渠道方式，例如中国农业银行_直联快捷、招商银行_通联
        fail : {action : "开户失败"}, // name 填入错误消息
        cancel : {action : "取消开户"},
        msg_verify_code : {action : "获取短信验证码"},
        select_bank : {action : "选择银行"} // name 填入银行名称＋渠道方式
    },

    addbank : {
        category: "增卡",

        select_bank : {action : "选择银行"}, // name 填入银行名称＋渠道方式，例如中国农业银行_直联快捷、招商银行_通联
        commit : {action : "提交增卡"},
        succ : {action : "增卡成功"}, // 银行名称＋渠道方式，例如中国农业银行_直联快捷、招商银行_通联
        fail : {action : "增卡失败"}, // name 填入错误消息
        cancel : {action : "取消增卡"}
    },

    changebank : {
        category: "换卡",

        commit : {action : "提交换卡"},
        succ : {action : "换卡成功"}, // 银行名称＋渠道方式，例如中国农业银行_直联快捷、招商银行_通联
        fail : {action : "换卡失败"}, // name 填入错误消息
        cancel : {action : "取消换卡"}
    },

    subscribe : {
        category: "认购",

        select_fund : {action : "选择基金"}, // name 填入基金代码
        select_pay_way : {action : "选择支付方式"}, // 超级现金宝、原现金宝、在线支付、汇款支付
        select_bank : {action : "选择支付银行"}, // name 填入银行名称＋渠道方式，例如中国农业银行_直联快捷、招商银行_通联

        preview : {action : "认购预览"}, // 填入基金代码
        commit : {action : "提交认购"}, // 填入基金代码
        succ : {action : "认购成功"}, // 填入基金代码
        fail : {action : "认购失败"}, // 填入错误消息
        cancel : {action : "取消认购"} // 填入基金代码
    },

    declare : {
        category: "申购",

        select_fund : {action : "选择基金"}, // name 填入基金代码
        select_pay_way : {action : "选择支付方式"}, // 超级现金宝、原现金宝、在线支付、汇款支付
        select_bank : {action : "选择支付银行"}, // name 填入银行名称＋渠道方式，例如中国农业银行_直联快捷、招商银行_通联

        preview : {action : "申购预览"}, // 填入基金代码
        commit : {action : "提交申购"}, // 填入基金代码
        succ : {action : "申购成功"}, // 填入基金代码
        fail : {action : "申购失败"}, // 填入错误消息
        cancel : {action : "取消申购"} // 填入基金代码
    },

    redeem : {
        category: "赎回",

        select_fund : {action : "选择基金"}, // name 填入基金代码
        acco : {action : "赎回帐户"}, // 超级现金宝或银行名称＋渠道方式
        commit : {action : "提交赎回"}, // 填入基金代码
        succ : {action : "赎回成功"}, // 填入基金代码
        fail : {action : "赎回失败"}, // 填入错误消息
        cancel : {action : "取消赎回"}
    },

    withdraw : {
        category: "撤单",
        commit : {action : "提交撤单"}, // 填入待撤单的业务名称
        succ : {action : "撤单成功"}, // 填入基金代码
        fail : {action : "撤单失败"}, // 填入错误消息
        cancel : {action : "取消撤单"}
    },

    convert : {
        category: "转换",
        out_fund : {action : "转出基金"}, // 基金代码
        in_fund : {action : "转入基金"}, // 基金代码
        commit : {action : "提交转换"},
        succ : {action : "转换成功"},
        fail : {action : "转换失败"}, // 填入错误消息
        cancel : {action : "取消转换"}
    },

    redeem2buy : {
        category: "赎回转购",

        out_fund : {action : "赎回基金"}, // 基金代码
        in_fund : {action : "转购基金"}, // 基金代码
        commit : {action : "提交赎回转购"},
        succ : {action : "赎回转购成功"},
        fail : {action : "赎回转购失败"}, // 填入错误消息
        cancel : {action : "取消赎回转购"}
    },

    add_fixinvest : {
        category: "新增定投协议",

        select_pay_way : {action : "选择支付方式"}, // 超级现金宝、原现金宝、在线支付、汇款支付
        select_bank : {action : "选择支付银行"}, // name 填入银行名称＋渠道方式，例如中国农业银行_直联快捷、招商银行_通联
        select_period : {action : "选择定投周期"}, // 每月、双周、单周
        select_day : {action : "选择定投日期"}, // 1～28，或周一～周五
        select_index : {action : "选择参照指数"}, // 沪深300／创业板等
        select_multi : {action : "最大投资倍数"}, // 1.5/2/3/其它
        select_range : {action : "级差"},  // 10%、20%等

        commit : {action : "提交定投"},
        succ : {action : "定投成功"},  // 填入定投类型:普通定投、组合定投、E智定投、指数定投、余额理财
        fail : {action : "定投失败"}, // 填入错误消息
        cancel : {action : "取消定投"}
    },

    modify_fixinvest : {
        category: "修改定投协议",

        select_pay_way : {action : "选择支付方式"}, // 超级现金宝、原现金宝、在线支付、汇款支付
        select_bank : {action : "选择支付银行"}, // name 填入银行名称＋渠道方式，例如中国农业银行_直联快捷、招商银行_通联
        select_period : {action : "选择定投周期"}, // 每月、双周、单周
        select_day : {action : "选择定投日期"}, // 1～28，或周一～周五
        select_index : {action : "选择参照指数"}, // 沪深300／创业板等
        select_multi : {action : "最大投资倍数"}, // 1.5/2/3/其它
        select_range : {action : "级差"},  // 10%、20%等

        commit : {action : "提交修改定投"},
        succ : {action : "修改定投成功"},  // 填入定投类型:普通定投、组合定投、E智定投、指数定投、余额理财
        fail : {action : "修改定投失败"}, // 填入错误消息
        cancel : {action : "取消修改定投"}
    },

    supermoney_charge : {
        category: "超级现金宝充值",

        select_bank : {action : "选择支付银行"}, // name 填入银行名称＋渠道方式，例如中国农业银行_直联快捷、招商银行_通联
        commit : {action : "提交充值"},
        succ : {action : "充值成功"},
        fail : {action : "充值失败"}, // 填入错误消息
        cancel : {action : "取消充值"}
    },

    supermoney_encash : {
        category: "超级现金宝取现",

        select_bank : {action : "选择取现银行"}, // name 填入银行名称＋渠道方式，例如中国农业银行_直联快捷、招商银行_通联
        select_quick_flag : {action : "选择取现方式"}, // 快速取现、普通取现
        commit : {action : "提交取现"},
        succ : {action : "取现成功"},
        fail : {action : "取现失败"}, // 填入错误消息
        cancel : {action : "取消取现"}
    },

    supermoney_fixinvest : {
        category: "超级现金宝定期充值",

        select_bank : {action : "选择支付银行"}, // name 填入银行名称＋渠道方式，例如中国农业银行_直联快捷、招商银行_通联
        select_period : {action : "选择定投周期"}, // 每月、双周、单周
        select_day : {action : "选择定投日期"}, // 1～28，或周一～周五
        commit : {action : "提交超宝定充"},
        succ : {action : "超宝定充成功"},
        fail : {action : "超宝定充失败"}, // 填入错误消息
        cancel : {action : "取消超宝定充"}
    },

    supermoney_fixencash : {
        category: "超级现金宝定期取现",

        select_bank: {action: "选择取现银行"}, // name 填入银行名称＋渠道方式，例如中国农业银行_直联快捷、招商银行_通联
        select_period: {action: "选择取现周期"}, // 每月、双周、单周
        select_day: {action: "选择取现日期"}, // 1～28，或周一～周五
        commit: {action: "提交超宝定取"},
        succ: {action: "超宝定取成功"},
        fail: {action: "超宝定取失败"}, // 填入错误消息
        cancel: {action: "取消超宝定取"}
    },

    combinvest : {
        category : "超级智投宝购买",

        select_bank : {action : "选择支付银行"}, // name 填入银行名称＋渠道方式，例如中国农业银行_直联快捷、招商银行_通联
        select_fund : {action : "选择组合基金"}, // 填入组合基金代码
        preview: {action: "超智宝购买预览"}, // 填入组合基金代码
        commit: {action: "提交超智宝购买"}, // 填入组合基金代码
        succ: {action: "超智宝购买成功"}, // 填入组合基金代码
        fail: {action: "超智宝购买失败"}, // 填入错误消息
        cancel: {action: "取消超智宝购买"}, // 填入组合基金代码
        view : {action : "查看超智宝方案"} // 填入组合基金代码
    },
    combinvest_adjust : {
        category : "超级智投宝调仓",

        preview: {action: "超智宝调仓预览"}, // 填入组合基金代码
        commit: {action: "提交超智宝调仓"}, // 填入组合基金代码
        succ: {action: "超智宝调仓成功"}, // 填入组合基金代码
        fail: {action: "超智宝调仓失败"}, // 填入错误消息
        cancel: {action: "取消超智宝调仓"}, // 填入组合基金代码
        view : {action : "查看超智宝方案"} // 填入组合基金代码
    },
    combinvest_redeem : {
        category : "超级智投宝赎回",

        preview: {action: "超智宝赎回预览"}, // 填入组合基金代码
        commit: {action: "提交超智宝赎回"}, // 填入组合基金代码
        succ: {action: "超智宝赎回成功"}, // 填入组合基金代码
        fail: {action: "超智宝赎回失败"}, // 填入错误消息
        cancel: {action: "取消超智宝赎回"}, // 填入组合基金代码
        view : {action : "查看超智宝方案"} // 填入组合基金代码
    },    
    modify_mobile : {
        category : "修改手机号码",
        msg_verify_code : {action : "获取短信验证码"},
        commit: {action: "提交修改手机号"},
        succ: {action: "修改手机号成功"},
        fail: {action: "修改手机号失败"} // 填入错误消息
    },

    modify_custinfo : {
        category : "修改个人资料",
        commit: {action: "提交修改个人资料"},
        succ: {action: "修改个人资料成功"},
        fail: {action: "修改个人资料失败"} // 填入错误消息
    },

    modify_trade_pwd : {
        category : "修改交易密码",
        commit: {action: "提交修改交易密码"},
        succ: {action: "修改交易密码成功"},
        fail: {action: "修改交易密码失败"} // 填入错误消息
    },

    modify_login_pwd : {
        category : "修改登录密码",
        commit: {action: "提交修改登录密码"},
        succ: {action: "修改登录密码成功"},
        fail: {action: "修改登录密码失败"} // 填入错误消息
    },

    reset_login_pwd : {
        category : "设置登录密码",
        commit: {action: "提交设置登录密码"},
        succ: {action: "设置登录密码成功"},
        fail: {action: "设置登录密码失败"} // 填入错误消息
    },

    reset_trade_pwd : {
        category : "重置交易密码",
        commit: {action: "提交重置交易密码"},
        succ: {action: "设置交易密码成功"},
        fail: {action: "设置交易密码失败"} // 填入错误消息
    },
    
    risk_custom : {
        category : "风险测评",
        commit: {action: "提交测评"},
        succ: {action: "测评成功"}, // 填入风险等级，例如稳健型
        fail: {action: "测评失败"} // 填入错误消息
    },

    combrisk_custom : {
        category : "组合投资风险测评",
        commit: {action: "提交测评"},
        succ: {action: "测评成功"}, // 填入风险等级，例如稳健型
        fail: {action: "测评失败"} // 填入错误消息
    },

    score : {
        category : "积分",
        succ: {action: "签到成功"}, // 填入连续签到的天数
        fail: {action: "签到失败"} // 填入错误消息
    },

    focus : {
        category : "关注",
        add: {action: "新增关注"}, // 基金代码
        cancel: {action: "取消关注"} // 基金代码
    },

    online : {
        category : "客服",
        click: {action: "点击在线客服"} // 当前页面URL，不带参数
    },

    forgot_login_pwd : {
        category: "找回登录密码",
        succ: {
            action: "提交成功",
            name : {
                by_register: "注册客户找回登录密码",
                by_mobile_email: "通过邮箱或手机找回登录密码",
                by_fund_info: "通过基金名称、销售机构设置登录密码",
                by_fund_acco: "通过基金帐号、身份信息设置登录密码"
            }
        },
        fail : {action : "提交失败"} // 填入错误消息
    },

    forgot_trade_pwd : {
        category: "找回交易密码",
        succ: {
            action: "提交成功", // 填入找回交易密码类型
            name : {
                by_material: "通过传递材料找回交易密码",
                by_mobile: "通过手机找回交易密码",
                by_QA: "通过密保找回交易密码",
                by_bank: "通过银行验证找回交易密码"
            }
        },
        fail : {action : "提交失败"} // 填入错误消息
    },

    weixin: {
        category : "绑定微信",

        bind : {action : "点击立即绑定"},
        openacco : {action : "点击我要开户"},
        bind_by_assets : {action : "点击通过持仓方式绑定"},
        bind_by_accoinfo : {action : "点击通过账户信息绑定"}
    }
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// 转化目标
var Piwik_Convesion_Quota = {
    register: {
        mobile_verify_ok: {
            id: 1,
            name: "注册-手机验证成功"
        },
        succ: {
            id: 2,
            name: "注册-成功"
        }
    },

    openacco: {
        ID_verify_ok: {
            id: 3,
            name: "开户-证件号和手机号验证成功"
        },
        bank_verify_ok: {
            id: 4,
            name: "开户-银行验证成功"
        },
        succ: {
            id: 5,
            name: "开户-成功"
        }
    },

    addbankcard: {
        bank_verify_ok: {
            id: 6,
            name: "增卡-银行验证成功"
        },
        succ: {
            id: 7,
            name: "增卡-成功"
        }
    },

    subscribe: {
        preview: {
            id: 8,
            name: "认购-预览"
        },
        succ: {
            id: 9,
            nmae: "认购-成功"
        }
    },

    declare: {
        preview: {
            id: 10,
            name: "申购-预览"
        },
        succ: {
            id: 11,
            nmae: "申购-成功"
        }
    },

    supermoney: {
        charge_succ: {
            id: 12,
            name: "超宝充值-成功"
        },
        encash_succ: {
            id: 13,
            name: "超宝取现-成功"
        }
    },

    combinvest: {
        preview: {
            id: 14,
            name: "超智宝购买-预览"
        },
        succ: {
            id: 15,
            nmae: "超智宝购买-成功"
        }
    }
};

// 目标ID
var Piwik_Goal = {
    register_verify_mobile : 1,  // 手机注册(弱认证)--第一步:手机验证成功
    register_succ : 2,           // 手机注册(弱认证)--第二步:注册-成功

    openacco_info_auth : 3,      // 开户(实名认证)--第一步:证件号和手机号验证成功
    openacco_bank_auth : 4,      // 开户(实名认证)--第二步:银行验证成功
    openacco_succ : 5,           // 开户(实名认证)--第三步:开户-成功

    addcard_bank_auth : 6,       // 增卡-银行验证成功
    addcard_succ : 7,            // 增卡-成功

    subscribe_preview : 8,       // 认购-预览
    subscribe_succ : 9 ,         // 认购-成功

    declare_preview : 10,        // 申购-预览
    declare_succ : 11,           // 申购-成功

    supermoney_charge : 12,      // 超宝充值-成功
    supermoney_encash : 13,      // 超宝取现-成功

    combinvest_preview : 14,     // 超智宝购买-预览
    combinvest_succ : 15         // 超智宝购买-成功
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// 自定义数据字段
(function (global, undefined) {
    global.Enum = function (namesToValues) {
        var enumeration = function () {
            throw "can't Instantiate Enumerations";
        };

        function inherit(superCtor) {
            var f = function () {
            };
            f.prototype = superCtor;
            var ctor = function () {
            };
            ctor.prototype = new f();
            ctor.prototype.constructor = superCtor.constructor;
            return new ctor;
        }

        var proto = enumeration.prototype = {
            constructor: enumeration,
            toString: function () {
                return this.name;
            },
            valueOf: function () {
                return this.value;
            },
            toJSON: function () {
                return this.name;
            }
        };

        enumeration.values = [];

        for(var name in namesToValues) {
            var e = inherit(proto);
            e.name = name;
            e.value = namesToValues[name];
            enumeration[name] = e;
            enumeration.values.push(e);
        }

        enumeration.foreach = function (f, c) {
            for (var i = 0; i < this.values.length; i++) {
                f.call(c, this.values[i]);
            }
        };

        return enumeration;
    };
})(window);

var Piwik_CustomField = window.Enum({
    errmsg : "错误信息",
    auth_code_img : "图片验证码",
    bank_card_no : "银行卡号",
    new_bank_card_no : "新银行卡号",
    bank_no : "银行编号",
    capital_mode : "资金方式",
    sub_capital_mode : "明细资金方式",
    interface_type : "是否支持快捷",
    name : "客户名称",
    nickname : "昵称",
    ID_type : "证件类型",
    ID_no : "证件号码",
    custodian_name : "监护人名字",
    custodian_ID_no : "监护人身份证号",
    referee : "推荐人姓名或代码",
    referee_phone : "推荐人手机或电话号码",
    mobile : "手机号",
    mobile_new : "新手机号",
    mobile_old : "旧手机号",
    auth_code_mobile : "手机验证码",
    fund_code : "基金代码或组合基金代码",
    balance : "金额",
    share : "份额",
    allot_no : "申请单号",
    sale_agency : "销售机构",
    fund_acco : "基金帐号",
    tradeacco : "交易帐号",
    payway : "支付方式：0-超级现金宝 1-在线支付 2-汇款支付 3-原现金宝",
    exceed_flag : "巨额赎回标志",
    target_fund_code : "目标基金",
    busi_code : "业务代码",
    fund_code_list : "基金代码列表",
    fast_redeem_flag : "快速取现标记",
    fixinvest_day : "定投日期",
    fixinvest_period : "定投周期：1-每月、2-双周、3-单周",
    fixinvest_index : "定投参考指数",
    fixinvest_name : "定投计划名称",
    max_multi_times : "定投最大投资倍数",
    range : "级差",
    max_deduct_balance : "最大扣款金额",
    remain_bank_balance : "银行卡留存余额",
    birthday : "生日",
    ID_date : "证件有效期",
    address : "联系地址",
    sex : "性别",
    email : "邮件",
    zipcode : "邮编",
    vac : "职业",
    appver : "APP版本号"
});

// 前端不用,做管理用
var Piwik_CustomType = {
    "openacco" : {
        name: "开户",
        fields: [Piwik_CustomField.name, Piwik_CustomField.mobile, Piwik_CustomField.ID_type, Piwik_CustomField.ID_no, Piwik_CustomField.bank_card_no, Piwik_CustomField.bank_no, Piwik_CustomField.capital_mode, Piwik_CustomField.sub_capital_mode, Piwik_CustomField.interface_type, Piwik_CustomField.custodian_ID_no, Piwik_CustomField.custodian_name, Piwik_CustomField.referee_phone, Piwik_CustomField.referee]
    },

    "register" : {
        name: "注册",
        fields: [Piwik_CustomField.mobile, Piwik_CustomField.auth_code_mobile]
    },

    "addbankcard" : {
        name: "增卡",
        fields: [Piwik_CustomField.bank_no, Piwik_CustomField.bank_card_no, Piwik_CustomField.capital_mode, Piwik_CustomField.sub_capital_mode, Piwik_CustomField.interface_type]
    },

    "signbankcard" : {
        name: "签约",
        fields: [Piwik_CustomField.tradeacco]
    },

    "unsignbankcard" : {
        name: "解约",
        fields: [Piwik_CustomField.tradeacco]
    },

    "changebankcard" : {
        name: "换卡",
        fields: [Piwik_CustomField.tradeacco, Piwik_CustomField.new_bank_card_no]
    },

    "login" : {
        name: "登录",
        fields: [Piwik_CustomField.ID_type, Piwik_CustomField.ID_no, Piwik_CustomField.mobile, Piwik_CustomField.fund_acco, Piwik_CustomField.nickname]
    },

    "subscribe" : {
        name: "认购",
        fields: [Piwik_CustomField.fund_code, Piwik_CustomField.payway, Piwik_CustomField.tradeacco, Piwik_CustomField.balance, Piwik_CustomField.referee, Piwik_CustomField.referee_phone]
    },

    "declare" : {
        name: "申购",
        fields: [Piwik_CustomField.fund_code, Piwik_CustomField.payway, Piwik_CustomField.tradeacco, Piwik_CustomField.balance, Piwik_CustomField.referee, Piwik_CustomField.referee_phone]
    },

    "redeem" : {
        name: "赎回",
        fields: [Piwik_CustomField.fund_code, Piwik_CustomField.payway, Piwik_CustomField.share, Piwik_CustomField.exceed_flag, Piwik_CustomField.target_fund_code, Piwik_CustomField.tradeacco, Piwik_CustomField.fast_redeem_flag]
    },

    "convert" : {
        name: "转换",
        fields: [Piwik_CustomField.fund_code, Piwik_CustomField.share, Piwik_CustomField.exceed_flag, Piwik_CustomField.target_fund_code, Piwik_CustomField.referee, Piwik_CustomField.referee_phone, Piwik_CustomField.tradeacco]
    },

    "withdraw" : {
        name: "撤单",
        fields: [Piwik_CustomField.fund_code, Piwik_CustomField.tradeacco, Piwik_CustomField.busi_code]
    },

    "fixinvest" : {
        name: "定投",
        fields: [Piwik_CustomField.fund_code_list, Piwik_CustomField.payway, Piwik_CustomField.tradeacco, Piwik_CustomField.fixinvest_day, Piwik_CustomField.fixinvest_period, Piwik_CustomField.balance, Piwik_CustomField.exceed_flag, Piwik_CustomField.referee, Piwik_CustomField.referee_phone, Piwik_CustomField.fixinvest_index, Piwik_CustomField.max_multi_times, Piwik_CustomField.range, Piwik_CustomField.remain_bank_balance, Piwik_CustomField.max_deduct_balance, Piwik_CustomField.fixinvest_name]
    },

    "fixredeem" : {
        name: "定期定额赎回",
        fields: [Piwik_CustomField.payway, Piwik_CustomField.tradeacco, Piwik_CustomField.fund_code, Piwik_CustomField.fixinvest_day, Piwik_CustomField.fixinvest_period, Piwik_CustomField.share, Piwik_CustomField.fixinvest_name]
    },

    "modifycustinfo" : {
        name: "修改客户资料",
        fields: [Piwik_CustomField.nickname, Piwik_CustomField.birthday, Piwik_CustomField.ID_date, Piwik_CustomField.address, Piwik_CustomField.sex, Piwik_CustomField.email, Piwik_CustomField.zipcode, Piwik_CustomField.vac, Piwik_CustomField.referee, Piwik_CustomField.referee_phone]
    },

    "modifymobile" : {
        name: "更换手机",
        fields: [Piwik_CustomField.mobile_old, Piwik_CustomField.mobile_new, Piwik_CustomField.authcode_mobile]
    },

    "forgot-loginpwd" : {
        name: "找回登录密码",
        fields: [Piwik_CustomField.mobile, Piwik_CustomField.auth_code_mobile, Piwik_CustomField.ID_type, Piwik_CustomField.ID_no, Piwik_CustomField.fund_code, Piwik_CustomField.sale_agency, Piwik_CustomField.fund_acco]
    },

    "forgot-tradepwd" : {
        name: "找回交易密码",
        fields: [Piwik_CustomField.name, Piwik_CustomField.ID_type, Piwik_CustomField.ID_no, Piwik_CustomField.mobile, Piwik_CustomField.auth_code_mobile, Piwik_CustomField.bank_card_no, Piwik_CustomField.bank_no, Piwik_CustomField.capital_mode]
    }
};

