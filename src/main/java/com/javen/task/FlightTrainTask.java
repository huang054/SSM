package com.javen.task;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.javen.dao.IDigiccyDao;
import com.javen.dao.IFinanceDao;

@Component
public class FlightTrainTask {
	
	@Autowired
	private IDigiccyDao digiccyDao;
	
	@Autowired
	private IFinanceDao financeDao;
	 @Scheduled(cron = "0 0/10 * * * ?") // 间隔10分钟执行
	    public void taskCycle() {
		 CloseableHttpClient httpCilent2 = HttpClients.createDefault();
	       RequestConfig requestConfig = RequestConfig.custom()
	               .setConnectTimeout(5000)   //设置连接超时时间
	               .setConnectionRequestTimeout(5000) // 设置请求超时时间
	               .setSocketTimeout(5000)
	               .setRedirectsEnabled(true)//默认允许自动重定向
	               .build();
	       HttpGet httpGet2 = new HttpGet("https://www.hbg.com/-/x/general/index/constituent_symbol/detail?r=8ca7e1h99xw");
	       httpGet2.setConfig(requestConfig);
	       String srtResult = "";
	       try {
	           HttpResponse httpResponse = httpCilent2.execute(httpGet2);
	           if(httpResponse.getStatusLine().getStatusCode() == 200){
	                srtResult = EntityUtils.toString(httpResponse.getEntity());//获得返回的结果
	              JSONObject obj=JSON.parseObject(srtResult);
	              JSONObject js1 = JSONObject.parseObject(obj.getString("data"));
	             JSONArray array = js1.getJSONArray("symbols");
	            
	              
	               List<JsonModel>  collection = JSONObject.parseArray(array.toString(), JsonModel.class);
	            for(JsonModel j:collection) {
	        	   if(j.getName().equals("btc")||j.getName().equals("eos")||j.getName().equals("eth")||j.getName().equals("etc")) {
	        		   digiccyDao.update(j);
	        		  //最新价 
	        		   System.out.println(j.getClose());
	        		//涨幅 
                 System.out.println(j.getRise_percent());
	        	   }
	           }
	           }else if(httpResponse.getStatusLine().getStatusCode() == 400){
	               //..........
	           }else if(httpResponse.getStatusLine().getStatusCode() == 500){
	               //.............
	           }
	       } catch (IOException e) {
	           e.printStackTrace();
	       }finally {
	           try {
	               httpCilent2.close();
	           } catch (IOException e) {
	               e.printStackTrace();
	           }
	       }
	    }

	 @Scheduled(cron = "* 0/15 * * * ?") // 15分钟执行一次
	 public void taskFund() {
		 CloseableHttpClient httpCilent2 = HttpClients.createDefault();
	       RequestConfig requestConfig = RequestConfig.custom()
	               .setConnectTimeout(5000)   //设置连接超时时间
	               .setConnectionRequestTimeout(5000) // 设置请求超时时间
	               .setSocketTimeout(5000)
	               .setRedirectsEnabled(true)//默认允许自动重定向
	               .build();
	       HttpGet httpGet2 = new HttpGet("http://api.money.126.net/data/feed/1399001,1399005,1399006,1399300,1399305,0000001,hkHSI,FU_thismonth,UD_SHAZ,UD_SHAP,UD_SHAD,UD_SZAZ,UD_SZAP,UD_SZAD,UD_HS3Z,UD_HS3P,UD_HS3D,0000011,FU_au,FU_ag,hkHSCEI,hkHSCCI,US_DOWJONES,USRANK_COUNT_DOWJONES,US_NASDAQ,USRANK_COUNT_NASDAQ,US_SP500,USRANK_COUNT_SP500,FX_USDJPY,FX_EURUSD,FX_USDCNY,money.api?4190");
	       httpGet2.setConfig(requestConfig);
	       String srtResult = "";
	       try {
	           HttpResponse httpResponse = httpCilent2.execute(httpGet2);
	           if(httpResponse.getStatusLine().getStatusCode() == 200){
	                srtResult = EntityUtils.toString(httpResponse.getEntity());//获得返回的结果
	                JSONObject obj=  JSONObject.parseObject(srtResult.split("\\(")[1].split("\\)")[0]);
	                
	                //红筹指数
	                String gem1=obj.getString("hkHSCCI");
	                
	                Share s1=new Share();
	                s1.setShareName(JSONObject.parseObject(gem1).getString("name"));
	                s1.setPrice(new BigDecimal(JSONObject.parseObject(gem1).getString("price")));
	                s1.setRiseandfallvalue(new BigDecimal(JSONObject.parseObject(gem1).getString("price")).subtract(new BigDecimal(JSONObject.parseObject(gem1).getString("yestclose"))));
	                s1.setRiseandfall(s1.getRiseandfallvalue().divide(new BigDecimal(JSONObject.parseObject(gem1).getString("yestclose")), 2, BigDecimal.ROUND_HALF_UP).toString());
	                System.out.println("比例1"+s1.getRiseandfall());
	                financeDao.update(s1);
	               
	                //中小板指
	                String gem3=obj.getString("1399005");
	                
	                Share s2=new Share();
	                s2.setShareName(JSONObject.parseObject(gem3).getString("name"));
	                s2.setPrice(new BigDecimal(JSONObject.parseObject(gem3).getString("price")));
	                s2.setRiseandfallvalue(new BigDecimal(JSONObject.parseObject(gem3).getString("price")).subtract(new BigDecimal(JSONObject.parseObject(gem3).getString("yestclose"))));
	                s2.setRiseandfall(s2.getRiseandfallvalue().divide(new BigDecimal(JSONObject.parseObject(gem3).getString("yestclose")), 2, BigDecimal.ROUND_HALF_UP).toString());
	                System.out.println("比例2"+s2.getRiseandfall());
	                financeDao.update(s2);
	                //沪深300
	                String gem5=obj.getString("1399300");
	                Share s3=new Share();
	                s3.setShareName(JSONObject.parseObject(gem5).getString("name"));
	                s3.setPrice(new BigDecimal(JSONObject.parseObject(gem5).getString("price")));
	                s3.setRiseandfallvalue(new BigDecimal(JSONObject.parseObject(gem5).getString("price")).subtract(new BigDecimal(JSONObject.parseObject(gem5).getString("yestclose"))));
	                s3.setRiseandfall(s3.getRiseandfallvalue().divide(new BigDecimal(JSONObject.parseObject(gem5).getString("yestclose")), 2, BigDecimal.ROUND_HALF_UP).toString());
	                System.out.println("比例3"+s3.getRiseandfall());
	                financeDao.update(s3);
	                //标普500
	                String gem6=obj.getString("US_SP500");
	                Share s4=new Share();
	                s4.setShareName(JSONObject.parseObject(gem6).getString("name"));
	                s4.setPrice(new BigDecimal(JSONObject.parseObject(gem6).getString("price")));
	                s4.setRiseandfallvalue(new BigDecimal(JSONObject.parseObject(gem6).getString("price")).subtract(new BigDecimal(JSONObject.parseObject(gem6).getString("yestclose"))));
	                s4.setRiseandfall(s4.getRiseandfallvalue().divide(new BigDecimal(JSONObject.parseObject(gem6).getString("yestclose")), 2, BigDecimal.ROUND_HALF_UP).toString());
	                System.out.println("比例4"+s4.getRiseandfall());
	                financeDao.update(s4);
	                //深证成指
	                String gem7=obj.getString("1399001");
	                Share s5=new Share();
	                s5.setShareName(JSONObject.parseObject(gem7).getString("name"));
	                s5.setPrice(new BigDecimal(JSONObject.parseObject(gem7).getString("price")));
	                s5.setRiseandfallvalue(new BigDecimal(JSONObject.parseObject(gem7).getString("price")).subtract(new BigDecimal(JSONObject.parseObject(gem7).getString("yestclose"))));
	                s5.setRiseandfall(s5.getRiseandfallvalue().divide(new BigDecimal(JSONObject.parseObject(gem7).getString("yestclose")), 2, BigDecimal.ROUND_HALF_UP).toString());
	                System.out.println("比例5"+s5.getRiseandfall());
	                financeDao.update(s5);
	                //上证指数
	                String gem8=obj.getString("0000001");
	                Share s6=new Share();
	                s6.setShareName(JSONObject.parseObject(gem8).getString("name"));
	                s6.setPrice(new BigDecimal(JSONObject.parseObject(gem8).getString("price")));
	                s6.setRiseandfallvalue(new BigDecimal(JSONObject.parseObject(gem8).getString("price")).subtract(new BigDecimal(JSONObject.parseObject(gem8).getString("yestclose"))));
	                s6.setRiseandfall(s6.getRiseandfallvalue().divide(new BigDecimal(JSONObject.parseObject(gem8).getString("yestclose")), 2, BigDecimal.ROUND_HALF_UP).toString());
	                System.out.println("比例6"+s6.getRiseandfall());
	                financeDao.update(s6);
	                //道琼斯指数
	                String gem9=obj.getString("US_DOWJONES");
	                Share s7=new Share();
	                s7.setShareName(JSONObject.parseObject(gem9).getString("name"));
	                s7.setPrice(new BigDecimal(JSONObject.parseObject(gem9).getString("price")));
	                s7.setRiseandfallvalue(new BigDecimal(JSONObject.parseObject(gem9).getString("price")).subtract(new BigDecimal(JSONObject.parseObject(gem9).getString("yestclose"))));
	                s7.setRiseandfall(s7.getRiseandfallvalue().divide(new BigDecimal(JSONObject.parseObject(gem9).getString("yestclose")), 2, BigDecimal.ROUND_HALF_UP).toString());
	                System.out.println("比例7"+s7.getRiseandfall());
	                financeDao.update(s7);
	                //纳斯达克
	                String gem10=obj.getString("US_NASDAQ");
	                Share s8=new Share();
	                s8.setShareName(JSONObject.parseObject(gem10).getString("name"));
	                s8.setPrice(new BigDecimal(JSONObject.parseObject(gem10).getString("price")));
	                s8.setRiseandfallvalue(new BigDecimal(JSONObject.parseObject(gem10).getString("price")).subtract(new BigDecimal(JSONObject.parseObject(gem10).getString("yestclose"))));
	                s8.setRiseandfall(s8.getRiseandfallvalue().divide(new BigDecimal(JSONObject.parseObject(gem10).getString("yestclose")), 2, BigDecimal.ROUND_HALF_UP).toString());
	                System.out.println("比例8"+s8.getRiseandfall());
	                financeDao.update(s8);
	                //恒生指数
	                String gem11=obj.getString("hkHSI");
	                Share s9=new Share();
	                s9.setShareName(JSONObject.parseObject(gem11).getString("name"));
	                s9.setPrice(new BigDecimal(JSONObject.parseObject(gem11).getString("price")));
	                s9.setRiseandfallvalue(new BigDecimal(JSONObject.parseObject(gem11).getString("price")).subtract(new BigDecimal(JSONObject.parseObject(gem11).getString("yestclose"))));
	                s9.setRiseandfall(s9.getRiseandfallvalue().divide(new BigDecimal(JSONObject.parseObject(gem11).getString("yestclose")), 2, BigDecimal.ROUND_HALF_UP).toString());
	                
	                System.out.println("比例9"+s9.getRiseandfall());
	                financeDao.update(s9);
	                
	                
	           }else if(httpResponse.getStatusLine().getStatusCode() == 400){
	               //..........
	           }else if(httpResponse.getStatusLine().getStatusCode() == 500){
	               //.............
	           }
	       } catch (IOException e) {
	           e.printStackTrace();
	       }finally {
	           try {
	               httpCilent2.close();
	           } catch (IOException e) {
	               e.printStackTrace();
	           }
	       }
	 }
		 
}
