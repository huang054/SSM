package com.javen.task;

import java.io.Serializable;

public class JsonModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private double close;
	private double open;
	private String name;
	private double rise_percent;
	private String symbol;
	private double weight;
	public double getClose() {
		return close;
	}
	public void setClose(double close) {
		this.close = close;
	}
	public double getOpen() {
		return open;
	}
	public void setOpen(double open) {
		this.open = open;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getRise_percent() {
		return rise_percent;
	}
	public void setRise_percent(double rise_percent) {
		this.rise_percent = rise_percent;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	@Override
	public String toString() {
		return "JsonModel [close=" + close + ", open=" + open + ", name=" + name + ", rise_percent=" + rise_percent
				+ ", symbol=" + symbol + ", weight=" + weight + "]";
	}
	
	
	
	

}
