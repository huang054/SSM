package com.javen.task;

import java.io.Serializable;
import java.math.BigDecimal;

public class Share implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String shareName;
	private BigDecimal price;
	private BigDecimal Riseandfallvalue;
	private String Riseandfall;
	public String getShareName() {
		return shareName;
	}
	public void setShareName(String shareName) {
		this.shareName = shareName;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getRiseandfallvalue() {
		return Riseandfallvalue;
	}
	public void setRiseandfallvalue(BigDecimal riseandfallvalue) {
		Riseandfallvalue = riseandfallvalue;
	}
	public String getRiseandfall() {
		return Riseandfall;
	}
	public void setRiseandfall(String riseandfall) {
		Riseandfall = riseandfall;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((shareName == null) ? 0 : shareName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Share other = (Share) obj;
		if (shareName == null) {
			if (other.shareName != null)
				return false;
		} else if (!shareName.equals(other.shareName))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Share [shareName=" + shareName + ", price=" + price + ", Riseandfallvalue=" + Riseandfallvalue
				+ ", Riseandfall=" + Riseandfall + "]";
	}
	
	

}
