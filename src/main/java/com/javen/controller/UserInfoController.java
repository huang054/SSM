package com.javen.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.javen.model.MobileCode;
import com.javen.model.QueryPara;
import com.javen.model.Subscribe;
import com.javen.model.UserFundDetails;
import com.javen.model.UserInfo;
import com.javen.service.ISubscribeService;
import com.javen.service.IUserFundDetailsService;
import com.javen.service.IUserInfoService;
import com.javen.utils.Api;
import com.javen.utils.Constants;
import com.javen.utils.CreateSessionUtil;
import com.javen.utils.VerifyMethod;

/**
 * 用户控制层
 * 
 * @author win 10
 *
 */

@Controller
@RequestMapping("/userInfo")
public class UserInfoController {


	private static Logger log = LoggerFactory.getLogger(UserInfoController.class);
	@Autowired
	private IUserInfoService userInfoService;

	@Autowired
	private IUserFundDetailsService userFundDetailsService;
	
	@Autowired
	private ISubscribeService subscribService; 

	@RequestMapping(value = "/longin", method = RequestMethod.POST)
	@ResponseBody
	public Map Login(HttpServletRequest request,HttpServletResponse response,String userMobile,String userPwd) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		Map<String, Object> map = new HashMap<String, Object>();
		if (!VerifyMethod.checkMobileNumber(userMobile)) {
			map.put("message", "手机号码输入有误！");
			map.put("code", "0000");
			map.put("succeed", "false");
			return map;
		}
		UserInfo user = userInfoService.findByUserMobile(userMobile);
		if (user == null) {
			map.put("message", "用户没有注册！");
			map.put("code", "0000");
			map.put("succeed", "false");
			return map;
		}
		if (!user.getUserPwd().equals(userPwd)) {
			map.put("message", "用户名与密码错误！");
			map.put("code", "0000");
			map.put("succeed", "false");
			return map;
		}
		/*
		 * String token =""; token = user.getId() + user.getUserPwd() + (new
		 * Date()).getTime(); Cookie cookie = new Cookie("ssoToken",token);
		 * cookie.setPath("/"); response.addCookie(cookie);
		 */

		String token = CreateSessionUtil.setSessionUser(user);
		// TODO查询数据
		map.put("message", "登录成功");
		map.put("code", "9999");
		map.put("succeed", "true");
		map.put("token", token);
		map.put(CreateSessionUtil.SESSION_USER, user);
		return map;
	}

	/**
	 * （更换手机号码，注册手机，）
	 * 发送手机短信
	 * 
	 * @param request
	 * @param userMobile
	 * @return
	 */
	@RequestMapping(value = "/sendNote", method = RequestMethod.GET)
	@ResponseBody
	public Map sendNote(HttpServletRequest request, @RequestParam("userMobile") String userMobile,
			HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		Map<String, Object> map = new HashMap<String, Object>();
		if (!VerifyMethod.checkMobileNumber(userMobile)) {
			map.put("message", "手机号码输入有误！");
			map.put("code", "0000");
			map.put("succeed", "false");
			return map;
		};

		UserInfo user = userInfoService.findByUserMobile(userMobile);
		if (user != null) {
			map.put("message", "手机号码以注册！");
			map.put("code", "0000");
			map.put("succeed", "false");
			return map;
		}
		String code = "";
		String message = "";
		try {
			code = Constants.getCode();
			message = "验证码为" + code + "，有效期10分钟。【链想】";
			Api.sendNote(userMobile, message);
		} catch (Exception e) {
			map.put("message", "发送失败！");
			map.put("code", "0000");
			map.put("succeed", "false");
			return map;
		}

		map.put("message", "验证码以发送到手机，请注意查收");
		map.put("code", "9999");
		map.put("succeed", "true");
		MobileCode codeData=new MobileCode();
		codeData.setMobile(userMobile);
		codeData.setSenTime(new Date());
		codeData.setNodeCode(code);
		CreateSessionUtil.setCode(userMobile, codeData);
//		CreateSessionUtil.setSessionData("codeTime", System.currentTimeMillis());
//		CreateSessionUtil.setSessionData("noteCode", code);
		return map;
}
	
	
	

	/**
	 * 注册用户
	 * 
	 * @param request
	 * @param userMobile
	 * @return
	 */
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	@ResponseBody
	public Map registerUser(HttpServletRequest request, @RequestParam("userMobile") String userMobile,
			@RequestParam("noteCode") String noteCode, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		Map<String, Object> map = new HashMap<String, Object>();
		long timeout = System.currentTimeMillis();
		Date ct = CreateSessionUtil.getCodeTime(userMobile);
		if(ct == null) {
			map.put("message", "验证码错误！");
			map.put("code", "0000");
			map.put("succeed", "false");
			return map;
		}
		long time = ct.getTime();
		long s = (System.currentTimeMillis() - time) / (1000 * 60);
		if (s > 10) {
			map.put("message", "验证码超时！");
			map.put("code", "0000");
			map.put("succeed", "false");
			CreateSessionUtil.SESSION_CODE.remove(userMobile);
			return map;
		}
		MobileCode code = CreateSessionUtil.getCode(userMobile);
		if (!noteCode.equals(code.getNodeCode())) {
			map.put("message", "验证码错误！");
			map.put("code", "0000");
			map.put("succeed", "false");
			return map;
		}
		CreateSessionUtil.SESSION_CODE.remove(userMobile);
		CreateSessionUtil.setSessionData("userMoblie", userMobile);
		map.put("message", "成功注册！");
		map.put("code", "9999");
		map.put("succeed", "true");
		return map;
	}
	/**
	 * 注册设置密码
	 * @param request
	 * @param onepwd
	 * @param twopwd
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/setpwd", method = RequestMethod.POST)
	@ResponseBody
	public Map setpwd(HttpServletRequest request,  String onepwd, String twopwd,String userMoblie, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		Map<String, Object> map = new HashMap<String, Object>();
		if (!onepwd.equals(twopwd)) {
			map.put("message", "前后密码不一致");
			map.put("code", "0000");
			map.put("succeed", "false");
			return map;
		}
//		String userMobile = String.valueOf(CreateSessionUtil.getSessionData("userMoblie"));
		String accountNumber = Constants.getNum(userMoblie);
		try {
			System.out.println(accountNumber);
			UserInfo user = new UserInfo();
			user.setUserMobile(userMoblie);
			user.setUserPwd(onepwd);
			user.setAccountNumber(accountNumber);
			user.setProfitLoss(0.00);
			user.setYesterdayProfitLoss(0.00);
			user.setTotalAssets(0.00);
			user.setTotalAssets(0.00);
			userInfoService.insertByUser(user);
		} catch (Exception e) {
			map.put("message", "注册失败！");
			map.put("code", "0000");
			map.put("succeed", "false");
			return map;
		}
//		userInfoService.updatepwd(user);
		map.put("message", "密码设置成功");
		map.put("code", "9999");
		map.put("succeed", "true");
		return map;
	}
	
	
	public Map verifyNodeCode(String noteCode,String userMobile) {
		long timeout = System.currentTimeMillis();
		Map<String, Object> map = new HashMap<String, Object>();
		Date ct = CreateSessionUtil.getCodeTime(userMobile);
		if(ct == null) {
			map.put("message", "验证码错误！");
			map.put("code", "0000");
			map.put("succeed", "false");
			return map;
		}
		long time = ct.getTime();
		long s = (System.currentTimeMillis() - time) / (1000 * 60);
		if (s > 10) {
			map.put("message", "验证码超时！");
			map.put("code", "0000");
			map.put("succeed", "false");
			CreateSessionUtil.SESSION_CODE.remove(userMobile);
			return map;
		}
		
		MobileCode code = CreateSessionUtil.getCode(userMobile);
		if (!noteCode.equals(code.getNodeCode())) {
			map.put("message", "验证码错误！");
			map.put("code", "0000");
			map.put("succeed", "false");
			return map;
		}
		CreateSessionUtil.SESSION_CODE.remove(userMobile);
		map.put("message", "成功！");
		map.put("code", "9999");
		map.put("succeed", "true");
		return map;
		
		
	}
	
	/**
	 * 发送手机短信
	 * @param request
	 * @param userMobile
	 * @return
	 */
	@RequestMapping(value = "/wujipwd", method = RequestMethod.GET)
	@ResponseBody
	public Map forget(HttpServletRequest request, @RequestParam("userMobile") String userMobile,
			HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		Map<String, Object> map = new HashMap<String, Object>();
		if (!VerifyMethod.checkMobileNumber(userMobile)) {
			map.put("message", "手机号码输入有误！");
			map.put("code", "0000");
			map.put("succeed", "false");
			return map;
		};

		UserInfo user = userInfoService.findByUserMobile(userMobile);
		if (user == null) {
			map.put("message", "手机号码未注册！");
			map.put("code", "6666");
			map.put("succeed", "false");
			return map;
		}
		String code = "";
		String message = "";
		try {
			code = Constants.getCode();
			message = "验证码为" + code + "，有效期10分钟。【链想】";
			Api.sendNote(userMobile, message);
		} catch (Exception e) {
			map.put("message", "发送失败！");
			map.put("code", "0000");
			map.put("succeed", "false");
			return map;
		}

		map.put("message", "验证码以发送到手机，请注意查收");
		map.put("code", "9999");
		map.put("succeed", "true");
		MobileCode codeData=new MobileCode();
		codeData.setMobile(userMobile);
		codeData.setSenTime(new Date());
		codeData.setNodeCode(code);
		CreateSessionUtil.setCode(userMobile, codeData);
//		CreateSessionUtil.setSessionData("codeTime", System.currentTimeMillis());
//		CreateSessionUtil.setSessionData("noteCode", code);
		return map;
	}
	
	/**
	 * 忘记密码
	 * @param request
	 * @param onepwd
	 * @param twopwd
	 * @param userMoblie
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/rupdatepwd", method = RequestMethod.POST)
	@ResponseBody
	public Map updatePwd(HttpServletRequest request,  String onepwd, String twopwd,String userMoblie,
			HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		Map<String, Object> map = new HashMap<String, Object>();
		
		
		if (!onepwd.equals(twopwd)) {
			map.put("message", "前后密码不一致");
			map.put("code", "0000");
			map.put("succeed", "false");
			return map;
		}
//		String userMobile = String.valueOf(CreateSessionUtil.getSessionData("userMoblie"));
		UserInfo user = new UserInfo();
		user.setUserMobile(userMoblie);
		user.setUserPwd(onepwd);
		userInfoService.updateNewPwd(userMoblie, onepwd);
		map.put("message", "密码设置成功");
		map.put("code", "9999");
		map.put("succeed", "true");
		return map;
	}
	
	
	/**
	 * 忘记密码
	 * @param request
	 * @param onepwd
	 * @param twopwd
	 * @param userMoblie
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updatepwd", method = RequestMethod.POST)
	@ResponseBody
	public Map updatePwd(HttpServletRequest request,  String onepwd, String twopwd,String userMoblie,String noteCode,
			HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		Map<String, Object> map = new HashMap<String, Object>();
		
		Map<String, Object> mapCode = new HashMap<String, Object>();
		mapCode = verifyNodeCode(noteCode,userMoblie);
		if(mapCode.get("code").equals("0000")) {
			return mapCode;
		}	
		if (!onepwd.equals(twopwd)) {
			map.put("message", "前后密码不一致");
			map.put("code", "0000");
			map.put("succeed", "false");
			return map;
		}
//		String userMobile = String.valueOf(CreateSessionUtil.getSessionData("userMoblie"));
		UserInfo user = new UserInfo();
		user.setUserMobile(userMoblie);
		user.setUserPwd(onepwd);
		userInfoService.updateNewPwd(userMoblie, onepwd);
		map.put("message", "密码设置成功");
		map.put("code", "9999");
		map.put("succeed", "true");
		return map;
	}

	/**
	 * 查询个人资料与资产
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/ssets", method = RequestMethod.GET)
	@ResponseBody
	public UserInfo findUserProperty(HttpServletRequest request, HttpServletResponse response,@RequestParam String token) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		System.out.println("此时的token"+token);
		UserInfo user = (UserInfo) CreateSessionUtil.getSessionUser(request.getParameter("token"));
		Integer id = user.getId();
		user = userInfoService.findByUserId(id);
		log.debug(user.getUserMobile() + "查询资料");
		return user;
	}

	/**
	 * 查询个人基金
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/findList", method = RequestMethod.POST)
	@ResponseBody
	public Map findList(HttpServletRequest request, HttpServletResponse response,Map map) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		UserInfo user = (UserInfo) CreateSessionUtil.getSessionUser(request.getParameter("token"));

//		UserInfo user = (UserInfo) CreateSessionUtil.getSessionUser(request.getParameter("token"));
		if(user == null) {	
			resultMap.put("msg", "没有登录！");
			resultMap.put("code", "9999");
			return resultMap;
			
		}

		List<UserFundDetails> list = userFundDetailsService.findByAllFund(user.getId(), map);
		List<UserFundDetails> listR = list == null ? new ArrayList<UserFundDetails>() : list;
		PageInfo<UserFundDetails> pageInfo = new PageInfo(listR);
		if (pageInfo.getTotal() != 0) {

			resultMap.put("msg", "查询成功！");
		} else {
			resultMap.put("msg", "没有数据");
			log.debug("查询成功！");
		}
		resultMap.put("count", pageInfo.getTotal());
		resultMap.put("code", "0");
		resultMap.put("data", listR);
		return resultMap;
	}
	
	/**
	 * 个人申购
	 * 
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/subscribe", method = RequestMethod.POST)
	@ResponseBody
	public Map insertSubscribe(HttpServletRequest request, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if(null==request.getParameter("token")||request.getParameter("token").trim().equals("")) {
        	resultMap.put("code", "9999");
    		resultMap.put("message", "用户没有登陆");
    		resultMap.put("succeed", "false");
    		return resultMap;
		}
		Integer sFundId =Integer.valueOf(request.getParameter("sFundId"));
		String sShare =request.getParameter("sShare");
		String token =request.getParameter("token");
		UserInfo user = (UserInfo) CreateSessionUtil.getSessionUser(token);
		Integer userId =user.getId();
		String accountNumber = user.getAccountNumber();
		BigDecimal paymentN = new BigDecimal(request.getParameter("payment"));
		
		Subscribe subscribe = new Subscribe();
		subscribe.setAccountNumber(accountNumber);
		subscribe.setUserId(Integer.valueOf(userId));
		subscribe.setsFundId(sFundId);
		subscribe.setsShare(sShare);
		subscribe.setPayment(paymentN);
		
		subscribService.insertSubscribe(subscribe);
		resultMap.put("code", "0");
		resultMap.put("message", "申购成功！请支付");
		resultMap.put("succeed", "true");
		log.debug("个人申购成功");
		return resultMap;
	}
	
	/**
	 * 查询个人的申购记录
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/selcetfund", method =RequestMethod.POST)
	@ResponseBody
	public Map selectfind(HttpServletRequest request, HttpServletResponse response,Map map) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		UserInfo user = (UserInfo) CreateSessionUtil.getSessionUser(request.getParameter("token"));
		List<Subscribe> list = subscribService.selectFundAll(user.getId(),map);
	
		List<Subscribe> listR = list == null ? new ArrayList<Subscribe>() : list;
		PageInfo<Subscribe> pageInfo = new PageInfo(listR);
		
		if (pageInfo.getTotal() != 0) {
			resultMap.put("msg", "查询成功！");
		} else {
			resultMap.put("msg", "没有数据！");		
			log.debug("查询成功！");
		}
		resultMap.put("count", pageInfo.getTotal());
		resultMap.put("code", "0");
		resultMap.put("data", listR);
		return resultMap;
		}
	
	/**
	 * 个人中心的支付确认与取消
	 * @param request
	 * @param response
	 * @param id
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "/affirm", method =RequestMethod.GET)
	@ResponseBody
	public Map selectfind(HttpServletRequest request, HttpServletResponse response,@RequestParam("id") Integer id,@RequestParam("status") String status) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		subscribService.updateStatus(id,status);
		resultMap.put("code", "0");
		resultMap.put("message", "操作成功");
		resultMap.put("succeed", "true");
		return resultMap;
		}
	
	/**
	 * 更换手机密码
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/mobilePwd",method=RequestMethod.POST)
	@ResponseBody
	public Map updateUserpwd(HttpServletRequest request,HttpServletResponse response,QueryPara para) {
		response.setHeader("Access-Control-Allow-Origin", "*");
//		UserInfo user = (UserInfo) CreateSessionUtil.getSessionUser(request.getParameter("token"));
//		String userMobile = para.getUserMobile();
//		System.out.println(userMobile);
//		String noteCode =  para.getNoteCode();
//		String userPwd = para.getUserPwd();
//		String twoPwd = para.getTwoPwd();
		String userMobile = request.getParameter("userMobile");
		System.out.println(userMobile);
		String noteCode =request.getParameter("noteCode");
		String userPwd =request.getParameter("userPwd");
		String twoPwd = request.getParameter("twoPwd");
		Date ct = CreateSessionUtil.getCodeTime(userMobile);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		long timeout = System.currentTimeMillis();
		long time = ct.getTime();
		long s = (System.currentTimeMillis() - time) / (1000 * 60);
		if (s > 10) {
			resultMap.put("message", "验证码超时！");
			resultMap.put("code", "0000");
			resultMap.put("succeed", "false");
			CreateSessionUtil.SESSION_CODE.remove(userMobile);
			return resultMap;
		}
	 MobileCode code = CreateSessionUtil.getCode(userMobile);
		if (!noteCode.equals(code.getNodeCode())) {
			resultMap.put("message", "验证码错误！");
			resultMap.put("code", "0000");
			resultMap.put("succeed", "false");
			return resultMap;
		}
		
		if(!userPwd.equals(twoPwd)) {
			resultMap.put("message", "密码不一致！！");
			resultMap.put("code", "0000");
			resultMap.put("succeed", "false");
			return resultMap;
		}
		CreateSessionUtil.SESSION_CODE.remove(userMobile);
		userInfoService.updateNewPwd(userMobile,userPwd);
		resultMap.put("message", "修改成功！");
		resultMap.put("code", "9999");
		resultMap.put("succeed", "true");
		return resultMap;
	}
	
	
	/**
	 * 修改手机号码
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/updateMobile",method=RequestMethod.POST)
	@ResponseBody
	public Map updateUserMobile(HttpServletRequest request,HttpServletResponse response,QueryPara para) {
		response.setHeader("Access-Control-Allow-Origin", "*");
//		UserInfo user = (UserInfo) CreateSessionUtil.getSessionUser(request.getParameter("token"));
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String userMobile =  para.getUserMobile();
		log.debug(userMobile);
		String token = para.getToken();
		UserInfo user = (UserInfo) CreateSessionUtil.getSessionUser(token);
		if(null==user) {
			resultMap.put("message", "未登录！");
			resultMap.put("code", "9999");
			return resultMap;
		}
		
		String noteCode =  para.getNoteCode();
		String userPwd = para.getUserPwd();
		//重复代码。有时间些成一个接口
		Date ct = CreateSessionUtil.getCodeTime(userMobile);
		
		long timeout = System.currentTimeMillis();
		long time = ct.getTime();
		long s = (System.currentTimeMillis() - time) / (1000 * 60);
		if (s > 10) {
			resultMap.put("message", "验证码超时！");
			resultMap.put("code", "0000");
			resultMap.put("succeed", "false");
			return resultMap;
		}
		 MobileCode code = CreateSessionUtil.getCode(userMobile);
		if (!noteCode.equals(code.getNodeCode())) {
			resultMap.put("message", "验证码错误！");
			resultMap.put("code", "0000");
			resultMap.put("succeed", "false");
			return resultMap;
		}
		Integer userId = user.getId();
		CreateSessionUtil.SESSION_CODE.remove(userMobile);
		userInfoService.updateByMoblie(userId,userMobile,userPwd);
		
		resultMap.put("message", "修改成功");
		resultMap.put("code", "9999");
		resultMap.put("succeed", "true");
		return resultMap;
	}
	 
	/**
	 * 退出
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/loginOut",method=RequestMethod.POST)
	@ResponseBody
	  public Map loginOut(HttpServletRequest request,HttpServletResponse response) {
		    response.setHeader("Access-Control-Allow-Origin", "*");
			Map<String, Object> resultMap = new HashMap<String, Object>();
			if(null==request.getParameter("token")||request.getParameter("token").trim().equals("")) {
	        	resultMap.put("code", "9999");
	    		resultMap.put("message", "用户没有登陆");
	    		resultMap.put("succeed", "false");
	    		return resultMap;
			}
			String token= request.getParameter("token");
			log.info("用户的sessionid为"+token);
			if(null==CreateSessionUtil.SESSION_MAP.get(token)) {
				resultMap.put("code", "9999");
	    		resultMap.put("message", "用户session不存在！");
	    		resultMap.put("succeed", "false");
	    		return resultMap;
			}
			CreateSessionUtil.SESSION_MAP.remove(token);
			resultMap.put("code", "0000");
    		resultMap.put("message", "用户退出成功！");
    		resultMap.put("succeed", "true");
    		return resultMap;
		  
	  }

	/*
	 * @RequestMapping(value="/ssets",method=RequestMethod.GET)
	 * 
	 * @ResponseBody public UserInfo findUserProperty(HttpServletRequest
	 * request,HttpServletResponse response) {
	 * response.setHeader("Access-Control-Allow-Origin", "*"); UserInfo user =
	 * (UserInfo) CreateSessionUtil.getSessionUser(); Integer id = user.getId();
	 * user = userInfoService.findByUserId(id); log.debug(user.getUserMobile() +
	 * "查询资料"); return user; }
	 */
	// /user/test?id=1
	/*
	 * @RequestMapping(value="/test",method=RequestMethod.GET) public String
	 * test(HttpServletRequest request,Model model){ int userId =
	 * Integer.parseInt(request.getParameter("id"));
	 * System.out.println("userId:"+userId); User user=null; if (userId==1) { user =
	 * new User(); // user.setAge(11); // user.setId(1); // user.setPassword("123");
	 * // user.setUserName("javen"); }
	 * 
	 * log.debug(user.toString()); model.addAttribute("user", user); return "index";
	 * }
	 * 
	 * 
	 * // /user/showUser?id=1
	 * 
	 * @RequestMapping(value="/showUser",method=RequestMethod.GET) public String
	 * toIndex(HttpServletRequest request,Model model){ int userId =
	 * Integer.parseInt(request.getParameter("id"));
	 * System.out.println("userId:"+userId); User user =
	 * this.userService.getUserById(userId); log.debug(user.toString());
	 * model.addAttribute("user", user); return "showUser"; }
	 * 
	 * // /user/showUser2?id=1
	 * 
	 * @RequestMapping(value="/showUser2",method=RequestMethod.GET) public String
	 * toIndex2(@RequestParam("id") String id,Model model){ int userId =
	 * Integer.parseInt(id); System.out.println("userId:"+userId); User user =
	 * this.userService.getUserById(userId); log.debug(user.toString());
	 * model.addAttribute("user", user); return "showUser"; }
	 * 
	 * 
	 * // /user/showUser3/{id}
	 * 
	 * @RequestMapping(value="/showUser3/{id}",method=RequestMethod.GET) public
	 * String toIndex3(@PathVariable("id")String id,Map<String, Object> model){ int
	 * userId = Integer.parseInt(id); System.out.println("userId:"+userId); User
	 * user = this.userService.getUserById(userId); log.debug(user.toString());
	 * model.put("user", user); return "showUser"; }
	 * 
	 * // /user/{id}
	 * 
	 * @RequestMapping(value="/{id}",method=RequestMethod.GET) public @ResponseBody
	 * User getUserInJson(@PathVariable String id,Map<String, Object> model){ int
	 * userId = Integer.parseInt(id); System.out.println("userId:"+userId); User
	 * user = this.userService.getUserById(userId); log.info(user.toString());
	 * return user; }
	 * 
	 * // /user/{id}
	 * 
	 * @RequestMapping(value="/jsontype/{id}",method=RequestMethod.GET) public
	 * ResponseEntity<User> getUserInJson2(@PathVariable String id,Map<String,
	 * Object> model){ int userId = Integer.parseInt(id);
	 * System.out.println("userId:"+userId); User user =
	 * this.userService.getUserById(userId); log.info(user.toString()); return new
	 * ResponseEntity<User>(user,HttpStatus.OK); }
	 * 
	 * //文件上传、
	 * 
	 * @RequestMapping(value="/upload") public String showUploadPage(){ return
	 * "user_admin/file"; }
	 * 
	 * @RequestMapping(value="/doUpload",method=RequestMethod.POST) public String
	 * doUploadFile(@RequestParam("file")MultipartFile file) throws IOException{ if
	 * (!file.isEmpty()) { log.info("Process file:{}",file.getOriginalFilename()); }
	 * FileUtils.copyInputStreamToFile(file.getInputStream(), new
	 * File("E:\\",System.currentTimeMillis()+file.getOriginalFilename())); return
	 * "succes"; }
	 */
}