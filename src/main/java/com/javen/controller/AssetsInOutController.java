package com.javen.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.javen.common.PageMode;
import com.javen.common.ParamCode;
import com.javen.common.Response;
import com.javen.model.AssetsInOut;
import com.javen.model.Finance;
import com.javen.model.Subscribe;
import com.javen.model.UserFundDetails;
import com.javen.model.UserInfo;
import com.javen.service.AssetsInOutService;
import com.javen.service.ISubscribeService;
import com.javen.utils.CreateSessionUtil;

@Controller  
@RequestMapping("/assetsInOut") 
public class AssetsInOutController {
	
	@Autowired
	private AssetsInOutService assets;
	
	@Autowired
	private ISubscribeService sbuService;
	
	@RequestMapping("/assetsIn")
	public @ResponseBody Map assetsIn(HttpServletRequest request,Map map,HttpServletResponse response ) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		UserInfo user=null;//(UserInfo) CreateSessionUtil.getSessionUser(request.getParameter("token"));
		if(null==user) {
			user= new UserInfo();
			user.setId(4);
		}
		 Map<String,Object> resultMap = new HashMap<String, Object>();
		List<AssetsInOut> assetss= assets.assetsIn(user.getId(),map);
		 List<AssetsInOut> listR = assetss == null ? new  ArrayList<AssetsInOut>() :assetss;
		 PageInfo<AssetsInOut> pageInfo = new PageInfo(listR);
		 if(pageInfo.getTotal() == 0) {
			 resultMap.put("code", "0");
			 resultMap.put("msg", "没有数据");
			 resultMap.put("count", pageInfo.getTotal());
			 resultMap.put("data", listR);
			 return resultMap;
		 }
		 resultMap.put("code", "0");
		 resultMap.put("msg", "查询成功！");
		 resultMap.put("count", pageInfo.getTotal());
		 resultMap.put("data", listR);
		return resultMap;
		
	}
	
	@RequestMapping("/assetsOut")
	public @ResponseBody Map assetsOut(HttpServletRequest request, Map map,HttpServletResponse response ) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		UserInfo user=(UserInfo) CreateSessionUtil.getSessionUser(request.getParameter("token"));
		 Map<String,Object> resultMap = new HashMap<String, Object>();
		List<AssetsInOut> assetss= assets.assetsOut(user.getId(),map);
		 List<AssetsInOut> listR = assetss == null ? new  ArrayList<AssetsInOut>() :assetss;
		 PageInfo<AssetsInOut> pageInfo = new PageInfo(listR);
		 if(pageInfo.getTotal() == 0) {
			 resultMap.put("code", "0");
			 resultMap.put("msg", "没有数据");
			 resultMap.put("count", pageInfo.getTotal());
			 resultMap.put("data", listR);
			 return resultMap;
		 }
		 resultMap.put("code", "0");
		 resultMap.put("msg", "查询成功！");
		 resultMap.put("count", pageInfo.getTotal());
		 resultMap.put("data", listR);
		return resultMap;
	}
	
	/**
	 * 查询审核
	 */
	@RequestMapping(value="/Nuclear",method=RequestMethod.POST)
	@ResponseBody
	public Response findNuclear(HttpServletRequest request,HttpServletResponse response,PageMode pageMode) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		String status = "1";
		Response  res = new Response();
		Map map = new HashMap<String, Object>();
		try {
			List<AssetsInOut> list = sbuService.findBysubStatus(status,pageMode);
			PageInfo<AssetsInOut> pageInfo = new PageInfo(list);
			map.put("list", list);
//			map.put("page",pageInfo.getPageNum() );
			map.put("count",pageInfo.getTotal());
			res.setData(map);
			res.setMsg("查询成功!");
			res.setRespone(ParamCode.SUCSESS);
			return res;
		} catch (Exception e) {
			res.setMsg("查询失败！");
			res.setRespone(ParamCode.FAIL);
			return res;
		}
		}
	
	/**
	 * 查询转入记录
	 * @param request
	 * @param response
	 * @param pageMode
	 * @return
	 */
	@RequestMapping(value="/shiftto",method=RequestMethod.POST)
	@ResponseBody
	public Response findshiftto(HttpServletRequest request,HttpServletResponse response,PageMode pageMode) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		Integer inOut = 1;
		Response  res = new Response();
		Map map = new HashMap<String, Object>();
		try {
			List<AssetsInOut> list = assets.findByinStatus(inOut,pageMode);
			PageInfo<AssetsInOut> pageInfo = new PageInfo(list);
			map.put("list", list);
//			map.put("page",pageInfo.getPageNum() );
			map.put("count",pageInfo.getTotal());
			res.setData(map);
			res.setMsg("查询成功!");
			res.setRespone(ParamCode.SUCSESS);
			return res;
		} catch (Exception e) {
			res.setMsg("查询失败！");
			res.setRespone(ParamCode.FAIL);
			return res;
		}
		}
	
	/**
	 * 根据手机与订单号查询
	 * @param request
	 * @param response
	 * @param pageMode
	 * @return
	 */
	/*@RequestMapping(value="/outAll",method=RequestMethod.POST)
	@ResponseBody
	public Response findoutAll(HttpServletRequest request,HttpServletResponse response,Map map) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		Integer inOut = 2;
		Response  res = new Response();
		Map resultMap = new HashMap<String, Object>();
		try {
			List<AssetsInOut> list = assets.findByinStatus(inOut);
			PageInfo<AssetsInOut> pageInfo = new PageInfo(list);
			return res;
		} catch (Exception e) {
			res.setMsg("查询失败！");
			res.setRespone(ParamCode.FAIL);
			return res;
		}
		} */
	
}
