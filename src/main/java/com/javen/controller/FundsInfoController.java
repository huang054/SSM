package com.javen.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javen.model.FundsInfo;
import com.javen.model.UserInfo;
import com.javen.service.FundsInfoService;
import com.javen.service.IUserInfoService;
import com.javen.utils.UserInfoNap;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller  
@RequestMapping("/foundInfo") 
public class FundsInfoController {
	private static Logger log=LoggerFactory.getLogger(FundsInfoController.class);
	
	@Resource 
	 private IUserInfoService userInfoService; 
	@Autowired
	private FundsInfoService findInfoService;
	
	
	
	@RequestMapping(value="/findslist",method=RequestMethod.GET)
	public @ResponseBody List<FundsInfo> findInfos(HttpServletRequest request,Model model,HttpServletResponse response ) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		
		List<FundsInfo> infos=findInfoService.findInfos();
		log.info("基金列表为"+infos.toString());
		return infos;
	}
	@RequestMapping(value="/findslist1",method=RequestMethod.GET)
	public @ResponseBody List<FundsInfo> findInfos1(HttpServletRequest request,Model model,HttpServletResponse response ) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		
		List<FundsInfo> infos=findInfoService.findInfos1();
		log.info("基金列表为"+infos.toString());
		return infos;
	}
	@RequestMapping(value="/findslist2",method=RequestMethod.GET)
	public @ResponseBody List<FundsInfo> findInfos2(HttpServletRequest request,Model model,HttpServletResponse response ) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		
		List<FundsInfo> infos=findInfoService.findInfos2();
		log.info("基金列表为"+infos.toString());
		return infos;
	}
	@RequestMapping(value="/findbyid",method=RequestMethod.GET)
	public @ResponseBody FundsInfo findbyid(HttpServletRequest request,@RequestParam("id") String id,Model model,HttpServletResponse response ) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		
		FundsInfo infos=findInfoService.fingInfoById(Integer.parseInt(id));
		log.info("基金详细信息"+id+"的详细信息"+infos.toString());
		return infos;
	}
	
	@RequestMapping(value="/buyfund",method=RequestMethod.GET)
	public @ResponseBody boolean buyFunds(@RequestParam String phone,HttpServletResponse response ) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		UserInfo user =  userInfoService.findByUserMobile(phone);
		if (user == null) {
			return false;
		}
		return true;
	}
	@RequestMapping(value="/hotfund",method=RequestMethod.GET)
    public String hotFund(HttpServletRequest request,@RequestParam("code") String code,Model model,HttpServletResponse response ) {
		response.setHeader("Access-Control-Allow-Origin", "*");
    	 log.info("置顶热门基金的基金代码："+code);
    	 findInfoService.hotFund(code);
    	return "";
    }
	
	@RequestMapping(value="/hotfunds",method=RequestMethod.GET)
    public String hotFunds(HttpServletRequest request,@RequestParam("codes") String codes,Model model,HttpServletResponse response ) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		log.info("置顶二级热门基金的基金代码："+codes);
		findInfoService.hotFunds(codes);
    	return "";
    }

}
