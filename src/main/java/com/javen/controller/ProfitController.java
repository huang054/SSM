package com.javen.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.javen.model.AssetsInOut;
import com.javen.model.Profit;
import com.javen.model.UserInfo;
import com.javen.service.ProfitService;
import com.javen.utils.CreateSessionUtil;
@Controller
@RequestMapping("/profit")
public class ProfitController {
	@Autowired
	private ProfitService pro;
	@RequestMapping("/findProfits")
	public @ResponseBody Map findProfitByUserId(HttpServletRequest request, Map map) {
		UserInfo user=(UserInfo) CreateSessionUtil.getSessionUser(request.getParameter("token"));
		
		 Map<String,Object> resultMap = new HashMap<String, Object>();
		List<Profit> assetss= pro.findProfit(user.getId(),map);
		 List<Profit> listR = assetss == null ? new  ArrayList<Profit>() :assetss;
		 PageInfo<Profit> pageInfo = new PageInfo(listR);
		 if(pageInfo.getTotal() == 0) {
			 resultMap.put("code", "0");
			 resultMap.put("msg", "没有数据");
			 resultMap.put("count", pageInfo.getTotal());
			 resultMap.put("data", listR);
			 return resultMap;
		 }
		 resultMap.put("code", "0");
		 resultMap.put("msg", "查询成功！");
		 resultMap.put("count", pageInfo.getTotal());
		 resultMap.put("data", listR);
		return resultMap;
		
	}

}
