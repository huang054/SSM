package com.javen.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javen.common.ParamCode;
import com.javen.common.Response;
import com.javen.model.Finance;
import com.javen.service.IFinanceService;

@Controller
@RequestMapping("/finance/")
public class FinanceController<T> {
	
	private static Logger log=LoggerFactory.getLogger(LnstitutionOpeningController.class);

	
	@Autowired
	private IFinanceService financeService;
	
	/**
	 * 查询股票基金全球
	 * @param request
	 * @param response
	 * @param type
	 * @return
	 */
	@RequestMapping(value="selecttype",method=RequestMethod.GET)
	@ResponseBody
	public Response findType(HttpServletRequest request,HttpServletResponse response,@RequestParam("type") Integer type) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		Response res = new Response();
		List<Finance> list =financeService.findType(type);
		log.debug("查询列表：" + list.toString());
	
		res.setRespone(ParamCode.SUCSESS);
		res.setMsg("查询成功！");
		res.setData(list);
		return res;
	}
	
	


}
