package com.javen.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javen.model.FundsInfo;
import com.javen.model.Indystry;
import com.javen.service.FindIndustryService;
import com.javen.service.FundsInfoService;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller  
@RequestMapping("/indystryInfo") 
public class IndustryController {
	private static Logger log=LoggerFactory.getLogger(IndustryController.class);
	
	@Autowired
	private FindIndustryService industryservice;
	
	
	
	@RequestMapping(value="/industrylist1",method=RequestMethod.GET)
	public @ResponseBody List<Indystry> findInfos1(HttpServletRequest request,Model  model,HttpServletResponse response ) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		
		List<Indystry> infos=industryservice.findIndystrys1();
		log.info("咨询列表为"+infos.toString());
		return infos;
	}
	@RequestMapping(value="/industrylist2",method=RequestMethod.GET)
	public @ResponseBody List<Indystry> findInfos2(HttpServletRequest request,Model model,HttpServletResponse response ) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		
		List<Indystry> infos=industryservice.findIndystrys2();
		log.info("咨询列表为"+infos.toString());
		return infos;
	}
	@RequestMapping(value="/industrylist3",method=RequestMethod.GET)
	public @ResponseBody List<Indystry> findInfos3(HttpServletRequest request,Model model,HttpServletResponse response ) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		
		List<Indystry> infos=industryservice.findIndystrys3();
		log.info("咨询列表为"+infos.toString());
		return infos;
	}
	@RequestMapping(value="/industrylist4",method=RequestMethod.GET)
	public @ResponseBody List<Indystry> findInfos4(HttpServletRequest request,Model model,HttpServletResponse response ) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		
		List<Indystry> infos=industryservice.findIndystrys4();
		log.info("咨询列表为"+infos.toString());
		return infos;
	}
	@RequestMapping(value="/findindustrybyid4",method=RequestMethod.GET)
	public @ResponseBody Indystry findbyid(HttpServletRequest request,@RequestParam("id") String id,Model model,HttpServletResponse response ) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		
		Indystry infos=industryservice.findIndystryById(Integer.parseInt(id));
		log.info("咨询详细信息"+id+"的详细信息"+infos.toString());
		return infos;
	}

}
