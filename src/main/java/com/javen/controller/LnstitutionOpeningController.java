package com.javen.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.javen.model.LnstitutionOpening;
import com.javen.service.LnstitutionOpeningService;



@Controller
@RequestMapping("/lnstitutionOpening")
public class LnstitutionOpeningController {
	
	private static Logger log=LoggerFactory.getLogger(LnstitutionOpeningController.class);
	
	LnstitutionOpening ln= new LnstitutionOpening();
	@Autowired
	private LnstitutionOpeningService service;
	  
	
	  @RequestMapping("/springUpload")
	  public @ResponseBody boolean springUpload(HttpServletRequest req ,@RequestParam("file") CommonsMultipartFile file, @RequestParam String data) 
	    {     
		     
		  long  startTime=System.currentTimeMillis();
	        System.out.println("fileName："+file.getOriginalFilename());
	        String path=req.getServletContext().getRealPath("/uploadFile")+new Date().getTime()+file.getOriginalFilename();
	         
	        File newFile=new File(path);
	        //通过CommonsMultipartFile的方法直接写文件（注意这个时候）
	        try {
				file.transferTo(newFile);
				if(data.equals("legal_man_licence")) {
					ln.setLegalManLicence(data);
				}else if(data.equals("business_licence")) {
					ln.setBusinessLicence(data);
				}else if(data.equals("card1")) {
					ln.setCard1(data);
				}else if(data.equals("card2")) {
					ln.setCard2(data);
				}  
				return true;
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				return false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				return false;
			}
	      
	    }
	  
	  @RequestMapping("/lnstitution")
	  public @ResponseBody boolean lnstitution(@RequestParam String enterPrise,@RequestParam String officeAdress,@RequestParam String phone,@RequestParam String status) {
		  
		  ln.setDate(new Date());
		  ln.setEnterPrise(enterPrise);
		  ln.setOfficeAdress(officeAdress);
		  ln.setPhone(phone);
		  ln.setStatus(Integer.parseInt(status));
		int i=  service.insertlnstitution(ln);
		 if(i>0) {
		  return true;
		 }
		 return false;
	  }
	  
}
