package com.javen.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javen.service.VerificationCodeService;
import com.javen.utils.CreateSessionUtil;
import com.javen.utils.MsgResult;
import com.javen.utils.VerifyMethod;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * 验证码类
 */
@Controller
@RequestMapping("/code")
public class VerificationCodeController {

    @Autowired
    private VerificationCodeService verificationCodeService;

    /**
     * session 注册 图形码
     * @param request
     * @param response
     * code调用
     */
    @RequestMapping(value="/getGraphCode",method = RequestMethod.GET)
    @ResponseBody
    public void getGraphCode(HttpServletRequest request, HttpServletResponse response) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        String code = VerifyMethod.getAuthCodeImg(output);
        CreateSessionUtil.setSessionAuthcode(code);//将验证码存session
        CreateSessionUtil.setSessionData("codeTime",System.currentTimeMillis());
        ServletOutputStream out =null;
        try {
            out = response.getOutputStream();
            output.writeTo(out);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 验证图形验证码
     * @param graphCode
     * @return
     */
    @RequestMapping(value ="/verificationCode",method = {RequestMethod.POST})
    @ResponseBody
    public MsgResult registerUserVerifyCode(String graphCode) {
        return verificationCodeService.registerUserVerifyCode(graphCode);

    }


}
