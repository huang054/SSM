package com.javen.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javen.common.ParamCode;
import com.javen.common.Response;
import com.javen.model.UserInfo;
import com.javen.service.UpdateUserInfoService;

@Controller
@RequestMapping("/updateuser")
public class UpdateUserInfoController {
	
	@Autowired
	private UpdateUserInfoService updateuser;
   @RequestMapping("/updateuserinfo")
   /**
    * 修改用户信息
    * @param user
    * @return
    */
   public @ResponseBody Response updateUserInfo(@RequestBody UserInfo user) {
	    
	   Response res = new Response();
	   if(null==user) {
		   res.setRespone(ParamCode.PARAMERROR);
		   res.setMsg("传入用户参数不合法");
		   return res;
	   }
	   
	    updateuser.updateUserInfo(user);
	    res.setRespone(ParamCode.SUCSESS);
		   res.setMsg("修改用户成功");
	   return res;
   }
   
   /**
    * 修改用户资产
    * @param user
    * @return
    */
   @RequestMapping("/updateuserassets")
   public @ResponseBody Response updateUserAssets(@RequestBody UserInfo user) {
	   Response res = new Response();
	   if(null==user) {
		   res.setRespone(ParamCode.PARAMERROR);
		   res.setMsg("传入用户参数不合法");
		   return res;
	   }
	    updateuser.updateUserAssets(user);
	    res.setRespone(ParamCode.SUCSESS);
		   res.setMsg("修改用户成功");
	   return res;
   }
   /**
    * 找到用户详细信息
    * @param id
    * @return
    */
   @RequestMapping("/findUserInfo")
   public @ResponseBody Response findUserInfo(@RequestParam String id) {
	   Response res = new Response();
	   if(null==id||id.trim().equals("")) {
		   res.setRespone(ParamCode.PARAMERROR);
		   res.setMsg("传入用户参数不合法");
		   return res;
	   }
	   UserInfo user=updateuser.findUserInfo(Integer.parseInt(id));
	   res.setRespone(ParamCode.SUCSESS);
	   res.setMsg("查询用户成功");
	   res.setData(user);
	   
	   return res;
   }
}
