package com.javen.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javen.common.ParamCode;
import com.javen.common.Response;
import com.javen.model.Digiccy;
import com.javen.service.IDigiccyService;

@Controller
@RequestMapping("/digiccy/")
public class DigiccyController {
	
	private static Logger log = LoggerFactory.getLogger(UserInfoController.class);
	
	@Autowired
	public IDigiccyService digiccyservice;
	
	/**
	 * 查询数字货币的种类
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="findall",method=RequestMethod.GET)
	@ResponseBody
	public Response findDgiccy(HttpServletRequest request,HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		Response res = new Response();
		List<Digiccy> list =digiccyservice.findDgiccy();
		log.debug("查询列表：" + list.toString());
	
		res.setRespone(ParamCode.SUCSESS);
		res.setMsg("查询成功！");
		res.setData(list);
		return res;
	}
}
