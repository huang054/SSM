package com.javen.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Digiccy implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String currency;// 币种名称',
	private BigDecimal news;// '最新价格（ustd）'
	private String rose;// '涨幅'
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public BigDecimal getNews() {
		return news;
	}
	public void setNews(BigDecimal news) {
		this.news = news;
	}
	public String getRose() {
		return rose;
	}
	public void setRose(String rose) {
		this.rose = rose;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Digiccy other = (Digiccy) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
