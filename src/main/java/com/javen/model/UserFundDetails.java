package com.javen.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class UserFundDetails implements Serializable{


	private static final long serialVersionUID = 1L;
		private Integer id;
		private String fName;
		@JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
		private Date purchaseTime;
		private BigDecimal share;
		private Integer userId;
		private BigDecimal netValue;
		private  BigDecimal dailyIncreases;
		private BigDecimal yesterDayProfitLoss;
		private  BigDecimal addProfitLoss;
		private BigDecimal totalMoney;
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getfName() {
			return fName;
		}
		public void setfName(String fName) {
			this.fName = fName;
		}
		public Date getPurchaseTime() {
			return purchaseTime;
		}
		public void setPurchaseTime(Date purchaseTime) {
			this.purchaseTime = purchaseTime;
		}
		public BigDecimal getShare() {
			return share;
		}
		public void setShare(BigDecimal share) {
			this.share = share;
		}
		public Integer getUserId() {
			return userId;
		}
		public void setUserId(Integer userId) {
			this.userId = userId;
		}
		public BigDecimal getNetValue() {
			return netValue;
		}
		public void setNetValue(BigDecimal netValue) {
			this.netValue = netValue;
		}
		public BigDecimal getDailyIncreases() {
			return dailyIncreases;
		}
		public void setDailyIncreases(BigDecimal dailyIncreases) {
			this.dailyIncreases = dailyIncreases;
		}
		
		
		
		public BigDecimal getYesterDayProfitLoss() {
			return yesterDayProfitLoss;
		}
		public void setYesterDayProfitLoss(BigDecimal yesterDayProfitLoss) {
			this.yesterDayProfitLoss = yesterDayProfitLoss;
		}
		public BigDecimal getAddProfitLoss() {
			return addProfitLoss;
		}
		public void setAddProfitLoss(BigDecimal addProfitLoss) {
			this.addProfitLoss = addProfitLoss;
		}
		public BigDecimal getTotalMoney() {
			return totalMoney;
		}
		public void setTotalMoney(BigDecimal totalMoney) {
			this.totalMoney = totalMoney;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			UserFundDetails other = (UserFundDetails) obj;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			return true;
		}
		
		
}
