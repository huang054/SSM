package com.javen.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Subscribe implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer userId;
	private String sFundName;//基金名称
	private Integer sFundId ;//基金id
	private BigDecimal sNetFund; //单位净值
	private String sShare;//支付方式
	private BigDecimal payment; //申购份额
	@JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date creatTime;//申购时间
	@JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date payCreatTime;//支付时间
	private  BigDecimal serviceCharge;//手续费
	private BigDecimal totalMoney;//申购总额
	private String status;//申购状态(0: 是待支付，1：支付当等后台确认，2：后台确认支付成功，3：后台确认支付失败 ,4:用户选择取消支付）
	private String sLink;//申购图片存放地址
	private String orderNumber;
	private BigDecimal ustdNumber;//ustd数量
	private String userMobile;//手机号
	private BigDecimal number;//数量
	private String accountNumber;
	
	
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public BigDecimal getNumber() {
		return number;
	}
	public void setNumber(BigDecimal number) {
		this.number = number;
	}
	public String getUserMobile() {
		return userMobile;
	}
	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}
	public BigDecimal getUstdNumber() {
		return ustdNumber;
	}
	public void setUstdNumber(BigDecimal ustdNumber) {
		this.ustdNumber = ustdNumber;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getsFundName() {
		return sFundName;
	}
	public void setsFundName(String sFundName) {
		this.sFundName = sFundName;
	}
	public Integer getsFundId() {
		return sFundId;
	}
	public void setsFundId(Integer sFundId) {
		this.sFundId = sFundId;
	}

	
	
	public BigDecimal getsNetFund() {
		return sNetFund;
	}
	public void setsNetFund(BigDecimal sNetFund) {
		this.sNetFund = sNetFund;
	}
	public String getsShare() {
		return sShare;
	}
	public void setsShare(String sShare) {
		this.sShare = sShare;
	}
	public BigDecimal getPayment() {
		return payment;
	}
	public void setPayment(BigDecimal payment) {
		this.payment = payment;
	}
	public Date getCreatTime() {
		return creatTime;
	}
	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}
	public Date getPayCreatTime() {
		return payCreatTime;
	}
	public void setPayCreatTime(Date payCreatTime) {
		this.payCreatTime = payCreatTime;
	}
	public BigDecimal getServiceCharge() {
		return serviceCharge;
	}
	public void setServiceCharge(BigDecimal serviceCharge) {
		this.serviceCharge = serviceCharge;
	}
	public BigDecimal getTotalMoney() {
		return totalMoney;
	}
	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getsLink() {
		return sLink;
	}
	public void setsLink(String sLink) {
		this.sLink = sLink;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Subscribe other = (Subscribe) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
