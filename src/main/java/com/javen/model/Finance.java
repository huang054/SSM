package com.javen.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Finance  implements Serializable{

	/**
	 * @author Administrator
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private String latest;
	private BigDecimal Riseandfallvalue;
	private String Riseandfall; 
	private Integer type;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLatest() {
		return latest;
	}
	public void setLatest(String latest) {
		this.latest = latest;
	}
	
	public BigDecimal getRiseandfallvalue() {
		return Riseandfallvalue;
	}
	public void setRiseandfallvalue(BigDecimal riseandfallvalue) {
		Riseandfallvalue = riseandfallvalue;
	}
	public String getRiseandfall() {
		return Riseandfall;
	}
	public void setRiseandfall(String riseandfall) {
		Riseandfall = riseandfall;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
