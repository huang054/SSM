package com.javen.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class FundsInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String code;
	@JsonFormat(pattern ="yyyy-MM-dd", timezone = "GMT+8")
	private Date net_time;
	private double net_fund;
	private double accumulated_net;
	private String fund_name;
	private String daily_increase;
	private String last_half_year;
	private String last_year;
	private String since_this_year;
	private String since_founding;
	private double ten_thousand_income;
	private String seven_day_annualized;
	private String Publisher;
	private String Details;
	private int Recommend;
	private int status;
	private String fund_type;
	private String mode_operation;
	private String fund_manager;
	private String custodian;
	private String Fund_risk_level;
	private String investment_scope;
	private Date survival_time;
	private String unit_value;
	private Date founding_time;
	
	private double transferCharge;
	private BigDecimal totalPayment;
	private BigDecimal residualShare;
	private String netValueUnit;
	private BigDecimal fundTotal;
	
	
	
	
	
	public BigDecimal getFundTotal() {
		return fundTotal;
	}
	public void setFundTotal(BigDecimal fundTotal) {
		this.fundTotal = fundTotal;
	}
	public String getNetValueUnit() {
		return netValueUnit;
	}
	public void setNetValueUnit(String netValueUnit) {
		this.netValueUnit = netValueUnit;
	}
	public BigDecimal getTotalPayment() {
		return totalPayment;
	}
	public void setTotalPayment(BigDecimal totalPayment) {
		this.totalPayment = totalPayment;
	}
	public BigDecimal getResidualShare() {
		return residualShare;
	}
	public void setResidualShare(BigDecimal residualShare) {
		this.residualShare = residualShare;
	}
	public double getTransferCharge() {
		return transferCharge;
	}
	public void setTransferCharge(double transferCharge) {
		this.transferCharge = transferCharge;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Date getNet_time() {
		return net_time;
	}
	public void setNet_time(Date net_time) {
		this.net_time = net_time;
	}
	public double getNet_fund() {
		return net_fund;
	}
	public void setNet_fund(double net_fund) {
		this.net_fund = net_fund;
	}
	public double getAccumulated_net() {
		return accumulated_net;
	}
	public void setAccumulated_net(double accumulated_net) {
		this.accumulated_net = accumulated_net;
	}
	public String getFund_name() {
		return fund_name;
	}
	public void setFund_name(String fund_name) {
		this.fund_name = fund_name;
	}
	public String getDaily_increase() {
		return daily_increase;
	}
	public void setDaily_increase(String daily_increase) {
		this.daily_increase = daily_increase;
	}
	public String getLast_half_year() {
		return last_half_year;
	}
	public void setLast_half_year(String last_half_year) {
		this.last_half_year = last_half_year;
	}
	public String getLast_year() {
		return last_year;
	}
	public void setLast_year(String last_year) {
		this.last_year = last_year;
	}
	public String getSince_this_year() {
		return since_this_year;
	}
	public void setSince_this_year(String since_this_year) {
		this.since_this_year = since_this_year;
	}
	public String getSince_founding() {
		return since_founding;
	}
	public void setSince_founding(String since_founding) {
		this.since_founding = since_founding;
	}
	public double getTen_thousand_income() {
		return ten_thousand_income;
	}
	public void setTen_thousand_income(double ten_thousand_income) {
		this.ten_thousand_income = ten_thousand_income;
	}
	public String getSeven_day_annualized() {
		return seven_day_annualized;
	}
	public void setSeven_day_annualized(String seven_day_annualized) {
		this.seven_day_annualized = seven_day_annualized;
	}
	public String getPublisher() {
		return Publisher;
	}
	public void setPublisher(String publisher) {
		Publisher = publisher;
	}
	public String getDetails() {
		return Details;
	}
	public void setDetails(String details) {
		Details = details;
	}
	public int getRecommend() {
		return Recommend;
	}
	public void setRecommend(int recommend) {
		Recommend = recommend;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getFund_type() {
		return fund_type;
	}
	public void setFund_type(String fund_type) {
		this.fund_type = fund_type;
	}
	public String getMode_operation() {
		return mode_operation;
	}
	public void setMode_operation(String mode_operation) {
		this.mode_operation = mode_operation;
	}
	public String getFund_manager() {
		return fund_manager;
	}
	public void setFund_manager(String fund_manager) {
		this.fund_manager = fund_manager;
	}
	public String getCustodian() {
		return custodian;
	}
	public void setCustodian(String custodian) {
		this.custodian = custodian;
	}
	public String getFund_risk_level() {
		return Fund_risk_level;
	}
	public void setFund_risk_level(String fund_risk_level) {
		Fund_risk_level = fund_risk_level;
	}
	public String getInvestment_scope() {
		return investment_scope;
	}
	public void setInvestment_scope(String investment_scope) {
		this.investment_scope = investment_scope;
	}
	public Date getSurvival_time() {
		return survival_time;
	}
	public void setSurvival_time(Date survival_time) {
		this.survival_time = survival_time;
	}
	public String getUnit_value() {
		return unit_value;
	}
	public void setUnit_value(String unit_value) {
		this.unit_value = unit_value;
	}
	public Date getFounding_time() {
		return founding_time;
	}
	public void setFounding_time(Date founding_time) {
		this.founding_time = founding_time;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FundsInfo other = (FundsInfo) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "FindInfo [id=" + id + ", code=" + code + ", net_time=" + net_time + ", net_fund=" + net_fund
				+ ", accumulated_net=" + accumulated_net + ", fund_name=" + fund_name + ", daily_increase="
				+ daily_increase + ", last_half_year=" + last_half_year + ", last_year=" + last_year
				+ ", since_this_year=" + since_this_year + ", since_founding=" + since_founding
				+ ", ten_thousand_income=" + ten_thousand_income + ", seven_day_annualized=" + seven_day_annualized
				+ ", Publisher=" + Publisher + ", Details=" + Details + ", Recommend=" + Recommend + ", status="
				+ status + ", fund_type=" + fund_type + ", mode_operation=" + mode_operation + ", fund_manager="
				+ fund_manager + ", custodian=" + custodian + ", Fund_risk_level=" + Fund_risk_level
				+ ", investment_scope=" + investment_scope + ", survival_time=" + survival_time + ", unit_value="
				+ unit_value + ", founding_time=" + founding_time + "]";
	}
	
	
   
}
