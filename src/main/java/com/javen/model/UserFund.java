package com.javen.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class UserFund implements Serializable {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer userId;
	private Integer fundId;
	private Double purchaseAmount;
	@JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date purchaseTime;
	private Double accumulatedIncome;
	private Double yesterdayIncome;
	
	private BigDecimal totalPayment;
	private BigDecimal netFund;
	
	
	
	public BigDecimal getTotalPayment() {
		return totalPayment;
	}
	public void setTotalPayment(BigDecimal totalPayment) {
		this.totalPayment = totalPayment;
	}
	public BigDecimal getNetFund() {
		return netFund;
	}
	public void setNetFund(BigDecimal netFund) {
		this.netFund = netFund;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getFundId() {
		return fundId;
	}
	public void setFundId(Integer fundId) {
		this.fundId = fundId;
	}
	public Double getPurchaseAmount() {
		return purchaseAmount;
	}
	public void setPurchaseAmount(Double purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}
	public Date getPurchaseTime() {
		return purchaseTime;
	}
	public void setPurchaseTime(Date purchaseTime) {
		this.purchaseTime = purchaseTime;
	}
	public Double getAccumulatedIncome() {
		return accumulatedIncome;
	}
	public void setAccumulatedIncome(Double accumulatedIncome) {
		this.accumulatedIncome = accumulatedIncome;
	}
	public Double getYesterdayIncome() {
		return yesterdayIncome;
	}
	public void setYesterdayIncome(Double yesterdayIncome) {
		this.yesterdayIncome = yesterdayIncome;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserFund other = (UserFund) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
