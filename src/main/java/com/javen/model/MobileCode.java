package com.javen.model;

import java.util.Date;

public class MobileCode {
 private String nodeCode;
 private String mobile;
 private Date senTime;
public String getNodeCode() {
	return nodeCode;
}
public void setNodeCode(String nodeCode) {
	this.nodeCode = nodeCode;
}
public String getMobile() {
	return mobile;
}
public void setMobile(String mobile) {
	this.mobile = mobile;
}
public Date getSenTime() {
	return senTime;
}
public void setSenTime(Date senTime) {
	this.senTime = senTime;
} 
}
