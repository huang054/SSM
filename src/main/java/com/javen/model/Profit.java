package com.javen.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Profit implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private int userId;
	private String yinFundName;
	private double yinFundsFnfo;
	private BigDecimal yinDailyIncrease;
	private BigDecimal profitLoss;

	private String time;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getYinFundName() {
		return yinFundName;
	}
	public void setYinFundName(String yinFundName) {
		this.yinFundName = yinFundName;
	}
	public double getYinFundsFnfo() {
		return yinFundsFnfo;
	}
	public void setYinFundsFnfo(double yinFundsFnfo) {
		this.yinFundsFnfo = yinFundsFnfo;
	}
	
	
	
	public BigDecimal getYinDailyIncrease() {
		return yinDailyIncrease;
	}
	public void setYinDailyIncrease(BigDecimal yinDailyIncrease) {
		this.yinDailyIncrease = yinDailyIncrease;
	}
	public BigDecimal getProfitLoss() {
		return profitLoss;
	}
	public void setProfitLoss(BigDecimal profitLoss) {
		this.profitLoss = profitLoss;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getTime() {
		return time;
	}
	public void setTime(Date time) {
		   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		     
		this.time = sdf.format(time);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Profit other = (Profit) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Profit [id=" + id + ", userId=" + userId + ", yinFundName=" + yinFundName + ", yinFundsFnfo="
				+ yinFundsFnfo + ", yinDailyIncrease=" + yinDailyIncrease + ", profitLoss=" + profitLoss + ", time="
				+ time + "]";
	}
	
	

}
