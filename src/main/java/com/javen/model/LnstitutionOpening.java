package com.javen.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class LnstitutionOpening implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String enterPrise;
	private String officeAdress;
	private String legalManLicence;
	private String businessLicence;
	private String card1;
	private String card2;
	private String phone;
	@JsonFormat(pattern ="yyyy-MM-dd", timezone = "GMT+8")
	private Date date;
	private int status;
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEnterPrise() {
		return enterPrise;
	}
	public void setEnterPrise(String enterPrise) {
		this.enterPrise = enterPrise;
	}
	public String getOfficeAdress() {
		return officeAdress;
	}
	public void setOfficeAdress(String officeAdress) {
		this.officeAdress = officeAdress;
	}
	public String getLegalManLicence() {
		return legalManLicence;
	}
	public void setLegalManLicence(String legalManLicence) {
		this.legalManLicence = legalManLicence;
	}
	public String getBusinessLicence() {
		return businessLicence;
	}
	public void setBusinessLicence(String businessLicence) {
		this.businessLicence = businessLicence;
	}
	public String getCard1() {
		return card1;
	}
	public void setCard1(String card1) {
		this.card1 = card1;
	}
	public String getCard2() {
		return card2;
	}
	public void setCard2(String card2) {
		this.card2 = card2;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LnstitutionOpening other = (LnstitutionOpening) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "LnstitutionOpening [id=" + id + ", enterPrise=" + enterPrise + ", officeAdress=" + officeAdress
				+ ", legalManLicence=" + legalManLicence + ", businessLicence=" + businessLicence + ", card1=" + card1
				+ ", card2=" + card2 + ", phone=" + phone + ", date=" + date + ", status=" + status + "]";
	}

	
	
	

}
