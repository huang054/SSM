package com.javen.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AssetsInOut implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	private int userId;
	private String fundName;
	private BigDecimal fundNum;
	private double fundPirce;
	private double totalPirce;
	private int inOut;
	private String submit;
	private String date;

	private String orderNumber;// '订单号'
	private String userPhone;// '手机号码'
	private String accountNumber;// '基金账户'
	private BigDecimal netFund;// '基金净值'
	private BigDecimal serviceCharge;// '总手续费'
	private BigDecimal ustdNumber;// ustd数'
	private String sShare;// '支付方式'
	private BigDecimal number;// '数量'
	private String imgUrl;// '凭证确认'
	private String sLink;//转账凭证
	
	
	

	public String getsLink() {
		return sLink;
	}

	public void setsLink(String sLink) {
		this.sLink = sLink;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getNetFund() {
		return netFund;
	}

	public void setNetFund(BigDecimal netFund) {
		this.netFund = netFund;
	}

	public BigDecimal getServiceCharge() {
		return serviceCharge;
	}

	public void setServiceCharge(BigDecimal serviceCharge) {
		this.serviceCharge = serviceCharge;
	}

	public BigDecimal getUstdNumber() {
		return ustdNumber;
	}

	public void setUstdNumber(BigDecimal ustdNumber) {
		this.ustdNumber = ustdNumber;
	}

	public String getsShare() {
		return sShare;
	}

	public void setsShare(String sShare) {
		this.sShare = sShare;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public BigDecimal getNumber() {
		return number;
	}

	public void setNumber(BigDecimal number) {
		this.number = number;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	
	
	public BigDecimal getFundNum() {
		return fundNum;
	}

	public void setFundNum(BigDecimal fundNum) {
		this.fundNum = fundNum;
	}

	public double getFundPirce() {
		return fundPirce;
	}

	public void setFundPirce(double fundPirce) {
		this.fundPirce = fundPirce;
	}

	public double getTotalPirce() {
		return totalPirce;
	}

	public void setTotalPirce(double totalPirce) {
		this.totalPirce = totalPirce;
	}

	public int getInOut() {
		return inOut;
	}

	public void setInOut(int inOut) {
		this.inOut = inOut;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getDate() {
		return date;
	}

	public void setDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		this.date = sdf.format(date);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssetsInOut other = (AssetsInOut) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AssetsInOut [id=" + id + ", userId=" + userId + ", fundName=" + fundName + ", fundNum=" + fundNum
				+ ", fundPirce=" + fundPirce + ", totalPirce=" + totalPirce + ", inOut=" + inOut + ", submit=" + submit
				+ ", date=" + date + "]";
	}

}
