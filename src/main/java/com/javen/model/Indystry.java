package com.javen.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Indystry implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String title;
	private String content;
	private int praise_num;
	@JsonFormat(pattern ="yyyy-MM-dd", timezone = "GMT+8")
	private Date create_date;
	private int status;
	private int hideStatus;
	private int informationCategory;
	private String issuer;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getPraise_num() {
		return praise_num;
	}
	public void setPraise_num(int praise_num) {
		this.praise_num = praise_num;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getHideStatus() {
		return hideStatus;
	}
	public void setHideStatus(int hideStatus) {
		this.hideStatus = hideStatus;
	}
	public int getInformationCategory() {
		return informationCategory;
	}
	public void setInformationCategory(int informationCategory) {
		this.informationCategory = informationCategory;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Indystry other = (Indystry) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Indystry [id=" + id + ", title=" + title + ", content=" + content + ", praise_num=" + praise_num
				+ ", create_date=" + create_date + ", status=" + status + ", hideStatus=" + hideStatus
				+ ", informationCategory=" + informationCategory + ", issuer=" + issuer + "]";
	}
	
	
	

}
