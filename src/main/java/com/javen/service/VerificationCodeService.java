package com.javen.service;

import com.javen.utils.MsgResult;

public interface VerificationCodeService {

    /**
     *验证图形码
     * @param graphCode
     * @return
     */
    public MsgResult registerUserVerifyCode(String graphCode);
}
