package com.javen.service;

import java.util.List;
import java.util.Map;

import com.javen.common.PageMode;
import com.javen.model.AssetsInOut;
import com.javen.model.Finance;
import com.javen.model.Subscribe;

public interface ISubscribeService {

	int insertSubscribe(Subscribe subscribe);

	List<Subscribe> selectFundAll(Integer id,Map map);

	int updateStatus(Integer id,String status);

	List<AssetsInOut> findBysubStatus(String status, PageMode pageMode);

}
