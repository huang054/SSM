package com.javen.service;

import com.javen.model.UserInfo;

public interface UpdateUserInfoService {
	
	public void updateUserInfo(UserInfo user);
	public void updateUserAssets(UserInfo user);
	public UserInfo findUserInfo(int id);

}
