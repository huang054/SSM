package com.javen.service;

import java.util.List;
import java.util.Map;

import com.javen.model.Profit;

public interface ProfitService {
	
	public List<Profit> findProfit(int id,Map map);

}
