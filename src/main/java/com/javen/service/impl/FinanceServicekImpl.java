package com.javen.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javen.dao.IFinanceDao;
import com.javen.model.Finance;
import com.javen.service.IFinanceService;

@Service
public class FinanceServicekImpl implements IFinanceService{
	
	@Autowired
	private IFinanceDao financeDao;
	
	public List<Finance> findType(Integer type) {
		List<Finance> list = financeDao.findType(type);
		return list;
	}


}
