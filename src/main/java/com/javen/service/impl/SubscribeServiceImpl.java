package com.javen.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.javen.common.PageMode;
import com.javen.dao.IDigiccyDao;
import com.javen.dao.IFundsInfoDao;
import com.javen.dao.ISubscribeDao;
import com.javen.model.AssetsInOut;
import com.javen.model.Digiccy;
import com.javen.model.Finance;
import com.javen.model.FundsInfo;
import com.javen.model.Subscribe;
import com.javen.service.ISubscribeService;
import com.javen.utils.BigDecimalUtil;
import com.javen.utils.Constants;
import com.javen.utils.BigDecimalUtil.BigDecimalOprations;

@Service
public class SubscribeServiceImpl implements ISubscribeService {

	@Autowired
	private ISubscribeDao subscrivedao;

	@Autowired
	private IFundsInfoDao foundsInfoDao;
	
	@Autowired
	private IDigiccyDao digiccyDao;

	/**
	 * 个人申购
	 */
	public int insertSubscribe(Subscribe subscribe) {
		Integer foundId = subscribe.getsFundId();
		// 查询基金表的数据（手续费，单位净值）
		FundsInfo fundsInfo = foundsInfoDao.fingInfoById(foundId);

		double tf = fundsInfo.getTransferCharge();// 单份手续费
		BigDecimal totalserviceChargeMoney = new BigDecimal(0);
		// BigDecimal zhonge = new BigDecimal(0.0000);//申购费用
		BigDecimal totalMoney = new BigDecimal(0);
		fundsInfo.getNet_fund();
		BigDecimal netfun = new BigDecimal(fundsInfo.getNet_fund());// 基金净值
		int i = 0;
		try {
			// 总手续费
			// totalserviceChargeMoney =
			// BigDecimalUtil.OperationASMD2(subscribe.getPayment(),
			// tf,subscribe.getsNetFund() , BigDecimalOprations.multiply,
			// 4,BigDecimal.ROUND_DOWN);
			BigDecimal tf1 = new BigDecimal(tf);
			totalserviceChargeMoney = tf1.multiply(subscribe.getPayment());
			// BigDecimal tf2 = new BigDecimal(subscribe.getsNetFund());
			BigDecimal tf2 = new BigDecimal(0);
			tf2 = totalserviceChargeMoney.multiply(netfun);
			// 不加手术费的金额
			// zhonge = BigDecimalUtil.OperationASMD(fundsInfo.getAccumulated_net(),
			// subscribe.getPayment(), BigDecimalOprations.multiply, 4,
			// BigDecimal.ROUND_DOWN);

			BigDecimal zhonge = subscribe.getPayment().multiply(netfun);
			
			// 申购总费用
			totalMoney = BigDecimalUtil.OperationASMD(tf2, zhonge, BigDecimalOprations.add, 2, BigDecimal.ROUND_DOWN);
			BigDecimal usdtNumber = BigDecimalUtil.OperationASMD(totalMoney, Constants.SLEWRATESL, BigDecimalOprations.divide, 4, BigDecimal.ROUND_DOWN);
			//计算ustd的数
			subscribe.setUstdNumber(usdtNumber);
			// 设置状态
			subscribe.setStatus("0");
			// 总手续费
			subscribe.setServiceCharge(tf2);
			// 设置申购总费用
			subscribe.setTotalMoney(totalMoney);
			// 生产申购时间
			subscribe.setCreatTime(new Date());
			String orderNumber = Constants.getOrderNumber();
			subscribe.setOrderNumber(orderNumber);
			subscribe.setsFundName(fundsInfo.getFund_name());
			BigDecimal net = new BigDecimal(fundsInfo.getAccumulated_net());
			subscribe.setsNetFund(net);
			//TODO
			//数量计算，要查询币种表计算出来
			Digiccy dig = digiccyDao.findByDigiccyNews(subscribe.getsShare());
			BigDecimal number = new BigDecimal(0);
			//计算出数量（花费总额换成不同的数字货币的数量）
			number =  BigDecimalUtil.OperationASMD(totalMoney,dig,BigDecimalOprations.divide,4,BigDecimal.ROUND_DOWN);
			subscribe.setNumber(number);
			
			i = subscrivedao.insert(subscribe);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return i;
	}

	public List<Subscribe> selectFundAll(Integer id, Map map) {
		Integer pageNum = map.get("page") == null ? 1 : Integer.valueOf(map.get("page").toString());
		Integer pageSize = map.get("limit") == null ? 10 : Integer.valueOf(map.get("limit").toString());
		PageHelper.startPage(pageNum, pageSize);
		List<Subscribe> list = subscrivedao.selectFun(id);
		return list;
	}

	public int updateStatus(Integer id, String status) {
		int i = subscrivedao.updateStatus(id, status);
		return i;

	}
	
	/**
	 * 查询审核转入
	 */
	public List<AssetsInOut> findBysubStatus(String status, PageMode pageMode) {
		Integer pageNum = pageMode.getPage() == null ? 1 : pageMode.getPage();
		Integer pageSize = pageMode.getLimit() == null ? 10 : pageMode.getLimit();
		PageHelper.startPage(pageNum, pageSize);
		List<Subscribe> list = subscrivedao.findAll(status);
		if (list == null) {
			List<AssetsInOut> listNull  = new ArrayList<AssetsInOut>();
			return listNull;
		}
		List<AssetsInOut> listAsInOut = new ArrayList<AssetsInOut>();
		//这里如果数据库表设置字段一样的话可以直接返回
		//TODO
		
		for(Subscribe sub :list) {
			AssetsInOut ass = new AssetsInOut();
			
			ass.setUserPhone(sub.getUserMobile());
			ass.setOrderNumber(sub.getOrderNumber());
			ass.setFundName(sub.getsFundName());
			ass.setNetFund(sub.getsNetFund());
			ass.setFundNum(sub.getPayment());
			ass.setServiceCharge(sub.getServiceCharge());
			ass.setUstdNumber(sub.getUstdNumber());
			ass.setsShare(sub.getsShare());
			ass.setNumber(sub.getNumber());
			ass.setAccountNumber(sub.getAccountNumber());
			listAsInOut.add(ass);
		}
		
		return listAsInOut;
	}

}
