package com.javen.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.javen.dao.ProfitDao;
import com.javen.model.AssetsInOut;
import com.javen.model.Profit;
import com.javen.service.AssetsInOutService;
import com.javen.service.ProfitService;
@Service
public class ProfitServiceImpl implements ProfitService {
    @Autowired
	private ProfitDao pro;
	
	public List<Profit> findProfit(int id, Map map) {
		Integer pageNum = map.get("page") == null ? 1 : Integer.valueOf(map.get("page").toString());
		Integer pageSize = map.get("limit") == null ? 10 : Integer.valueOf(map.get("limit").toString());
		PageHelper.startPage(pageNum, pageSize);
		return pro.findProfit(id);
	}



}
