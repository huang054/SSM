package com.javen.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.javen.common.PageMode;
import com.javen.dao.AssetsInOutDao;
import com.javen.model.AssetsInOut;
import com.javen.service.AssetsInOutService;
@Service
public class AssetsInOutServiceImpl implements AssetsInOutService {
    
	@Autowired
	private AssetsInOutDao assets;
	
	public List<AssetsInOut> assetsIn(int id,Map map) {
		Integer pageNum = map.get("page") == null ? 1 : Integer.valueOf(map.get("page").toString());
		Integer pageSize = map.get("limit") == null ? 10 : Integer.valueOf(map.get("limit").toString());
		PageHelper.startPage(pageNum, pageSize);
		return assets.assetsIn(id);
	}

	public List<AssetsInOut> assetsOut(int id,Map map) {
		Integer pageNum = map.get("page") == null ? 1 : Integer.valueOf(map.get("page").toString());
		Integer pageSize = map.get("limit") == null ? 10 : Integer.valueOf(map.get("limit").toString());
		PageHelper.startPage(pageNum, pageSize);
		
		return assets.assetsOut(id);
	}

	public List<AssetsInOut> findByinStatus(Integer inOut, PageMode pageMode) {
		Integer pageNum = pageMode.getPage() == null ? 1 : pageMode.getPage();
		Integer pageSize =pageMode.getLimit() == null ? 10 : pageMode.getLimit();
		PageHelper.startPage(pageNum, pageSize);
		List<AssetsInOut> list = assets.findByinStatus(inOut);
		return list;
	}

}
