package com.javen.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javen.dao.IDigiccyDao;
import com.javen.model.Digiccy;
import com.javen.service.IDigiccyService;

@Service
public class DigiccyServiceImpl implements IDigiccyService{
	
	@Autowired
	private IDigiccyDao digiccyDao;
	
	public List<Digiccy> findDgiccy() {
		List<Digiccy> list = digiccyDao.findDgiccyAll();
		return list;
	}

}
