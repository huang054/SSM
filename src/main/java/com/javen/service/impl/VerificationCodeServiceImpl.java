package com.javen.service.impl;

import org.springframework.stereotype.Service;

import com.javen.service.VerificationCodeService;
import com.javen.utils.CreateSessionUtil;
import com.javen.utils.MsgResult;
import com.javen.utils.VerifyMethod;

@Service
public class VerificationCodeServiceImpl implements VerificationCodeService {

    /**
     * 验证图形码
     * @param graphCode
     * @return
     */
    public MsgResult registerUserVerifyCode(String graphCode) {
        MsgResult msgResultModel = new MsgResult();
        String strCode=(String) CreateSessionUtil.getSessionAuthcode();
        Long  creatTime =Long.parseLong(CreateSessionUtil.getSessionData("codeTime").toString());
        Long s = (System.currentTimeMillis() - creatTime)/ (1000 * 60) ;
        if (s > 3){
            msgResultModel.setResult(false);
            msgResultModel.setMessage("验证码超时！");
            return msgResultModel;
        }
        if(!VerifyMethod.hasLength(strCode)){
            msgResultModel.setResult(false);
            msgResultModel.setMessage("验证码无法获取！");
            return msgResultModel;
        }
        if(!strCode.equalsIgnoreCase(graphCode)){
            msgResultModel.setResult(false);
            msgResultModel.setMessage("验证码不匹配！");
            return msgResultModel;
        }

        msgResultModel.setResult(true);
        msgResultModel.setMessage("SUCCESS");
        return msgResultModel;
    }
}
