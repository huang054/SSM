package com.javen.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.github.pagehelper.PageHelper;
import com.javen.dao.IUserFundDetailsDao;
import com.javen.dao.IUserInfoDao;
import com.javen.model.FundsInfo;
import com.javen.model.UserFundDetails;
import com.javen.model.UserInfo;
import com.javen.service.IUserInfoService;
import com.javen.utils.MsgResult;

@Service
public class UserInfoServiceImpl implements IUserInfoService {

	@Autowired
	private IUserInfoDao userInfoDao;

	

	public UserInfo findByUserMobile(String userMobile) {

		return userInfoDao.findByuserMobile(userMobile);

	}

	public int insertByUser(UserInfo user) {
		return userInfoDao.insertByUser(user);

	}

	public int updatepwd(UserInfo user) {

		return userInfoDao.updatepwd(user);
	}

	public UserInfo findByUserId(Integer id) {

		return userInfoDao.findByUserId(id);
	}

	public int updateNewPwd(String userMobile, String newPwd) {
		int i = userInfoDao.updateNewPwd(userMobile,newPwd);
		
		return i;
	}

	public int updateByMoblie(Integer userId, String userMobile, String userPwd) {
		int i = userInfoDao.updateByMoblie(userId,userMobile,userPwd);
		return i;
	}

	public List<UserInfo> FindUsers(Map map) {
		Integer pageNum = map.get("page") == null ? 1 : Integer.valueOf(map.get("page").toString());
		Integer pageSize = map.get("limit") == null ? 10 : Integer.valueOf(map.get("limit").toString());
		PageHelper.startPage(pageNum, pageSize);
		return userInfoDao.FindUsers();
	}

	public UserInfo findUserByFundName(String fundName) {
		// TODO Auto-generated method stub
		return userInfoDao.findUserByUserFund(fundName);
	}

	public List<FundsInfo> findFundsByUserId(String userId, Map map) {
		Integer pageNum = map.get("page") == null ? 1 : Integer.valueOf(map.get("page").toString());
		Integer pageSize = map.get("limit") == null ? 10 : Integer.valueOf(map.get("limit").toString());
		PageHelper.startPage(pageNum, pageSize);
		return userInfoDao.findFundsByUserId(Integer.parseInt(userId));
	}

}
