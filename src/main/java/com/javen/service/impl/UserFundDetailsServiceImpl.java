package com.javen.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.javen.dao.IUserFundDetailsDao;
import com.javen.model.UserFundDetails;
import com.javen.service.IUserFundDetailsService;

@Service
public class UserFundDetailsServiceImpl implements IUserFundDetailsService{
	
	@Autowired
    private IUserFundDetailsDao iUserFundDetailsDao;
	
	/**
	 * 查询个人基金
	 */
	public List<UserFundDetails> findByAllFund(Integer id, Map map) {
		Integer pageNum = map.get("page") == null ? 1 : Integer.valueOf(map.get("page").toString());
		Integer pageSize = map.get("limit") == null ? 10 : Integer.valueOf(map.get("limit").toString());
		PageHelper.startPage(pageNum, pageSize);
		List<UserFundDetails> userFundD = iUserFundDetailsDao.findUserAllFun(id);
		return userFundD;
	}
}
