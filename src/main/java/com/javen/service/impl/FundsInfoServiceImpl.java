package com.javen.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javen.dao.IFundsInfoDao;
import com.javen.model.FundsInfo;
import com.javen.service.FundsInfoService;
import com.sun.org.apache.bcel.internal.generic.IFNONNULL;

@Service 
public class FundsInfoServiceImpl implements FundsInfoService {
    
	@Autowired
	private IFundsInfoDao findInfoDao;
	public List<FundsInfo> findInfos() {
		// TODO Auto-generated method stub
		return findInfoDao.findInfos();
	}

	public FundsInfo fingInfoById(int id) {
		// TODO Auto-generated method stub
		return findInfoDao.fingInfoById(id);
	}

	public List<FundsInfo> findInfos1() {
		// TODO Auto-generated method stub
		return findInfoDao.findInfos1();
	}

	public List<FundsInfo> findInfos2() {
		// TODO Auto-generated method stub
		return findInfoDao.findInfos2();
	}

	public void hotFund(String code) {
		// TODO Auto-generated method stub
		findInfoDao.hotFund(code);
	}

	public void hotFunds(String codes) {
		// TODO Auto-generated method stub
		findInfoDao.hotFunds(codes);
	}



}
