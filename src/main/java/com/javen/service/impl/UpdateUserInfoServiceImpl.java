package com.javen.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javen.dao.UpdateUserDao;
import com.javen.model.UserInfo;
import com.javen.service.UpdateUserInfoService;

@Service
public class UpdateUserInfoServiceImpl implements UpdateUserInfoService {
	
	@Autowired
	private UpdateUserDao updateUser;
   
	public void updateUserInfo(UserInfo user) {
		// TODO Auto-generated method stub
        this.updateUser.updateUserInfo(user);
	}

	public void updateUserAssets(UserInfo user) {
		// TODO Auto-generated method stub
        this.updateUser.updateUserAssets(user);
	}

	public UserInfo findUserInfo(int id) {
		// TODO Auto-generated method stub
		return updateUser.findUserInfo(id);
	}

}
