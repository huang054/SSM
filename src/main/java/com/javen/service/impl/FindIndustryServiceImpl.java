package com.javen.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javen.dao.IndustryDao;
import com.javen.model.Indystry;
import com.javen.service.FindIndustryService;

@Service 
public class FindIndustryServiceImpl implements FindIndustryService {
	
	@Autowired
	private IndustryDao industryDao;



	public Indystry findIndystryById(int id) {
		// TODO Auto-generated method stub
		return this.industryDao.findIndystryById(id);
	}

	public List<Indystry> findIndystrys1() {
		// TODO Auto-generated method stub
		return this.industryDao.findIndystrys1();
	}

	public List<Indystry> findIndystrys2() {
		// TODO Auto-generated method stub
		return this.industryDao.findIndystrys2();
	}

	public List<Indystry> findIndystrys3() {
		// TODO Auto-generated method stub
		return this.industryDao.findIndystrys3();
	}

	public List<Indystry> findIndystrys4() {
		// TODO Auto-generated method stub
		return this.industryDao.findIndystrys4();
	}

}
