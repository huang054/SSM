package com.javen.service;

import java.util.List;
import java.util.Map;

import com.javen.common.PageMode;
import com.javen.model.AssetsInOut;

public interface AssetsInOutService {
	public List<AssetsInOut> assetsIn(int id,Map map);
	public List<AssetsInOut> assetsOut(int id,Map map);
	public List<AssetsInOut> findByinStatus(Integer inOut, PageMode pageMode);

}
