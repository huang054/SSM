package com.javen.service;

import java.util.List;
import java.util.Map;

import com.javen.model.UserFundDetails;

public interface IUserFundDetailsService {

	List<UserFundDetails> findByAllFund(Integer userId, Map map);

}
