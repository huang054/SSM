package com.javen.service;

import java.util.List;

import com.javen.model.Indystry;

public interface FindIndustryService {
	
	public List<Indystry> findIndystrys1();
	public List<Indystry> findIndystrys2();
	public List<Indystry> findIndystrys3();
	public List<Indystry> findIndystrys4();
    public Indystry findIndystryById(int id);

}
