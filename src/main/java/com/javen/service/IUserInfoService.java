package com.javen.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.Model;

import com.javen.model.FundsInfo;
import com.javen.model.UserFundDetails;
import com.javen.model.UserInfo;



public interface IUserInfoService {
	
	//通过手机号码查询用户
	UserInfo findByUserMobile(String userMobile);

	//注册一个用户
	int insertByUser(UserInfo user);
	
	int updatepwd(UserInfo user); 


	int updateNewPwd(String userMobile, String newPwd);

	UserInfo findByUserId(Integer id);

	int updateByMoblie(Integer userId, String userMobile, String userPwd);
	
	List<UserInfo> FindUsers(Map map);
	
	UserInfo findUserByFundName(String fundName);
	List<FundsInfo> findFundsByUserId(String userId,Map map);
 
}  