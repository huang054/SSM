package com.javen.dao;

import java.util.List;

import com.javen.model.Finance;
import com.javen.task.Share;

public interface IFinanceDao {

	List<Finance> findType(Integer type);
    void update(Share share);
}
