package com.javen.dao;

import java.util.List;

import com.javen.model.Profit;

public interface ProfitDao {
	
	public List<Profit> findProfit(int id);

}
