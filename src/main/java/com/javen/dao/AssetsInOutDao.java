package com.javen.dao;

import java.util.List;

import com.javen.model.AssetsInOut;

public interface AssetsInOutDao {
	
	public List<AssetsInOut> assetsIn(int id);
	public List<AssetsInOut> assetsOut(int id);
	public List<AssetsInOut> findByinStatus(Integer inOut);

}
