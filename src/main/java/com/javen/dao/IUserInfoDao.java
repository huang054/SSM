package com.javen.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.javen.model.FundsInfo;
import com.javen.model.UserInfo;

public interface IUserInfoDao {

	UserInfo findByuserMobile(String userMobile);
	
	int insertByUser(UserInfo user);
/*    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);*/

	int updatepwd(UserInfo user);

	UserInfo findByUserId(Integer id);

	int updateNewPwd(@Param("userMobile")String userMobile,@Param("newPwd") String newPwd);

	int updateByMoblie(@Param("userId")Integer userId,@Param("userMobile") String userMobile,@Param("userPwd") String userPwd);
	List<UserInfo> FindUsers();
	UserInfo findUserByUserFund(String userFund);
	List<FundsInfo> findFundsByUserId(int userId);

}