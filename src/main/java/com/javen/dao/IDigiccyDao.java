package com.javen.dao;

import java.util.List;

import com.javen.model.Digiccy;
import com.javen.task.JsonModel;

public interface IDigiccyDao {

	List<Digiccy> findDgiccyAll();

	Digiccy findByDigiccyNews(String getsShare);
    
	void update(JsonModel json);
}
