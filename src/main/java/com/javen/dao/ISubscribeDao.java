package com.javen.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.javen.model.Subscribe;

public interface ISubscribeDao {

	int insert(Subscribe subscribe);

	List<Subscribe> selectFun(Integer id);
	int updateStatus(@Param("id")Integer id,@Param("status")String status);

	List<Subscribe> findAll(String status);
}
