package com.javen.dao;

import com.javen.model.UserInfo;

public interface UpdateUserDao {
      
	public void updateUserInfo(UserInfo user);
	public void updateUserAssets(UserInfo user);
	public UserInfo findUserInfo(int id);
	
}
