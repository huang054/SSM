package com.javen.utils;

import java.math.BigDecimal;

/**
 * @author yazhang
 * @date 2018/3/112:00
 */
public class BigDecimalUtil {
    /**
     * + - * / add subtract multiply divide
     */
   public  enum BigDecimalOprations{
        add,subtract,multiply,divide
    }

    /**
     *
     * @param numOne
     * @param numTwo
     * @param bigDecimalOpration
     * @param scale 保留位数
     * @param roundingMode
     * @return
     * @throws Exception
     */
    public static BigDecimal OperationASMD(Object numOne,Object numTwo,BigDecimalOprations bigDecimalOpration,int scale,int roundingMode) throws Exception{
        BigDecimal num1 = new BigDecimal(String.valueOf(numOne)).setScale(scale,roundingMode);
        BigDecimal num2 = new BigDecimal(String.valueOf(numTwo)).setScale(scale,roundingMode);
        switch (bigDecimalOpration){
            case add: return num1.add(num2).setScale(scale,roundingMode);
            case subtract: return num1.subtract(num2).setScale(scale,roundingMode);
            case multiply: return num1.multiply(num2).setScale(scale,roundingMode);
            case divide: return num1.divide(num2, scale, roundingMode);
        }
        return null;
    }
    
    public static BigDecimal OperationASMD2(Object numOne,Object numTwo,Object numThree,BigDecimalOprations bigDecimalOpration,int scale,int roundingMode) throws Exception{
        BigDecimal num1 = new BigDecimal(String.valueOf(numOne)).setScale(scale,roundingMode);
        BigDecimal num2 = new BigDecimal(String.valueOf(numTwo)).setScale(scale,roundingMode);
        BigDecimal num3 = new BigDecimal(String.valueOf(numThree)).setScale(scale,roundingMode);
        switch (bigDecimalOpration){
            case add: return num1.add(num2).add(num3).setScale(scale,roundingMode);
            case subtract: return num1.subtract(num2).subtract(num3).setScale(scale,roundingMode);
            case multiply: return num1.multiply(num2).multiply(num3).setScale(scale,roundingMode);
            case divide: return num1.divide(num2, scale, roundingMode).divide(num3, scale, roundingMode);
        }
        return null;
    }
    
    public static void main(String[] args){
        try {
            System.out.println(BigDecimalUtil.OperationASMD(36.23,23.369,BigDecimalOprations.add,2,BigDecimal.ROUND_DOWN));
            System.out.println(BigDecimalUtil.OperationASMD("36.23","23.369",BigDecimalOprations.add,2,BigDecimal.ROUND_DOWN));
            System.out.println(BigDecimalUtil.OperationASMD(36,23,BigDecimalOprations.add,2,BigDecimal.ROUND_DOWN));
            System.out.println(BigDecimalUtil.OperationASMD(36l,69l,BigDecimalOprations.add,2,BigDecimal.ROUND_DOWN));
            System.out.println(BigDecimalUtil.OperationASMD(new BigDecimal(0.2635),new BigDecimal(2.3568),BigDecimalOprations.add,2,BigDecimal.ROUND_DOWN));


            System.out.println(BigDecimalUtil.OperationASMD(36.23,23.369,BigDecimalOprations.subtract,2,BigDecimal.ROUND_DOWN));
            System.out.println(BigDecimalUtil.OperationASMD("36.23","23.369",BigDecimalOprations.subtract,2,BigDecimal.ROUND_DOWN));
            System.out.println(BigDecimalUtil.OperationASMD(36,23,BigDecimalOprations.subtract,2,BigDecimal.ROUND_DOWN));
            System.out.println(BigDecimalUtil.OperationASMD(36l,69l,BigDecimalOprations.subtract,2,BigDecimal.ROUND_DOWN));
            System.out.println(BigDecimalUtil.OperationASMD(new BigDecimal(0.2635),new BigDecimal(2.3568),BigDecimalOprations.subtract,2,BigDecimal.ROUND_DOWN));


            System.out.println(BigDecimalUtil.OperationASMD(36.23,23.369,BigDecimalOprations.multiply,2,BigDecimal.ROUND_DOWN));
            System.out.println(BigDecimalUtil.OperationASMD("36.23","23.369",BigDecimalOprations.multiply,2,BigDecimal.ROUND_DOWN));
            System.out.println(BigDecimalUtil.OperationASMD(36,23,BigDecimalOprations.multiply,2,BigDecimal.ROUND_DOWN));
            System.out.println(BigDecimalUtil.OperationASMD(36l,69l,BigDecimalOprations.multiply,2,BigDecimal.ROUND_DOWN));
            System.out.println(BigDecimalUtil.OperationASMD(new BigDecimal(0.2635),new BigDecimal(2.3568),BigDecimalOprations.multiply,2,BigDecimal.ROUND_DOWN));


            System.out.println(BigDecimalUtil.OperationASMD(36.23,23.369,BigDecimalOprations.divide,2,BigDecimal.ROUND_DOWN));
            System.out.println(BigDecimalUtil.OperationASMD("36.23","23.369",BigDecimalOprations.divide,2,BigDecimal.ROUND_DOWN));
            System.out.println(BigDecimalUtil.OperationASMD(36,23,BigDecimalOprations.divide,2,BigDecimal.ROUND_DOWN));
            System.out.println(BigDecimalUtil.OperationASMD(36l,69l,BigDecimalOprations.divide,2,BigDecimal.ROUND_DOWN));
            System.out.println(BigDecimalUtil.OperationASMD(new BigDecimal(0.235),new BigDecimal(0.5689),BigDecimalOprations.divide,2,BigDecimal.ROUND_DOWN));
            
            System.out.println(BigDecimalUtil.OperationASMD2(36.23,23.369,23.0002,BigDecimalOprations.multiply,4,BigDecimal.ROUND_DOWN));
            
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    /**
     * 乘法计算
     * @param goodAmount 物品数量
     * @param price 价格
     * @return
     */
    public static BigDecimal multi(Long goodAmount, BigDecimal price){

        BigDecimal b1 = new BigDecimal((double)goodAmount);
        BigDecimal b2 = new BigDecimal(price.doubleValue());
        return b1.multiply(b2);
    }

    
    /**
     * 字符串的乘法计算
     * @param val1
     * @param val2
     * @return
     */
    public static String multi(String val1,String val2){
        BigDecimal b1 = new BigDecimal(val1);
        BigDecimal b2 = new BigDecimal(val2);
        return b1.multiply(b2).toString();
    }
}
