package com.javen.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.javen.model.MobileCode;

public class CreateSessionUtil {
	/**
	 * 图形验证码 code
	 */
	final static String SESSION_AUTHCODE = "sessionAuthcode";

	/**
	 * 用户信息
	 */
	public final static String SESSION_USER = "sessionUser";

	public static Map<String, HttpSession> SESSION_MAP = new HashMap<String, HttpSession>();
	
	public static Map<String, MobileCode> SESSION_CODE = new HashMap<String,MobileCode>(); 
	
	public static void setCode(String mobile,MobileCode value) {
		
		SESSION_CODE.put(mobile, value);
		System.out.println(value.getNodeCode());
		}
	
	public static MobileCode getCode(String key) {	
		return  SESSION_CODE.get(key);	
	}
	
	public static Map<String,Date> SESSION_TIME = new HashMap<String,Date>();
	
	public static void setCodeTime(String code,Date l) {
		
		SESSION_TIME.put(code, l);
		System.out.println(l);
		}
	
	public static Date getCodeTime(String code) {
		
		return  SESSION_CODE.get(code).getSenTime();
		
	}

	/**
	 * 构造session
	 * 
	 * @return
	 */
	public static HttpSession getSession() {
		HttpSession session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest()
				.getSession();
         
		// System.out.println("Current SessionID:"+session.getId());
		return session;
	}

	/**
	 * 获取当前Session ID
	 * 
	 * @return 当前会话Session ID
	 */
	public static String getSessionId() {
		return getSession().getId();
	}

	/**
	 * 从会话(Session)中提取数据
	 * 
	 * @param key
	 *            键值key
	 * @return
	 */
	public static Object getSessionData(String key) {
		return SESSION_MAP.get(key).getAttribute(SESSION_USER);
	}

	/**
	 * 往会话(Session)中保存数据
	 * 
	 * @param key
	 *            键值key
	 * @param data
	 *            保存数据
	 */
	public static String  setSessionData(String key, Object data) {
		HttpSession session = CreateSessionUtil.getSession();
		session.setAttribute(key, data);
		SESSION_MAP.put(session.getId(), session);
		return session.getId();
	}

	/**
	 * 删除会话(Session)中的数据
	 * 
	 * @param key
	 *            键值key
	 * @return
	 */
	public static void removeSessionData(String key) {
		CreateSessionUtil.getSession().removeAttribute(key);
	}

	/**
	 * 创建图形码的session对象
	 * 
	 * @param strCode
	 */
	public static void setSessionAuthcode(String strCode) {
		setSessionData(SESSION_AUTHCODE, strCode);
	}

	/**
	 * 得到图形码session对象
	 * 
	 * @return
	 */
	public static Object getSessionAuthcode() {
		return getSessionData(SESSION_AUTHCODE);
	}

	/**
	 * 移除图形码session对象
	 */
	public static void removeSessionAuthcode() {
		removeSessionData(SESSION_AUTHCODE);
	}

	/**
	 * 从Session中提取用户信息
	 * 
	 * @return 用户信息
	 */
	public static Object getSessionUser(String token) {
		return getSessionData(token);
	}

	/**
	 * 保存用户信息至Session中
	 * 
	 * @param userInfo
	 *            用户信息
	 */
	public static String   setSessionUser(Object userInfo) {
		return setSessionData(SESSION_USER, userInfo);
	}

	/**
	 * 从Session中删除用户信息
	 * 
	 * @param userInfo
	 *            用户信息
	 */
	public static void removeSessionUser(Object userInfo) {
		removeSessionData(SESSION_USER);
	}

}
