package com.javen.utils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Constants {
	public final static String testUsername = "18826520066";
	public final static String testPassword = "boatsPass1wMs";
	
	//人民币转换美元的汇率
	public final static BigDecimal SLEWRATESL= new BigDecimal(6.69) ;
	
	public static String getCode() {
        String res = "";
        Random r = new Random();
        for(int i=0;i<6;i++){
            int m =0 + r.nextInt(9 - 0);
            res=res+m;
        }

        return res;
    }
	
	/**
	 * 生成个人账号
	 * @param userMobile
	 * @return
	 */
	public static String getNum(String userMobile) {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");//设置日期格式
		String date = df.format(new Date());// new Date()为获取当前系统时间，也可使用当前时间戳
		date.substring(2);
		int i = 100+(int)(Math.random()*899);
		String s = userMobile.substring(8) + date.substring(2) + i;
		return s;}
	
	/**
	 * 生成订单号   订单号规则：随机2位+年月日（180629）+随机3位    11+180629+111
	 * @param userMobile
	 * @return
	 */
	public static String getOrderNumber() {
		int j =  10+(int)(Math.random()*89);
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");//设置日期格式
		String date = df.format(new Date());// new Date()为获取当前系统时间，也可使用当前时间戳
		date.substring(2);
		int i = 100+(int)(Math.random()*899);
		String s = j + date.substring(2) + i;
		return s;}
	
/*	public static void main(String[] args) {
		for(int i =0;i < 50; i++) {
		System.out.println(getOrderNumber());
		}
	}*/
	
}
