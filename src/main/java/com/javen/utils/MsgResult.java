package com.javen.utils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * 页面请求结果类
 * @author Quyuzhong
 * @version 1.0
 */
public class MsgResult implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Boolean result;
    private String code;
    private String message;
    private Map<String, Object> params;

    public MsgResult(){
        result=true;
        code="000";
        message="SUCCESS";
        params=new HashMap<String, Object>();
    }
    public MsgResult(String message){
        this();
        this.message=message;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
        if(!result&&this.code.equalsIgnoreCase("000")){
            this.code="999";
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    @Override
    public String toString() {
        StringBuffer result=new StringBuffer();
        result.append("MsgResult [result=");
        result.append(result);
        result.append(", message=");
        result.append(message);
        for(String key:params.keySet()){
            result.append(",params->");
            result.append(key);
            result.append("=");
            result.append(params.get(key));
        }
        return "MsgResult [result=" + result + ", message=" + message + "]";
    }


}