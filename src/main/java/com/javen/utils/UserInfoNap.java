package com.javen.utils;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.javen.model.UserInfo;

public class UserInfoNap {
	
	public static Map<String,UserInfo> userInfos = new ConcurrentHashMap<String, UserInfo>();
	
	public static UserInfo getUserInfo(String phone) {
		return userInfos.get(phone);
	}
    
	public static void putUserInfo(String phone,UserInfo user) {
		userInfos.put(phone, user);
	}
}
